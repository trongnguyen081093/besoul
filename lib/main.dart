import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/src/presentation/access_order_besoul/access_order_screen.dart';
import 'package:flutter_app/src/presentation/cart_besoul/cart_besoul_screen.dart';
import 'package:flutter_app/src/presentation/contact_besoul/contact_besoul_screen.dart';
import 'package:flutter_app/src/presentation/create_address_besoul/export.dart';
import 'package:flutter_app/src/presentation/create_order_besoul/create_order_screen.dart';
import 'package:flutter_app/src/presentation/create_rate_besoul/create_rate_screen.dart';
import 'package:flutter_app/src/presentation/detail_order_1_besoul/detail_order_1_screen.dart';
import 'package:flutter_app/src/presentation/detail_order_besoul/detail_order_screen.dart';
import 'package:flutter_app/src/presentation/detail_product_1_besoul/detail_product_1_screen.dart';
import 'package:flutter_app/src/presentation/detail_product_besoul/detail_product_screen.dart';
import 'package:flutter_app/src/presentation/list_%20ward_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_address_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_city_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_district_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_trademark_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_voucher_besoul/export.dart';
import 'package:flutter_app/src/presentation/my_kol_besoul/my_kol_screen.dart';
import 'package:flutter_app/src/presentation/order_tracking_besoul/export.dart';
import 'package:flutter_app/src/presentation/rate_detail_besoul/export.dart';
import 'package:flutter_app/src/presentation/search_besoul/export.dart';
import 'package:flutter_app/src/presentation/transportation_besoul/export.dart';
import 'package:get/get.dart';
import 'package:overlay_support/overlay_support.dart'; 

import 'src/configs/configs.dart';
import 'src/presentation/presentation.dart';
import 'src/utils/utils.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await Firebase.initializeApp();
  await AppPrefs.initListener();
  await notificationInitialed();
  runApp(OverlaySupport(child: RestartWidget(child: App())));
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ThemeSwitcherWidget(initialThemeData: normalTheme(context), child: MyApp());
  }
}

class MyApp extends StatefulWidget {
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: FirebaseAnalytics.instance);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addObserver(this);
    AppDeviceInfo.init();
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("ChangeAppLifecycleState: $state");
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: APP_NAME,
      theme: ThemeSwitcher.of(context).themeData,
      navigatorObservers: <NavigatorObserver>[MyApp.observer],
      locale: Locale('vi', 'VN'),
      translationsKeys: AppTranslation.translations,
      home: MyKOLScreen(),
      onGenerateRoute: Routers.generateRoute,
    );
  }
}
