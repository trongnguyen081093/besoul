import 'package:get/get.dart';

const String APP_NAME = "FLUTTER APPLICATION";

const double originalWidth = 375; // width device dùng để xây dựng app
const double originalHeight = 812; // height device dùng để xây dựng app

double scaleWidth = Get.width / originalWidth;
double scaleHeight =  Get.height / originalHeight;

double scaleGeneral = scaleWidth < scaleHeight ? scaleWidth : scaleHeight;

