import 'package:flutter/material.dart';
import '../../utils/utils.dart'; 

class AppColors {
  AppColors._();

  static Color get primary => AppUtils.valueByMode(values: [primaryDark, primaryLight]);

  static Color get primaryDark => fromHex('#FF6915');

  static Color get primaryLight => fromHex('#FF6915');

  static Color get text => AppUtils.valueByMode(values: [textDark, textLight]);

  static Color get textDark => Colors.white;

  static Color get textLight => Colors.black; 
  
  static Color get mint => fromHex('#5EDFDE');

  static Color get gray01 => fromHex('#333333');

  static Color get gray03 => fromHex('#828282');

  static Color get gray05 => fromHex('#E0E0E0');

  static Color get gray04 => fromHex('#BDBDBD');

  static Color get gray02 => fromHex('#4F4F4F');

  static Color get gray06 => fromHex('#F2F2F2');

  static Color get gray07 => fromHex('#E6E7E9');

  static Color get gray08 => fromHex('#C4C4C4');

  static Color get gray09 => fromHex('#E9EAED');

  static Color get gray10 => fromHex('#E8EBEE');

  static Color get gray11 => fromHex('#F5F5F5');

  static Color get gray12 => fromHex('#D8D8D8');

  static Color get blue01 => fromHex('#2F80ED');

  static Color get blue02 => fromHex('#14AFF1');

  static Color get mint01 => fromHex('#17D2D1');

  static Color get mint02 => fromHex('#F4FFFC');

  static Color get star => fromHex('#FDD836');

  static Color get redPrice => fromHex('#FF4E16');

  static Color get mart => fromHex('#1955A6');


  
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
