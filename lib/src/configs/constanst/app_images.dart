class AppImages {

  static png(name) => 'assets/images/png/$name.png';
  static jpg(name) => 'assets/images/jpg/$name.jpg';
  static svg(name) => 'assets/images/svg/$name.svg';
  static lottie(name) => 'assets/lotties/$name.json';

  AppImages._();

  static final String imgLogo = 'assets/images/ic_moon.png';
  static final String imgIntro = 'assets/images/intro/intro.png';
  static final String imgLogoIntro = 'assets/images/intro/logoIntro.png';
  static final String imgFlagVietNam = 'assets/images/flag/vietnam.png';
  static final String imgFlagSouthKorea = 'assets/images/flag/southkorea.png';
  static final String imgFlagUnitedKingdom = 'assets/images/flag/unitedKingdom.png';
  static final String imgFlagUSA = 'assets/images/flag/unitedstates.png';
  static final String imgFlagJapan = 'assets/images/flag/japan.png';
  static final String imgFlagBrazil = 'assets/images/flag/brazil.png';
  static final String imgFlagCanada = 'assets/images/flag/canada.png';
  static final String imgFlagGermany = 'assets/images/flag/germany.png';
  static final String imgFlagSweden = 'assets/images/flag/sweden.png';
  static final String imgFlagFrance = 'assets/images/flag/france.png';
  static final String imgNext = 'assets/images/flag/next.png';
}
