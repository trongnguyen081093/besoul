import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/contact_besoul/contact_besoul_viewmodel.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../presentation.dart';

class ContactScreen extends StatefulWidget {
  const ContactScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<ContactScreen> {
  late ContactViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ContactViewModel>(
        viewModel: ContactViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: AppColors.gray06,
          );
        });
  }

  Widget _buildBody() {
    return Column(
      children: [
        _buildAppBar(),
        _buildContact()
      ],
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.productDetail);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 85, top: 30),
            child: Text(
              "Liên hệ",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildContact(){
    return Container(
      child: Column(
        children: [
          Container(
            color: Colors.white,
            child: Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 14, bottom: 14),
                    child: Image.asset(AppImages.png("phone_call"),
                      width: 20,
                      height: 20,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 8),
                    child: Text(
                      "Hotline hỗ trợ:",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Text(
                      "1900 123456",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.redPrice
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 70),
                    child: IconButton(
                      onPressed: (){
                        launch('tel:1900123456');
                      },
                      icon: Icon(Icons.navigate_next,
                        color: AppColors.gray03,
                      ),
                      iconSize: 24,
                    )
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 5,),
          Container(
            color: Colors.white,
            child: Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 14, bottom: 14),
                    child: Image.asset(AppImages.png("zalo"),
                      width: 20,
                      height: 20,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 8),
                    child: Text(
                      "Chat qua Zalo",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray01
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 164),
                      child: IconButton(
                        onPressed: (){},
                        icon: Icon(Icons.navigate_next,
                          color: AppColors.gray03,
                        ),
                        iconSize: 24,
                      )
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 1,),
          Container(
            color: Colors.white,
            child: Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 14, bottom: 14),
                    child: Image.asset(AppImages.png("messenger"),
                      width: 24,
                      height: 24,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 4),
                    child: Text(
                      "Chat qua Messenger",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray01
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 123),
                      child: IconButton(
                        onPressed: (){},
                        icon: Icon(Icons.navigate_next,
                          color: AppColors.gray03,
                        ),
                        iconSize: 24,
                      )
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
