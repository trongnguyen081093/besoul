import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/presentation/home_besoul/home_besoul_screen.dart';
import '../../configs/configs.dart';
import '../presentation.dart';
import 'package:get/get.dart';

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> with ResponsiveWidget {
  late NavigationViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NavigationViewModel>(
        viewModel: NavigationViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        child: _buildAppbar(context),
        childMobile: _buildMobileAppbar(context),
        builder: (context, viewModel, appbar) {
          return Scaffold(
            body: Column(
              children: [Expanded(
                  child: Center(
                      child: _viewModel.changePage()
                  )
              ),
              ],
            ),
              bottomNavigationBar: _navBar(),
          );
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return Text("NavigationScreen Desktop");
  }

  @override
  Widget buildMobile(BuildContext context) {
    return Text("NavigationScreen Mobile");
  }

  @override
  Widget buildTablet(BuildContext context) {
    return Text("NavigationScreen Tablet");
  }

  Widget _buildMobileAppbar(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: Text(
          'app_name'.tr,
        ),
      ),
    );
  }

  Widget _buildAppbar(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: Text('app_name'.tr),
      ),
    );
  }

  Widget _navBar(){
    return Container(
      height: 84,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [BoxShadow(
          color: AppColors.gray05,
          blurRadius: 10
        )]
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            children: [
              IconButton(
                enableFeedback: false,
                onPressed: () {
                  setState(() {
                    _viewModel.changeIndex = 0;
                  });
                },
                icon: Image.asset(
                  AppImages.png("navigation_home"),
                  color: _viewModel.changeIndex == 0 ? AppColors.mint : AppColors.gray04,
                  width: 24,
                  height: 24,
                ),
              ),
              Text("Trang chủ",
              style: TextStyle(
                fontFamily: AppTextStyles.fontFamilyRoboto,
                fontSize: 10,
                fontWeight: FontWeight.w500,
                color: AppColors.gray03
              ),
              ),
            ],
          ),
          Column(
            children: [
              IconButton(
                  enableFeedback: false,
                  onPressed: () {
                    setState(() {
                      _viewModel.changeIndex  = 1;
                    });
                  },
                  icon: Image.asset(
                    AppImages.png("navigation_gift"),
                    color: _viewModel.changeIndex == 1 ? AppColors.mint : AppColors.gray04,
                    width: 24,
                    height: 24,
                  )
              ),
              Text("Quà của bạn",
                style: TextStyle(
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    fontSize: 10,
                    fontWeight: FontWeight.w500,
                    color: AppColors.gray03
                ),
              ),
            ],
          ),
          Column(
            children: [
              IconButton(
                  enableFeedback: false,
                  onPressed: () {
                    setState(() {
                      _viewModel.changeIndex = 2;
                    });
                  },
                  icon: Image.asset(
                    AppImages.png("navigation_delivery_truck"),
                    color: _viewModel.changeIndex == 2 ? AppColors.mint : AppColors.gray04,
                    width: 24,
                    height: 24,
                  )
              ),
              Text("Vận chuyển",
                style: TextStyle(
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    fontSize: 10,
                    fontWeight: FontWeight.w500,
                    color: AppColors.gray03
                ),
              ),
            ],
          ),
          Column(
            children: [
              Stack(
                children: [
                  Positioned(
                    child: IconButton(
                        enableFeedback: false,
                        onPressed: () {
                          setState(() {
                            _viewModel.changeIndex = 3;
                          });
                        },
                        icon: Image.asset(
                          AppImages.png("navigation_shopping"),
                          color: _viewModel.changeIndex == 3 ? AppColors.mint : AppColors.gray04,
                          width: 24,
                          height: 24,
                        )
                    ),
                  ),
                  Positioned(
                    bottom: 30,
                    left: 20,
                    child: Stack(
                      children: [
                        Positioned(
                          child: Image.asset(
                              AppImages.png("navigation_notify"),
                              width: 20,
                              height: 15,
                            )
                        ),
                        Positioned(
                          top: 2,
                          left: 4,
                          child: Text ("10",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                            fontFamily: AppTextStyles.fontFamilyRoboto
                          ),
                          )
                        ),
                      ],
                    )
                  ),
                ],
              ),
              Text("Giỏ hàng",
                style: TextStyle(
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    fontSize: 10,
                    fontWeight: FontWeight.w500,
                    color: AppColors.gray03
                ),
              ),
            ],
          ),
          Column(
            children: [
              IconButton(
                  enableFeedback: false,
                  onPressed: () {
                    setState(() {
                      _viewModel.changeIndex = 4;
                    });
                  },
                  icon: Image.asset(
                    AppImages.png("navigation_profile"),
                    color: _viewModel.changeIndex == 4 ? AppColors.mint : AppColors.gray04,
                    width: 24,
                    height: 24,
                  )
              ),
              Text("Tôi",
                style: TextStyle(
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    fontSize: 10,
                    fontWeight: FontWeight.w500,
                    color: AppColors.gray03
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
