import 'package:flutter/material.dart';
import 'package:flutter_app/src/presentation/home_besoul/export.dart';
import 'package:flutter_app/src/presentation/login_besoul/export.dart';
import 'package:flutter_app/src/presentation/otp_forgot_password_besoul/export.dart';
import 'package:flutter_app/src/presentation/register_besoul/export.dart';
import 'package:flutter_app/src/presentation/select_country_besoul/export.dart';
import 'package:flutter_app/src/presentation/transportation_besoul/export.dart';
import 'package:flutter_app/src/presentation/your_gift_besoul/export.dart';
import '../../resource/resource.dart';
import '../presentation.dart';

class NavigationViewModel extends BaseViewModel{
  int changeIndex = 0;
  init()async{}

  Widget changePage(){
    switch (changeIndex){
      case 0:
        return HomeBesoulScreen();
        break;
      case 1:
        return YourGiftScreen();
        break;
      case 2:
        return TransportationScreen();
        break;
      case 3:
        return SelectCountryScreen();
        break;
      case 4:
        return OtpForgotPasswordScreen();
        break;
      default:
        return HomeBesoulScreen();
    }
  }
}