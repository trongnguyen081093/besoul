import 'package:flutter/cupertino.dart';
import 'package:flutter_app/src/presentation/list_address_besoul/export.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class CreateAddressViewModel extends BaseViewModel {
  init() async {}

  ValueNotifier<bool> checkSave = ValueNotifier(false);

  TextEditingController name = TextEditingController();



  bool CheckSaveAddress(){
    if(checkSave.value == false){
      return checkSave.value = true;
    } else {
      return checkSave.value = false;
    }
  }
}

class AddressOrder{
  String nameCus;
  String phoneNumber;
  String addressCus;

  AddressOrder({Key ? key, required this.nameCus, required this.phoneNumber, required this.addressCus});
}
