import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/create_address_besoul/create_address_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class CreateAddressScreen extends StatefulWidget {
  const CreateAddressScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<CreateAddressScreen> {
  late CreateAddressViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreateAddressViewModel>(
        viewModel: CreateAddressViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
            bottomNavigationBar: _buildNavigateBar(),
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          SizedBox(height: 20,),
          _buildInputAddress(),
          SizedBox(height: 20,),
          Container(
            height: 8,
            color: AppColors.gray09,
          ),
          _buildCheckBox()
        ],
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.createOrder);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 55, top: 30),
            child: Text(
              "Thêm địa chỉ",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildInputAddress(){
    return Container(
      child: Form(
        child: Container(
          width: Get.width,
          margin: EdgeInsets.only(left: 16, right: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Text(
                  "Họ và tên",
                  style: TextStyle(
                    fontSize: AppFontSize.fontText,
                    fontWeight: FontWeight.w400,
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    color: AppColors.gray02
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                height: 36,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(color: AppColors.gray05)
                ),
                child: TextFormField(
                  controller: _viewModel.name,
                  style: TextStyle(color: AppColors.gray01),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Nhập họ tên",
                    hintStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray03,
                    ),
                    contentPadding: EdgeInsets.only(left: 12,bottom: 14),
                  ),
                ),
              ),

              SizedBox(height: 20,),

              Container(
                child: Text(
                  "Số điện thoại",
                  style: TextStyle(
                      fontSize: AppFontSize.fontText,
                      fontWeight: FontWeight.w400,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray02
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                height: 36,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: AppColors.gray05)
                ),
                child: TextFormField(
                  style: TextStyle(color: AppColors.gray01),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Nhập số điện thoại",
                    hintStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray03,
                    ),
                    contentPadding: EdgeInsets.only(left: 12,bottom: 14),
                  ),
                ),
              ),

              SizedBox(height: 20,),

              Container(
                child: Text(
                  "Địa chỉ nhận hàng",
                  style: TextStyle(
                      fontSize: AppFontSize.fontText,
                      fontWeight: FontWeight.w400,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray02
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                height: 36,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: AppColors.gray05)
                ),
                child: TextFormField(
                  style: TextStyle(color: AppColors.gray01),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Số nhà, tên đường",
                    hintStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray03,
                    ),
                    contentPadding: EdgeInsets.only(left: 12,bottom: 14),
                  ),
                ),
              ),

              SizedBox(height: 20,),

              Container(
                child: Text(
                  "Tỉnh/ Thành phố",
                  style: TextStyle(
                      fontSize: AppFontSize.fontText,
                      fontWeight: FontWeight.w400,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray02
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                height: 36,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: AppColors.gray05)
                ),
                child: TextFormField(
                  style: TextStyle(color: AppColors.gray01),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Nhập Tỉnh/ Thành phố",
                    hintStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray03,
                    ),
                    suffixIcon: Align(
                        widthFactor: 1,
                        heightFactor: 1,
                        child: GestureDetector(
                          onTap: (){},
                          child: Container(
                            width: 24,
                            height: 24,
                            child: Icon(
                              Icons.keyboard_arrow_down,
                              color: AppColors.gray04,
                            ),
                          ),
                        ),
                    ),
                    contentPadding: EdgeInsets.only(left: 12,bottom: 14),
                  ),
                ),
              ),

              SizedBox(height: 20,),

              Container(
                child: Text(
                  "Quận/ Huyện",
                  style: TextStyle(
                      fontSize: AppFontSize.fontText,
                      fontWeight: FontWeight.w400,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray02
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                height: 36,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: AppColors.gray05)
                ),
                child: TextFormField(
                  style: TextStyle(color: AppColors.gray01),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Nhập Quận/ Huyện",
                    hintStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray03,
                    ),
                    suffixIcon: Align(
                      widthFactor: 1,
                      heightFactor: 1,
                      child: GestureDetector(
                        onTap: (){},
                        child: Container(
                          width: 24,
                          height: 24,
                          child: Icon(
                            Icons.keyboard_arrow_down,
                            color: AppColors.gray04,
                          ),
                        ),
                      ),
                    ),
                    contentPadding: EdgeInsets.only(left: 12,bottom: 14),
                  ),
                ),
              ),

              SizedBox(height: 20,),

              Container(
                child: Text(
                  "Phường/ Xã",
                  style: TextStyle(
                      fontSize: AppFontSize.fontText,
                      fontWeight: FontWeight.w400,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray02
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                height: 36,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: AppColors.gray05)
                ),
                child: TextFormField(
                  style: TextStyle(color: AppColors.gray01),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Nhập Phường/ Xã",
                    hintStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray03,
                    ),
                    suffixIcon: Align(
                      widthFactor: 1,
                      heightFactor: 1,
                      child: GestureDetector(
                        onTap: (){},
                        child: Container(
                          width: 24,
                          height: 24,
                          child: Icon(
                            Icons.keyboard_arrow_down,
                            color: AppColors.gray04,
                          ),
                        ),
                      ),
                    ),
                    contentPadding: EdgeInsets.only(left: 12,bottom: 14),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCheckBox(){
    return Container(
      margin: EdgeInsets.only(left: 16, top: 18),
      child: Row(
        children: [
          GestureDetector(
            onTap: (){
              _viewModel.CheckSaveAddress();
            },
            child: Container(
              width: 16,
              height: 16,
              decoration: BoxDecoration(
                border: Border.all(color: AppColors.mint)
              ),
              child: ValueListenableBuilder<bool>(
                valueListenable: _viewModel.checkSave,
                builder: (context, value, child) =>
                    value == true ? Image.asset(AppImages.png("icon_check_2")) : SizedBox()
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 12),
            child: Text(
              "Đặt làm địa chỉ mặc định",
              style: TextStyle(
                  fontSize: AppFontSize.fontText,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: AppColors.gray02
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildNavigateBar(){
    return Container(
      height: 84,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
              color: AppColors.gray05,
              blurRadius: 10
          )]
      ),
      child: Container(
        margin: EdgeInsets.only(left: 20, top: 9, right: 20, bottom: 25),
        child: TextButton(
            onPressed: (){
            },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(AppColors.mint),
            ),
            child: Text(
              "Lưu địa chỉ",
              style: TextStyle(
                  fontSize: AppFontSize.fontText,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: Colors.white
              ),
            )
        ),
      ),
    );
  }
}
