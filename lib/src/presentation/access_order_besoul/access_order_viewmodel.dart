import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class AccessOrderViewModel extends BaseViewModel {
  init() async {}

  List<Product> listProduct = [
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 4,
        totalRate: 300,
        priceSale: 300000,
        priceProduct: 350000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 3,
        totalRate: 100,
        priceSale: 500000,
        priceProduct: 550000),
    Product(imgProduct: AppImages.png("home_inazuma1"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 3,
        totalRate: 100,
        priceSale: 400000,
        priceProduct: 550000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 5,
        totalRate: 100,
        priceSale: 450000,
        priceProduct: 650000),
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 2,
        totalRate: 100,
        priceSale: 300000,
        priceProduct: 500000),
    Product(imgProduct: AppImages.png("home_inazuma1"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 4,
        totalRate: 100,
        priceSale: 450000,
        priceProduct: 500000),
  ];
}

class Product{
  final String imgProduct;
  final String nameProduct;
  final int rateStar;
  final int totalRate;
  final int priceSale;
  final int priceProduct;

  Product({Key ? key, required this.imgProduct, required this.nameProduct, required this.rateStar,
    required this.totalRate, required this.priceSale, required this.priceProduct,});
}
