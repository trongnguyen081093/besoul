import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/access_order_besoul/access_order_viewmodel.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../presentation.dart';

class AccessOrderScreen extends StatefulWidget {
  const AccessOrderScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<AccessOrderScreen> {
  late AccessOrderViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<AccessOrderViewModel>(
        viewModel: AccessOrderViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
            bottomNavigationBar: _buildNavigateBar(),
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          _buildAccess(),
          _buildListProduct()
        ],
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      alignment: Alignment.centerLeft,
      width: Get.width,
      height: 24,
      margin: EdgeInsets.only(left: 16, top: 30),
      child: IconButton(
          onPressed: (){
            Get.toNamed(Routers.createAddress);
          },
          icon: Icon(Icons.arrow_back_ios, color: Colors.black,)),
    );
  }

  Widget _buildAccess(){
    return  Center(
      child: Container(
        child: Column(
          children: [
            Container(
                width: 80,
                height: 80,
                child: Image.asset(AppImages.png("icon_access"),
                )
            ),
            Container(
              margin: EdgeInsets.only(top: 16),
              child: Text(
                "Đặt hàng thành công",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: AppColors.gray01
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 4),
              child: Text(
                "Lorem ipsum dolor sit amet, consectetur",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    fontFamily: AppTextStyles.fontFamilyOpenSans,
                    color: AppColors.gray03
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 24, bottom: 16),
              height: 4,
              color: AppColors.gray09,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildListProduct(){
    return Container(
      width: Get.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List.generate(_viewModel.listProduct.length, (index1) => Container(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only( left: 16, right: 16, bottom: 8, top: 8),
                child: Row(
                  children: [
                    Container(
                      width: 88,
                      height: 88,
                      child: Image.asset(
                        _viewModel.listProduct[index1].imgProduct,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Container(
                      width: 228,
                      margin: EdgeInsets.only(left: 12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              _viewModel.listProduct[index1].nameProduct,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: AppColors.gray01,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: AppTextStyles.fontFamilyRoboto
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 4),
                            child: Row(
                              children: [
                                Row(
                                  children: List.generate(5, (index2) =>  Container(
                                    child: _viewModel.listProduct[index1].rateStar >= index2 + 1 ?
                                    Icon(
                                        Icons.star, size: 15, color: AppColors.star
                                    ) : _viewModel.listProduct[index1].rateStar == 0 ? null :
                                    Icon(
                                        Icons.star, size: 15, color: AppColors.gray05
                                    ),
                                  ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.only(left: 8),
                                  child: Text(
                                    _viewModel.listProduct[index1].totalRate > 0 ? '(${_viewModel.listProduct[index1].totalRate})' : "",
                                    style: TextStyle(
                                        color: AppColors.gray03,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: AppTextStyles.fontFamilyRoboto
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only( top: 10),
                            child: Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.listProduct[index1].priceSale)}',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: AppColors.redPrice,
                                        fontSize: AppFontSize.fontText,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: AppTextStyles.fontFamilyRoboto
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 6),
                                  child: Text(
                                    '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.listProduct[index1].priceProduct)}',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: AppColors.gray03,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        decoration: TextDecoration.lineThrough
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                height: 1,
                color: AppColors.gray10,
              )
            ],
          ),
        )
        ),
      ),
    );
  }

  Widget _buildNavigateBar(){
    return Container(
      height: 84,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
              color: AppColors.gray05,
              blurRadius: 10
          )]
      ),
      child: Container(
        margin: EdgeInsets.only(left: 20, top: 9, right: 20, bottom: 25),
        child: TextButton(
            onPressed: (){
            },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(AppColors.mint),
            ),
            child: Text(
              "Về trang chủ",
              style: TextStyle(
                  fontSize: AppFontSize.fontText,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: Colors.white
              ),
            )
        ),
      ),
    );
  }
}
