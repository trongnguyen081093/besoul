import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class DetailOrderViewModel extends BaseViewModel {
  init() async {}

  int result = 0;
  int Total(){
    int total = 0;
    for(int i = 0; i < listOrder.length; i++){
      for(int j = 0; j < listOrder[i].listProduct.length; j++){
        int resultTemp =  listOrder[i].listProduct[j].amount * listOrder[i].listProduct[j].price;
        total = total + resultTemp;

      }
    }
    result = total;
    notifyListeners();
    return result;
  }

  List<AddressOrder> listAddress = [
    AddressOrder(nameCus: "Nguyen", phoneNumber: "0932127783", addressCus: "20 Lê Lợi, Phường Vĩnh Ninh, Thành Phố Huế")
  ];

  List<ListOrder> listOrder = [
    ListOrder(
        listProduct: [
          ListProduct(imageProduct: AppImages.png("product_sua_rua_mat"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", amount: 2, price: 450000),
          ListProduct(imageProduct: AppImages.png("home_inazuma"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", amount: 1, price: 550000),
          ListProduct(imageProduct: AppImages.png("home_inazuma1"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", amount: 2, price: 500000),
        ]
    )
  ];
}

class AddressOrder{
  String nameCus;
  String phoneNumber;
  String addressCus;

  AddressOrder({Key ? key, required this.nameCus, required this.phoneNumber, required this.addressCus});
}

class ListOrder{
  List<ListProduct> listProduct;

  ListOrder({Key ? key, required this.listProduct});
}

class ListProduct{
  String imageProduct;
  String nameProduct;
  int amount;
  int price;

  ListProduct({Key ? key, required this.imageProduct, required this.nameProduct, required this.amount, required this.price});
}
