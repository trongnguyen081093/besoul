import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/detail_order_besoul/detail_order_viewmodel.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../presentation.dart';

class DetailOrderScreen extends StatefulWidget {
  const DetailOrderScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<DetailOrderScreen> {
  late DetailOrderViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DetailOrderViewModel>(
        viewModel: DetailOrderViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
            bottomNavigationBar: _buildNavigateBar(),
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          _buildTitle(),
          _buildAddress(),
          Container(
            height: 5,
            color: AppColors.gray06,
          ),
          _buildOrder(),
          Container(
            height: 5,
            color: AppColors.gray06,
          ),
          _buildPay(),
          Container(
            height: 5,
            color: AppColors.gray06,
          ),
          _buildTotal()
        ],
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.transportationBesoul);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 35, top: 30),
            child: Text(
              "Chi tiết đơn hàng",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTitle(){
    return Container(
      margin: EdgeInsets.only(top: 16),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Row(
              children: [
                Expanded(
                    child: Text(
                      "Mã đơn hàng",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray01
                      ),
                    )
                ),
                Container(
                  child: Text(
                    "#123456789G",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.mart
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 8,),
          Container(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Row(
              children: [
                Expanded(
                    child: Text(
                      "Thời gian đặt hàng",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray01
                      ),
                    )
                ),
                Container(
                  child: Text(
                    "10-11-2021",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray03
                    ),
                  ),
                ),
                SizedBox(width: 12,),
                Container(
                  child: Text(
                    "10:29",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray03
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 16,),
          Container(
            height: 5,
            color: AppColors.gray06,
          )
        ],
      ),
    );
  }

  Widget _buildAddress(){
    return Container(
      color: Colors.white,
      child: Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Image.asset(AppImages.png("icon_location"),
                  width: 16,
                  height: 16,
                ),
              ),
              SizedBox(width: 8,),
              Expanded(
                // width: 270,
                // margin: EdgeInsets.only(left: 10, top: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        "Địa chỉ nhận hàng:",
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            fontFamily: AppTextStyles.fontFamilyRoboto,
                            color: AppColors.gray01
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              _viewModel.listAddress[0].nameCus,
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: AppTextStyles.fontFamilyRoboto,
                                  color: AppColors.gray03
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 8),
                            width: 1,
                            height: 12,
                            color: AppColors.gray03,
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 8),
                            child: Text(
                              _viewModel.listAddress[0].phoneNumber,
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: AppTextStyles.fontFamilyRoboto,
                                  color: AppColors.gray03
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8, bottom: 16),
                      child: Text(
                        _viewModel.listAddress[0].addressCus,
                        maxLines: 4,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray03,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              GestureDetector(
                onTap: (){
                  Get.toNamed(Routers.listAddress);
                },
                child: Container(
                  width: 24,
                  height: 24,
                  child: Icon(Icons.navigate_next,
                    color: AppColors.gray04,
                  ),
                ),
              ),
            ],
          )
      ),
    );
  }

  Widget _buildOrder(){
    return Container(
      color: Colors.white,
      child: Column(
          children: List.generate(_viewModel.listOrder.length, (index) => Container(
            margin: EdgeInsets.only(left: 16, right: 16),
            child: Column(
                children: List.generate(_viewModel.listOrder[index].listProduct.length, (index1) => Container(
                  margin: EdgeInsets.only(top: 16,),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Image.asset(_viewModel.listOrder[index].listProduct[index1].imageProduct, fit: BoxFit.cover,),
                            width: 77,
                            height: 77,
                          ),
                          Container(
                            width: 240,
                            margin: EdgeInsets.only(left: 10),
                            child: Column(
                              children: [
                                Container(
                                  child: Text(
                                    _viewModel.listOrder[index].listProduct[index1].nameProduct,
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: AppTextStyles.fontFamilyRoboto,
                                      color: AppColors.gray01,
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 26),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        child: Text(
                                          '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.listOrder[index].listProduct[index1].price)}',
                                          style: TextStyle(
                                            fontSize: AppFontSize.fontText,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: AppTextStyles.fontFamilyRoboto,
                                            color: AppColors.gray02,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          'x${_viewModel.listOrder[index].listProduct[index1].amount}',
                                          style: TextStyle(
                                            fontSize: AppFontSize.fontText,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: AppTextStyles.fontFamilyRoboto,
                                            color: AppColors.gray03,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 16,),
                      Divider(
                        height: 1,
                        color: AppColors.gray09,
                      ),
                    ],
                  ),
                )
                )
            ),
          )
          )
      ),
    );
  }

  Widget _buildPay(){
    return Container(
      width: Get.width,
      margin: EdgeInsets.only(left: 16, top: 16,bottom: 16, right: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              "Hình thức thanh toán",
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                color: AppColors.gray01
              ),
            ),
          ),
          SizedBox(height: 8,),
          Container(
            child: Text(
              "Thanh toán bằng tiền mặt",
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: AppColors.gray03
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildTotal(){
    return Container(
      width: Get.width,
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.only(left: 16, right: 16),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "Tạm tính",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  ),
                  Container(
                    child: Text(
                      "Phí vận chuyển",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  ),
                  Container(
                    child: Text(
                      "Tổng cộng",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 12, bottom: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    child: Text(
                      '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.Total())}',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  ),
                  Container(
                    child: Text(
                      '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(15000)}',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  ),
                  Container(
                    child: Text(
                      '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.Total() + 15000)}',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.redPrice,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildNavigateBar(){
    return Container(
      height: 84,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
              color: AppColors.gray05,
              blurRadius: 10
          )]
      ),
      child: Container(
        margin: EdgeInsets.only(left: 20, top: 9, right: 20, bottom: 25),
        child: TextButton(
            onPressed: (){
            },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(AppColors.mint),
            ),
            child: Text(
              "Mua lại",
              style: TextStyle(
                  fontSize: AppFontSize.fontText,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: Colors.white
              ),
            )
        ),
      ),
    );
  }
}
