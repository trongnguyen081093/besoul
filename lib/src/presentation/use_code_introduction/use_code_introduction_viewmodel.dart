
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class UseCodeIntroductionViewModel extends BaseViewModel {
  final TextEditingController textController = TextEditingController();
  ValueNotifier<bool> isButtonDisable = ValueNotifier(false);

  init() async {
    textController.addListener(() {
        isButtonDisable.value = textController.text.length == 7;
    });
  }

  @override
  void dispose() {
    super.dispose();
    textController.dispose();
    isButtonDisable.dispose();
  }
}
