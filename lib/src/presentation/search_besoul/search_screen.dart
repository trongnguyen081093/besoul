import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/search_besoul/search_viewmodel.dart';
import 'package:flutter_app/src/utils/app_prefs.dart';
import 'package:flutter_app/src/utils/utils.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../presentation.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<SearchScreen> {
  late SearchViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SearchViewModel>(
        viewModel: SearchViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          _viewModel.isChangeWidget ? _buildListProduct() : _buildListSearch(),
        ],
      ),
    );
  }

  Widget _buildAppBar() {
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 24,
            height: 24,
            margin: EdgeInsets.only(left: 16, top: 6),
            child: IconButton(
                onPressed: () {
                  Get.toNamed(Routers.productDetail);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white)),
          ),
          Container(
              width: 280,
              height: 36,
              margin: EdgeInsets.only(left: 8, top: 22),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20), color: Colors.white),
              child: TextField(
                onSubmitted: (keyword) {
                  _viewModel.onSearch(keyword);
                  _viewModel.onSearchProduct(keyword);
                  _viewModel.changeWidget();
                },
                controller: _viewModel.searchController,
                decoration: InputDecoration(
                  isDense: true,
                  border: InputBorder.none,
                  prefixIcon: Container(
                    margin: EdgeInsets.only(left: 16, right: 0),
                    child: Icon(
                      Icons.search,
                      size: 20,
                      color: AppColors.gray04,
                    ),
                  ),
                  contentPadding: EdgeInsets.only(left: 8, top: 8, bottom: 8),
                  hintText: 'Tìm kiếm sản phẩm',
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    color: AppColors.gray03,
                  ),
                  suffixIcon: Align(
                      widthFactor: 1,
                      heightFactor: 1,
                      child: ValueListenableBuilder<bool>(
                        valueListenable: _viewModel.isIconDisable,
                        builder: (context, value, child) => IconButton(
                          onPressed: () {
                            _viewModel.searchController.text = "";
                            _viewModel.changeWidget();
                          },
                          icon: Icon(value ? Icons.cancel : null,
                              color: AppColors.gray04, size: 12),
                        ),
                      )),
                ),
                style: TextStyle(color: AppColors.gray01),
              ))
        ],
      ),
    );
  }

  Widget _buildListSearch() {
    return StreamBuilder<List<String>>(
        stream: AppPrefs.watchHistorySearch,
        builder: (context, snapshot) {
          List<String> listSearch = AppPrefs.historySearch;
          return Container(
            margin: EdgeInsets.only(top: 14),
            child: Column(
              children: [
                listSearch.isNotEmpty ? Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 16, right: 16),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              "Lịch sử tìm kiếm",
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                color: AppColors.gray02,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              _viewModel.delSearch();
                            },
                            child: Container(
                              width: 32,
                              height: 32,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: AppColors.gray06, width: 1)),
                              child: Center(
                                  child: Image.asset(
                                    AppImages.png("icon_recycle"),
                                    width: 14,
                                    height: 14,
                                  )),
                            ),
                          )
                        ],
                      ),
                    ),
                    _buildHistorySearch(listSearch),
                    Container(
                      margin: EdgeInsets.only(top: 18),
                      height: 5,
                      color: AppColors.gray10,
                    ),
                  ],
                ) : SizedBox(),
                _buildHotProduct(),
                SizedBox(height: 16,),
                _buildTrademark()
              ],
            ),
          );
        }
        );
  }

  Widget _buildListProduct(){
    return StreamBuilder<List<Product>>(
      stream: _viewModel.searchProduct.stream,
        builder: (context, snapshot){
          List<Product> listSearchProduct = snapshot.data??[];
          return Container(
            width: Get.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: List.generate(listSearchProduct.length, (index1) => Container(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only( left: 16, right: 16, bottom: 8, top: 8),
                      child: Row(
                        children: [
                          Container(
                            width: 88,
                            height: 88,
                            child: Image.asset(
                              listSearchProduct[index1].imgProduct,
                              fit: BoxFit.fill,
                            ),
                          ),
                          Container(
                            width: 228,
                            margin: EdgeInsets.only(left: 12),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  child: Text(
                                    listSearchProduct[index1].nameProduct,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: AppColors.gray01,
                                        fontSize: 13,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: AppTextStyles.fontFamilyRoboto
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 4),
                                  child: Row(
                                    children: [
                                      Row(
                                        children: List.generate(5, (index2) =>  Container(
                                          child: listSearchProduct[index1].rateStar >= index2 + 1 ?
                                          Icon(
                                              Icons.star, size: 15, color: AppColors.star
                                          ) : listSearchProduct[index1].rateStar == 0 ? null :
                                          Icon(
                                              Icons.star, size: 15, color: AppColors.gray05
                                          ),
                                        ),
                                        ),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(left: 8),
                                        child: Text(
                                          listSearchProduct[index1].totalRate > 0 ? '(${listSearchProduct[index1].totalRate})' : "",
                                          style: TextStyle(
                                              color: AppColors.gray03,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: AppTextStyles.fontFamilyRoboto
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only( top: 10),
                                  child: Row(
                                    children: [
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(listSearchProduct[index1].priceSale)}',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color: AppColors.redPrice,
                                              fontSize: AppFontSize.fontText,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: AppTextStyles.fontFamilyRoboto
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 6),
                                        child: Text(
                                          '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(listSearchProduct[index1].priceProduct)}',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color: AppColors.gray03,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: AppTextStyles.fontFamilyRoboto,
                                              decoration: TextDecoration.lineThrough
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 1,
                      color: AppColors.gray10,
                    )
                  ],
                ),
              )
              ),
            ),
          );
        }
    );
  }

  Widget _buildHistorySearch(List<String> listSearch){
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              child: Container(
                height: _viewModel.isChangeHeight ? 60 : 30,
                child: Wrap(
                  clipBehavior: Clip.antiAlias,
                  runSpacing: 8,
                  children: List.generate(
                      listSearch.length,
                          (index) => GestureDetector(
                        onTap: () {
                          _viewModel.onSearchProduct(_viewModel.changeSearch(index));
                          _viewModel.changeWidget();
                        },
                        child: Container(
                          margin: EdgeInsets.only(right: 8),
                          padding: EdgeInsets.only(
                              left: 10,
                              right: 10,
                              top: 4,
                              bottom: 4),
                          decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.circular(16),
                              color: AppColors.gray11),
                          child: Text(
                            listSearch[index],
                            style:
                            TextStyle(color: AppColors.gray03),
                          ),
                        ),
                      )),
                ),
              )),
          GestureDetector(
            onTap: () {
              _viewModel.changeHeight();
            },
            child: Container(
              margin: EdgeInsets.only(right: 3),
              width: 24,
              height: 24,
              child: _viewModel.isChangeHeight
                  ? Icon(
                Icons.keyboard_arrow_up,
                color: AppColors.gray04,
              )
                  : Icon(
                Icons.keyboard_arrow_down,
                color: AppColors.gray04,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHotProduct(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 16),
          child: Text(
            "Sản phẩm nổi bật",
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              fontFamily: AppTextStyles.fontFamilyRoboto,
              color: AppColors.gray02,
            ),
          ),
        ),
        SizedBox(height: 16,),
        Container(
          height: 162,
          margin: EdgeInsets.only(left: 16, right: 16),
          child: Wrap(
            clipBehavior: Clip.antiAlias,
            runSpacing: 10,
            spacing: 10,
            children: List.generate(_viewModel.listProduct.length, (index) => Container(
              width: 159,
              height: 44,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                border: Border.all(color: AppColors.gray11)
              ),
              child: Row(
                children: [
                  Container(
                    width: 44,
                    height: 44,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(2), bottomLeft: Radius.circular(2))
                    ),
                    child: Image.asset(_viewModel.listProduct[index].imgProduct, fit: BoxFit.cover,),
                  ),
                  Container(
                    width: 105,
                    margin: EdgeInsets.only(left: 8),
                    child: Text(
                      _viewModel.listProduct[index].nameProduct,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
            )
            )
          ),
        ),
        SizedBox(height: 16,),
        Container(
          height: 5,
          color: AppColors.gray10,
        )
      ],
    );
  }

  Widget _buildTrademark(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 16),
          child: Text(
            "Thương hiệu",
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              fontFamily: AppTextStyles.fontFamilyRoboto,
              color: AppColors.gray02,
            ),
          ),
        ),
        SizedBox(height: 16,),
        Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: Wrap(
            spacing: 26,
            runSpacing: 23,
            children: List.generate(_viewModel.listTrademark.length, (index) => Container(
              child: Column(
                children: [
                  Container(
                    width: 60,
                    height: 60,
                    child: Image.asset(_viewModel.listTrademark[index].imgTrademark, fit: BoxFit.cover,),
                  ),
                  SizedBox(height: 8,),
                  Container(
                    child: Text(
                      _viewModel.listTrademark[index].nameTrademark,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                  )
                ],
              ),
            ))
          ),
        ),
      ],
    );
  }
}
