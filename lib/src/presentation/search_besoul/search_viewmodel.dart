import 'package:flutter/cupertino.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:flutter_app/src/utils/utils.dart';
import 'package:get/get.dart';
import 'package:rxdart/rxdart.dart';


import '../presentation.dart';

class SearchViewModel extends BaseViewModel {
  init() async {
    searchController.addListener(() {
      showIcon();
    });
  }

  TextEditingController searchController = TextEditingController();

  bool isChangeHeight = false;

  bool isChangeWidget = false;

  ValueNotifier<bool> isIconDisable = ValueNotifier(false);

  var searchProduct = BehaviorSubject<List<Product>>();

  void changeHeight(){
    if(isChangeHeight == false){
      isChangeHeight = true;
      notifyListeners();
    } else {
      isChangeHeight = false;
      notifyListeners();
    }
  }

  changeWidget(){
    if(searchController.text.isEmpty){
      isChangeWidget = false;
      notifyListeners();
    } else{
      isChangeWidget = true;
      notifyListeners();
    }
  }

  onSearchProduct(String keyword){
    List<Product> result = [];
    for (int i = 0; i < listProduct.length; i++){
      if(listProduct[i].nameProduct.trim().toLowerCase().contains(keyword.trim().toLowerCase())){
        result.add(listProduct[i]);
      }
    }
    searchProduct.add(result);
  }

  onSearch(String keyword) {
    // save local
    bool isExist = false;
    List<String> temp = AppPrefs.historySearch;
    for(int i=0;i<temp.length;i++) {
      if(temp[i] == keyword || keyword.isEmpty) {
        isExist = true;
        break;
      }
    }
    if(!isExist) temp.add(keyword);
    AppPrefs.historySearch = temp;
  }

  void delSearch(){
    if(AppPrefs.historySearch.isNotEmpty){
      AppPrefs.historySearch = [];
    }
  }

  String changeSearch(int index){
   return searchController.text = AppPrefs.historySearch[index];
  }

  void showIcon(){
    if (searchController.text.isNotEmpty){
      isIconDisable.value = true;
    } else {
      isIconDisable.value = false;
    }
  }

  List<Product> listProduct = [
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Innisfree Green Tea Seed Serum 80ml",
        rateStar: 4,
        totalRate: 300,
        priceSale: 300000,
        priceProduct: 350000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 3,
        totalRate: 100,
        priceSale: 500000,
        priceProduct: 550000),
    Product(imgProduct: AppImages.png("home_inazuma1"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 3,
        totalRate: 100,
        priceSale: 400000,
        priceProduct: 550000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 5,
        totalRate: 100,
        priceSale: 450000,
        priceProduct: 650000),
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 2,
        totalRate: 100,
        priceSale: 300000,
        priceProduct: 500000),
    Product(imgProduct: AppImages.png("home_inazuma1"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 4,
        totalRate: 100,
        priceSale: 450000,
        priceProduct: 500000),
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Innisfree Green Tea Seed Serum 80ml",
        rateStar: 4,
        totalRate: 300,
        priceSale: 300000,
        priceProduct: 350000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 3,
        totalRate: 100,
        priceSale: 500000,
        priceProduct: 550000),
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Innisfree Green Tea Seed Serum 80ml",
        rateStar: 4,
        totalRate: 300,
        priceSale: 300000,
        priceProduct: 350000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansing...",
        rateStar: 3,
        totalRate: 100,
        priceSale: 500000,
        priceProduct: 550000),
  ];
  
  List<Trademark> listTrademark = [
    Trademark(imgTrademark: AppImages.png("mask1"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask2"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask3"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask4"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask5"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask6"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask7"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask8"), nameTrademark: "Innisfree")
  ];
}

class Product{
  final String imgProduct;
  final String nameProduct;
  final int rateStar;
  final int totalRate;
  final int priceSale;
  final int priceProduct;

  Product({Key ? key, required this.imgProduct, required this.nameProduct, required this.rateStar,
    required this.totalRate, required this.priceSale, required this.priceProduct,});
}

class Trademark{
  final String imgTrademark;
  final String nameTrademark;
  
  Trademark({Key ? key, required this.imgTrademark, required this.nameTrademark});
}

