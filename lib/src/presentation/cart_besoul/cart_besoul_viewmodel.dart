import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class CartBesoulViewModel extends BaseViewModel {
  init() async {}
  
  List<ListCart> listCart = [
    ListCart(checkCard: false, imgProduct: AppImages.png("product_sua_rua_mat"), nameProduct: "Innisfree Green Tea Seed Serum 80ml", price: 450000, count: 1),
    ListCart(checkCard: false, imgProduct: AppImages.png("home_inazuma"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Clea...", price: 550000, count: 1),
    ListCart(checkCard: false, imgProduct: AppImages.png("home_inazuma1"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Clea...", price: 350000, count: 1),
  ];

  List<ListCart> countProduct(){
    List<ListCart> temp = [];
    for(var count in listCart){
      if(count.checkCard == true){
        temp.add(count);
      }
    }
    return temp;
  }

  int totalPrice(){
    int result = 0;
    for(var total in countProduct()){
      result = result + (total.count * total.price);
    }
    return result;
  }

  void deleteProduct(int index){
    listCart.removeAt(index);
    notifyListeners();
  }

}

class ListCart {
  bool checkCard;
  String imgProduct;
  String nameProduct;
  int price;
  int count;

  ListCart({Key ? key, required this.checkCard, required this.imgProduct, 
    required this.nameProduct, required this.price, required this.count});
}
