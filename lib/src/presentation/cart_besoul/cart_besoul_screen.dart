import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/cart_besoul/cart_besoul_viewmodel.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../presentation.dart';

class CartBesoulScreen extends StatefulWidget {
  const CartBesoulScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<CartBesoulScreen> {
  late CartBesoulViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CartBesoulViewModel>(
        viewModel: CartBesoulViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: AppColors.gray06,
            bottomNavigationBar: _buildNavigateBar(),
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          _buildListProduct()
        ],
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.productDetail);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 75, top: 30),
            child: Text(
              "Giỏ hàng",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          GestureDetector(
            onTap: (){

            },
            child: Container(
              margin: EdgeInsets.only(left: 80, top: 36, right: 16),
              child: Text(
                "Thêm",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    fontFamily: AppTextStyles.fontFamilyRoboto
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildListProduct(){
    return Container(
      color: Colors.white,
      child: Column(
        children: List.generate(_viewModel.listCart.length, (index) => Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
          child: Column(
            children: [
              Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          if(_viewModel.listCart[index].checkCard == false){
                            _viewModel.listCart[index].checkCard = true;
                          } else {
                            _viewModel.listCart[index].checkCard = false;
                          }
                        });
                      },
                      child: Container(
                        width: 16,
                        height: 16,
                        child: _viewModel.listCart[index].checkCard
                        ? Image.asset(AppImages.png("icon_check_2"))
                        : Image.asset(AppImages.png("icon_uncheck1"))
                      ),
                    ),
                    Container(
                      width: 77,
                      height: 77,
                      margin: EdgeInsets.only(left: 10),
                      child: Image.asset(
                        _viewModel.listCart[index].imgProduct,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      width: 212,
                      margin: EdgeInsets.only(left: 12),
                      child: Column(
                        children: [
                          Container(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Text(
                                    _viewModel.listCart[index].nameProduct,
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: AppTextStyles.fontFamilyRoboto,
                                      color: AppColors.gray01
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: (){
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) => AlertDialog(
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(8)
                                        ),
                                        buttonPadding: EdgeInsets.all(0),
                                        backgroundColor: Colors.white,
                                        title: Center(
                                          child: Text(
                                            "Bạn có chắc rằng muốn xoá sản phẩm",
                                            style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: AppTextStyles.fontFamilyRoboto,
                                              color: AppColors.gray01
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        content: Text(
                                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Orci, ut vulputate turpis",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: AppTextStyles.fontFamilyRoboto,
                                              color: AppColors.gray03
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                        actionsPadding: EdgeInsets.zero,
                                        actions: <Widget>[
                                          Container(
                                            child: TextButton(
                                              onPressed: (){
                                                Navigator.pop(context, 'Cancel');
                                              },
                                              child: Text(
                                                "Hủy",
                                                style: TextStyle(
                                                    fontSize: AppFontSize.fontText,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: AppTextStyles.fontFamilyRoboto,
                                                    color: AppColors.mint
                                                ),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                              border: Border.all(color: AppColors.gray09),
                                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8))
                                            ),
                                            width: 140,
                                          ),
                                          Container(
                                            child: TextButton(
                                              onPressed: (){
                                                _viewModel.deleteProduct(index);
                                                Navigator.pop(context, 'OK');
                                              },
                                              child: Text(
                                                  "Xoá",
                                                style: TextStyle(
                                                    fontSize: AppFontSize.fontText,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: AppTextStyles.fontFamilyRoboto,
                                                    color: AppColors.mint
                                                ),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                border: Border.all(color: AppColors.gray09),
                                                borderRadius: BorderRadius.only(bottomRight: Radius.circular(8))
                                            ),
                                            width: 140,
                                          )
                                        ],
                                      )
                                    );
                                  },
                                  child: Container(
                                    width: 10,
                                    height: 10,
                                    child: Image.asset(AppImages.png("icon_x")),
                                  ),
                                )
                              ],
                            ),
                            width: 213,
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 18),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Text(
                                      '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.listCart[index].price)}',
                                    style: TextStyle(
                                      fontSize: AppFontSize.fontText,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: AppTextStyles.fontFamilyRoboto,
                                      color: AppColors.gray01
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(left: 16),
                                          width: 32,
                                          height: 32,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(topLeft: Radius.circular(4), bottomLeft: Radius.circular(4)),
                                              color: AppColors.gray06
                                          ),
                                          child: IconButton(
                                            onPressed: (){
                                              if(_viewModel.listCart[index].count >= 1){
                                                setState(() {
                                                  _viewModel.listCart[index].count --;
                                                });
                                              }
                                            },
                                            icon: Icon(Icons.remove, color: AppColors.gray03, size: 15,),
                                          )
                                      ),
                                      Container(
                                          width: 40,
                                          height: 32,
                                          decoration: BoxDecoration(
                                              border: Border.all(color: AppColors.gray06)
                                          ),
                                          child: Center(
                                            child: Text(
                                              '${_viewModel.listCart[index].count }',
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: AppTextStyles.fontFamilyRoboto,
                                                  color: AppColors.gray01
                                              ),
                                            ),
                                          )
                                      ),
                                      Container(
                                          width: 32,
                                          height: 32,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(topRight: Radius.circular(4), bottomRight: Radius.circular(4)),
                                              color: AppColors.gray06
                                          ),
                                          child: IconButton(
                                            onPressed: (){
                                              setState((){
                                                _viewModel.listCart[index].count ++;
                                              });
                                            },
                                            icon: Icon(Icons.add, color: AppColors.gray03, size: 15,),
                                          )
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                height: 1,
                color: AppColors.gray06,
              )
            ],
          ),
        )
        )
      ),
    );
  }

  Widget _buildNavigateBar(){
    return Container(
      height: 133,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
              color: AppColors.gray05,
              blurRadius: 10
          )]
      ),
      child: Column(
        children: [
          Container(
              width: Get.width,
              child: Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 14, bottom: 14),
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: 8),
                      child: Image.asset(AppImages.png("icon_voucher"),
                        width: 20,
                        height: 13,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "BeSoul Voucher",
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray01,
                        ),
                      ),
                    ),
                    GestureDetector(
                      child: Container(
                        width: 24,
                        height: 24,
                        child: Icon(Icons.navigate_next,
                          color: AppColors.gray04,
                        ),
                      ),
                    ),
                  ],
                ),
              )
          ),
          Container(
            height: 1,
            color: AppColors.gray06,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Text(
                                  'Tổng tiền (${_viewModel.countProduct().length} sản phẩm)',
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: AppTextStyles.fontFamilyRoboto,
                                      color: AppColors.gray01
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8),
                                child: Text(
                                  '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.totalPrice())}',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: AppTextStyles.fontFamilyRoboto,
                                    color: AppColors.redPrice,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ]
                  ),
                ),
                Container(
                  width: 171,
                  height: 40,
                  margin: EdgeInsets.only(right: 16, top: 15),
                  child: TextButton(
                      onPressed: (){
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(AppColors.mint),
                      ),
                      child: Text(
                        "Đặt hàng",
                        style: TextStyle(
                            fontSize: AppFontSize.fontText,
                            fontWeight: FontWeight.w500,
                            fontFamily: AppTextStyles.fontFamilyRoboto,
                            color: Colors.white
                        ),
                      )
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
