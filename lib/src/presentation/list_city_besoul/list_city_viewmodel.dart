import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import '../presentation.dart';

class ListCityViewModel extends BaseViewModel {
  init() async {
    listCity.sort();
    result.add(listCity);
    groupMyList();
  }

  TextEditingController search = TextEditingController();
  var result = BehaviorSubject<List<String>>();

  List<String> listCity = [
    "Hà Nội",
    "Huế",
    "Hà Giang",
    "Đà Lạt",
    "Tiền Giang",
    "Hải Phòng",
    "Quảng Bình",
    "Thành Phố Hồ Chí Minh",
    "Phú Yên",
    "Nghệ An",
    "Bình Định",
    "KonTum",
    "Hậu Giang",
    "Đà Nẵng",
    "Quảng Nam",
    "Thanh Hóa",
    "Quảng Trị",
    "Khánh Hòa",
    "An Giang",
    "Bạc Liêu",
    "Cần Thơ",
    "Vũng Tàu",
  ];

  Map<String, List<String>> groupMyList() {
    Map<String, List<String>> groupedLists = {};
    result.value.forEach((city) {
      if (groupedLists['${city[0]}'] == null) {
        groupedLists['${city[0]}'] = <String>[];
      }
      groupedLists['${city[0]}']?.add(city);
    });
    return groupedLists;
  }

  void searchList(String keyWord){
    List<String> temp = [];
    listCity.forEach((city) {
      if(city.toLowerCase().contains(keyWord.toLowerCase()))temp.add(city);
    });
    result.add(temp);
  }
}

