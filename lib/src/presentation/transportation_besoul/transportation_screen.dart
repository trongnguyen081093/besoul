import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/transportation_besoul/transportation_viewmodel.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../presentation.dart';

class TransportationScreen extends StatefulWidget {
  const TransportationScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<TransportationScreen> {
  late TransportationViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<TransportationViewModel>(
        viewModel: TransportationViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          SizedBox(height: 8,),
          _buildList(),
          SizedBox(height: 16,),
          _buildListProduct()
        ],
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Center(
        child: Container(
          margin: EdgeInsets.only( top: 30),
          child: Text(
            "Vận chuyển",
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500,
                fontFamily: AppTextStyles.fontFamilyRoboto
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildList(){
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
            color: AppColors.gray06,
            offset: const Offset(0, 3),
            blurRadius: 2,
            spreadRadius: 0.05,
          )]
      ),
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: List.generate(_viewModel.listApp.length, (index) => Container(
            margin: EdgeInsets.only(right: 20),
            child: GestureDetector(
              onTap: (){
               _viewModel.changeIndex(index);
              },
              child: Column(
                children: [
                  Container(
                    child: Text(
                      _viewModel.listApp[index],
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          fontWeight: FontWeight.w400,
                          color: _viewModel.indexItem == index ? AppColors.mint : AppColors.gray01
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  _viewModel.indexItem == index ?
                  Container(
                    width: _viewModel.sizeContainer(index),
                    height: 2,
                    color: AppColors.mint,
                  ) :
                  SizedBox()
                ],
              ),
            ),
          )
          ),
        ),
      ),
    );
  }

  Widget _buildListProduct(){
    return Container(
      child: Column(
        children: List.generate(_viewModel.selectList().length, (index) => Container(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        'Đơn ngày: ${_viewModel.selectList()[index].date}',
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray01
                        ),
                      )
                    ),
                    Container(
                      child: Text(
                        _viewModel.selectList()[index].status,
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontFamily: AppTextStyles.fontFamilyRoboto,
                            color: AppColors.redPrice
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 8,),
              Container(
                height: 1,
                color: AppColors.gray10,
              ),
              SizedBox(height: 16,),
              Container(
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 77,
                      height: 77,
                      child: Image.asset(_viewModel.selectList()[index].imgProduct, fit: BoxFit.cover,),
                    ),
                    Container(
                      width: 251,
                      padding: EdgeInsets.only(left: 12,),
                      child: Column(
                        children: [
                          Container(
                            child: Text(
                              _viewModel.selectList()[index].nameProduct,
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: AppTextStyles.fontFamilyRoboto,
                                  color: AppColors.gray01
                              ),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          SizedBox(height: 26,),
                          Container(
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.selectList()[index].price)}',
                                    style: TextStyle(
                                        fontSize: AppFontSize.fontText,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray02
                                    ),
                                  )
                                ),
                                Container(
                                  child: Text(
                                    'x${_viewModel.selectList()[index].amount}',
                                    style: TextStyle(
                                        fontSize: AppFontSize.fontText,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray03
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: 26,),
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(2),
                                    border: Border.all(color: AppColors.mint)
                                  ),
                                  width: 115,
                                  height: 40,
                                  child: TextButton(
                                    onPressed: (){
                                      if(_viewModel.selectList()[index].status == "Đã tiếp nhận" || _viewModel.selectList()[index].status == "Đã giao" || _viewModel.selectList()[index].status == "Đã hoàn thành")
                                      {
                                        Get.toNamed(Routers.detailOrder);
                                      }
                                      else if(_viewModel.selectList()[index].status == "Đơn mới" ){
                                        Get.toNamed(Routers.detailOrder1);
                                      }
                                    },
                                    child: Text(
                                      "Chi tiết",
                                      style: TextStyle(
                                          fontSize: AppFontSize.fontText,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: AppTextStyles.fontFamilyRoboto,
                                          color: AppColors.mint
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 8,),
                                Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(2),
                                      border: Border.all(color: AppColors.mint)
                                  ),
                                  width: 115,
                                  height: 40,
                                  child: TextButton(
                                    onPressed: (){
                                      if(_viewModel.selectList()[index].status == "Đã tiếp nhận" || _viewModel.selectList()[index].status == "Đã giao")
                                        {
                                          Get.toNamed(Routers.orderTracking);
                                        }
                                      else if(_viewModel.selectList()[index].status == "Đơn mới" ){
                                          Get.toNamed(Routers.detailOrder1);
                                      }
                                    },
                                    child: Text(
                                      '${_viewModel.changeStatus(_viewModel.selectList()[index].status)}',
                                      style: TextStyle(
                                          fontSize: AppFontSize.fontText,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: AppTextStyles.fontFamilyRoboto,
                                          color: AppColors.mint
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 16,),
              Container(
                margin: EdgeInsets.only(bottom: 16),
                height: 5,
                color: AppColors.gray10,
              )
            ],
          ),
        )),
      ),
    );
  }
}
