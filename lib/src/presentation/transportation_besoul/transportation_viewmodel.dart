

import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class TransportationViewModel extends BaseViewModel {
  init() async {}

  int indexItem = 0;
  List<String> listApp = ["Tất cả", "Đơn mới", "Đã tiếp nhận", "Đã giao", "Đã hoàn thành"];

  double sizeContainer(int index){
    switch (index){
      case 0:
        return 70;
      case 1:
        return 85;
      case 2:
        return 110;
      case 3:
        return 80;
      case 4:
        return 123;
      default:
        return 150;
    }
  }

  String changeStatus(String status){
    switch(status){
      case "Đơn mới":
        return "Hủy hàng";
        break;
      case "Đã tiếp nhận":
        return "Theo dõi";
        break;
      case "Đã giao":
        return "Theo dõi";
        break;
      case "Đã hoàn thành":
        return "Mua lại";
        break;
      default:
        return "Theo dõi";
        break;
    }
  }

  void changeIndex(int index){
    indexItem = index;
    notifyListeners();
  }
  
  List<ListTransportation> listTransportation = [
    ListTransportation(imgProduct: AppImages.png("product_detail"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "07/04/2022", status: "Đơn mới", price: 450000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("product_detail_1"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "27/03/2022", status: "Đơn mới", price: 550000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("product_detail_clothel"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "18/03/2022", status: "Đơn mới", price: 600000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("home_inazuma"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "01/04/2022", status: "Đơn mới", price: 500000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("home_inazuma1"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "03/03/2022", status: "Đơn mới", price: 250000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("product_detail"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "07/04/2022", status: "Đã tiếp nhận", price: 550000, amount: 2),
    ListTransportation(imgProduct: AppImages.png("product_detail_1"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "27/03/2022", status: "Đã tiếp nhận", price: 500000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("product_detail_clothel"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "18/03/2022", status: "Đã tiếp nhận", price: 650000, amount: 3),
    ListTransportation(imgProduct: AppImages.png("home_inazuma"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "01/04/2022", status: "Đã tiếp nhận", price: 500000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("home_inazuma1"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "03/03/2022", status: "Đã tiếp nhận", price: 250000, amount: 1),

    ListTransportation(imgProduct: AppImages.png("product_detail"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "07/04/2022", status: "Đã giao", price: 350000, amount: 2),
    ListTransportation(imgProduct: AppImages.png("product_detail_1"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "27/03/2022", status: "Đã giao", price: 550000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("product_detail_clothel"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "18/03/2022", status: "Đã giao", price: 600000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("home_inazuma"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "01/04/2022", status: "Đã giao", price: 550000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("home_inazuma1"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "03/03/2022", status: "Đã giao", price: 250000, amount: 1),

    ListTransportation(imgProduct: AppImages.png("product_detail"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "07/04/2022", status: "Đã hoàn thành", price: 450000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("product_detail_1"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "27/03/2022", status: "Đã hoàn thành", price: 550000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("product_detail_clothel"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "18/03/2022", status: "Đã hoàn thành", price: 600000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("home_inazuma"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "01/04/2022", status: "Đã hoàn thành", price: 500000, amount: 1),
    ListTransportation(imgProduct: AppImages.png("home_inazuma1"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", date: "03/03/2022", status: "Đã hoàn thành", price: 250000, amount: 1),
  ];

  List<ListTransportation> selectList(){
    switch(indexItem){
      case 0:
        return listTransportation.toList();
        break;
      case 1:
        return listTransportation.where((i) => i.status == ("Đơn mới")).toList();
        break;
      case 2:
        return listTransportation.where((i) => i.status == ("Đã tiếp nhận")).toList();
        break;
      case 3:
        return listTransportation.where((i) => i.status == ("Đã giao")).toList();
        break;
      case 4:
        return listTransportation.where((i) => i.status == ("Đã hoàn thành")).toList();
        break;
      default:
        return listTransportation.toList();
        break;
    }
  }
}

class ListTransportation{
  final String imgProduct;
  final String nameProduct;
  final String date;
  final String status;
  final int price;
  final int amount;
  
  ListTransportation({Key ? key, required this.imgProduct, required this.nameProduct, required this.date, required this.status,
  required this.price, required this.amount});  
}
