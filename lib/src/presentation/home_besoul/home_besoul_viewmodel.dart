import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class HomeBesoulViewModel extends BaseViewModel {
  init() async {
    scrollController.addListener(() {
     if(scrollController.offset == scrollController.position.maxScrollExtent){
       showFloatButton.value = true;
     }
      else{
        showFloatButton.value = false;
     }
    });
  }
  var index;
  final CarouselController slideController = CarouselController();
  final ScrollController scrollController = ScrollController();
  int indexSlide = 0;
  int rate = 0;
  ValueNotifier<bool> showFloatButton = ValueNotifier(false);


  List<PickLanguage> items = [
    PickLanguage(
        imgCountry: AppImages.imgFlagUnitedKingdom, nameCountry: "English"),
    PickLanguage(
        imgCountry: AppImages.imgFlagVietNam, nameCountry: "Tiếng Việt"),
    PickLanguage(imgCountry: AppImages.imgFlagSouthKorea, nameCountry: "한국어"),
  ];

  List<ListIcon> icons1 = [
    ListIcon(imgIcon: AppImages.png("home_mykol"), name: "My KOL"),
    ListIcon(imgIcon: AppImages.png("home_hot"), name: "Hot"),
    ListIcon(imgIcon: AppImages.png("home_new"), name: "New"),
    ListIcon(imgIcon: AppImages.png("home_beauty"), name: "Beauty"),
  ];

  List<ListIcon> icons2 = [
    ListIcon(imgIcon: AppImages.png("home_fashion"), name: "Fashion"),
    ListIcon(imgIcon: AppImages.png("home_brand"), name: "Brand"),
    ListIcon(imgIcon: AppImages.png("home_health"), name: "Health"),
    ListIcon(imgIcon: AppImages.png("home_food"), name: "Food"),
  ];

  List<Image> images = [
    Image.asset(AppImages.png("home_slide"), width: 343 * scaleWidth,
      height: 127 * scaleHeight,
      fit: BoxFit.cover,),
    Image.asset(AppImages.png("home_inazuma"), width: 343 * scaleWidth,
      height: 127 * scaleHeight,
      fit: BoxFit.cover,),
    Image.asset(AppImages.png("home_slide"), width: 343 * scaleWidth,
      height: 127 * scaleHeight,
      fit: BoxFit.cover,),
    Image.asset(AppImages.png("home_inazuma1"), width: 343 * scaleWidth,
      height: 127 * scaleHeight,
      fit: BoxFit.cover,),
  ];

  List<Product> listProduct = [
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisfhksdabgvkhasbd adsbgaerb",
        rateStar: 4,
        totalRate: 300,
        priceSale: 300000,
        priceProduct: 350000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 0,
        totalRate: 0,
        priceSale: 500000,
        priceProduct: 550000),
    Product(imgProduct: AppImages.png("home_inazuma1"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 1,
        totalRate: 100,
        priceSale: 400000,
        priceProduct: 550000),
  ];

  List<Product> listProduct1 = [
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 4,
        totalRate: 100,
        priceSale: 400000,
        priceProduct: 650000),
    Product(imgProduct: AppImages.png("home_inazuma1"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 0,
        totalRate: 0,
        priceSale: 200000,
        priceProduct: 450000),
    Product(imgProduct: AppImages.png("home_inazuma1"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 1,
        totalRate: 100,
        priceSale: 450000,
        priceProduct: 550000),
  ];

  List<Product> listProduct2 = [
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 5,
        totalRate: 100,
        priceSale: 400000,
        priceProduct: 650000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 3,
        totalRate: 189,
        priceSale: 200000,
        priceProduct: 450000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 4,
        totalRate: 100,
        priceSale: 450000,
        priceProduct: 550000),
  ];

  List<Product> listProduct3 = [
    Product(imgProduct: AppImages.png("home_inazuma1"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 3,
        totalRate: 100,
        priceSale: 600000,
        priceProduct: 650000),
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 4,
        totalRate: 189,
        priceSale: 400000,
        priceProduct: 450000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 4,
        totalRate: 100,
        priceSale: 450000,
        priceProduct: 550000),
  ];

  List<GiftCode> listGift = [
    GiftCode(imgGift: AppImages.png("home_logogift"),
        nameGift: "GIẢM 50%",
        giftApply: "Áp dụng với hóa đơn 150k",
        giftExpiry: "30/03/2022"),
    GiftCode(imgGift: AppImages.png("home_logogift"),
        nameGift: "GIẢM 40%",
        giftApply: "Áp dụng với hóa đơn 100k",
        giftExpiry: "20/03/2022"),
    GiftCode(imgGift: AppImages.png("home_logogift"),
        nameGift: "GIẢM 10%",
        giftApply: "Áp dụng với hóa đơn 50k",
        giftExpiry: "25/03/2022"),
    GiftCode(imgGift: AppImages.png("home_logogift"),
        nameGift: "GIẢM 20%",
        giftApply: "Áp dụng với hóa đơn 70k",
        giftExpiry: "22/03/2022"),
  ];
}


class PickLanguage{
  final String imgCountry;
  final String nameCountry;

  PickLanguage({Key? key, required this.imgCountry, required this.nameCountry});
}

class ListIcon{
  final String imgIcon;
  final String name;

  ListIcon({Key? key, required this.imgIcon, required this.name});
}

class Product{
  final String imgProduct;
  final String nameProduct;
  final int rateStar;
  final int totalRate;
  final int priceSale;
  final int priceProduct;

  Product({Key ? key, required this.imgProduct, required this.nameProduct, required this.rateStar,
                      required this.totalRate, required this.priceSale, required this.priceProduct,});
}

class GiftCode{
  final String imgGift;
  final String nameGift;
  final String giftApply;
  final String giftExpiry;


  GiftCode({Key ? key, required this.imgGift, required this.nameGift, required this.giftApply,
    required this.giftExpiry});
}




