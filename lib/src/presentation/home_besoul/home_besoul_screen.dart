import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/home_besoul/home_besoul_viewmodel.dart';
import 'package:flutter_app/src/presentation/notify_besoul/notify_besoul_viewmodel.dart';
import 'package:flutter_app/src/resource/model/model.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../presentation.dart';

class HomeBesoulScreen extends StatefulWidget {
  const HomeBesoulScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<HomeBesoulScreen> {
  late HomeBesoulViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<HomeBesoulViewModel>(
        viewModel: HomeBesoulViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
            floatingActionButton: ValueListenableBuilder<bool>(
              valueListenable: _viewModel.showFloatButton,
              builder: (context, value, child) =>
              value ? FloatingActionButton(
                onPressed: (){
                  _viewModel.scrollController.jumpTo(0.0);
                },
                  backgroundColor: AppColors.gray01.withOpacity(0.55),
                child: Image.asset(AppImages.png("home_icon_backup"))
              ) : SizedBox(),
            ),
          );
        });
  }

  Widget _buildBody() {
    return Container(
      width: Get.width,
      height: Get.height,
      child: SingleChildScrollView(
        controller: _viewModel.scrollController,
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            _buildSearch(),
            _buildListIcon(),
            _buildSlide(),
            SizedBox(height: 4,),
            _buildBookMark("Beauty - Bí mật toả sáng cùng KOLs", 305),
            _buildProduct(_viewModel.listProduct),
            _buildBookMark("HOT - BEST REVIEW with KOLS ", 270),
            _buildProduct(_viewModel.listProduct1),
            _buildBookMark("GOLD BOX - Giờ vàng khuyến mãi",286 ),
            _buildProduct(_viewModel.listProduct2),
            _buildBookMark("NEW - Khám phá cùng KOLS",250),
            _buildProduct(_viewModel.listProduct3),
            _buildBookMark("Gửi tặng người thương",208),
            _buildGift(),
            _buildBookMark("Event - Chương trình khuyến mãi",277),
            SizedBox(height: 12,),
            WidgetPromotionBesoulState().buildPromotion(3)
          ],
        ),
      ),
    );
  }

  Widget _buildSearch(){
    return Container(
      width: Get.width,
      decoration: BoxDecoration(
        color: AppColors.mint
      ),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: 11, top: 17, right: 0),
                child: Image.asset(
                  AppImages.png("home_logo"),
                  width: 93,
                  height: 22,
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 110, top: 17),
                child: Theme(
                  data: Theme.of(context).copyWith(
                    canvasColor: AppColors.mint,
                  ),
                  child: DropdownButton(
                    underline: Container(
                      color: AppColors.mint,
                    ),
                    value: _viewModel.index,
                    items:_viewModel.items.map((PickLanguage item){
                     return DropdownMenuItem<PickLanguage>(
                       value: item,
                       child: Row(
                         children: [
                            Image.asset(
                              item.imgCountry,
                              height: 20,
                              width: 20,
                            ),
                            SizedBox(width: 5,),
                            Text(
                              item.nameCountry,
                              style: TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                fontFamily: AppTextStyles.fontFamilyRoboto,
                              ),
                            )
                            ],
                           )
                         );
                       }).toList(),
                      onChanged: (item){
                        setState(() {
                          _viewModel.index = item;
                        });
                      } ),
                ),
              ),
              Container(
                width: 38,
                height: 30,
                margin: EdgeInsets.only(top: 17),
                child: Stack(
                  children: [
                    Positioned(
                      child: IconButton(
                        padding: EdgeInsets.all(0),
                        icon: Image.asset(
                          AppImages.png("home_bell"),
                          width: 24,
                          height: 24,
                        ),
                        onPressed: (){},
                      ),
                    ),
                    Positioned(
                        child: GestureDetector(
                          onTap: (){
                            Get.toNamed(Routers.notifyBesoul);
                          },
                          child: Stack(
                            children: [
                              Positioned(
                                left: 15,
                                  child: Image.asset(
                                    AppImages.png("navigation_notify"),
                                    width: 20,
                                    height: 15,
                                  )
                              ),
                              Positioned(
                                  top: 2,
                                  left: 19,
                                  child: Text (
                                  NotifyBesoulViewModel().listNotify.length > 10 ? "${NotifyBesoulViewModel().listNotify.length}" : "0${NotifyBesoulViewModel().listNotify.length}",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 10,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: AppTextStyles.fontFamilyRoboto
                                    ),
                                  )
                              ),
                            ],
                          ),
                        )
                    )
                  ],
                ),
              )
            ],
          ),
          Container(
            height: 34,
            margin: EdgeInsets.only(left: 16, right: 16, bottom: 10, top: 11),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
            ),
            child: TextField(
              style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    isDense: true,
                    border: InputBorder.none,
                    prefixIcon: Icon(Icons.search, size: 20, color: AppColors.gray04,),
                    contentPadding: EdgeInsets.only(left: 40, top: 8, bottom: 8),
                    hintText: 'Tìm kiếm quốc gia',
                    hintStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray04,
                    )
                ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildListIcon(){
    return Container(
      width: Get.width,
      margin: EdgeInsets.only(top: 18),
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List.generate(_viewModel.icons1.length, (index) =>
                  Column(
                    children: [
                      GestureDetector(
                        onTap: (){},
                        child: Image.asset(_viewModel.icons1[index].imgIcon,
                        width: 56,
                        height: 56,
                        fit: BoxFit.fill,),
                        ),
                      SizedBox(height: 8,),
                      Text(_viewModel.icons1[index].name,
                        style: TextStyle(
                          color: AppColors.gray02,
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTextStyles.fontFamilyRoboto
                        ),
                      )
                    ],
                  ),
              )
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List.generate(_viewModel.icons1.length, (index) =>
                  Column(
                    children: [
                      GestureDetector(
                        onTap: (){},
                        child: Image.asset(_viewModel.icons2[index].imgIcon,
                          width: 56,
                          height: 56,
                          fit: BoxFit.fill,),
                      ),
                      SizedBox(height: 8,),
                      Text(_viewModel.icons2[index].name,
                        style: TextStyle(
                            color: AppColors.gray02,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            fontFamily: AppTextStyles.fontFamilyRoboto
                        ),
                      )
                    ],
                  ),
              )
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSlide(){
    return Container(
      width: Get.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4)
      ),
      margin: EdgeInsets.only(left: 16, right: 16, top: 20),
      child: Stack(
        children: [
          Positioned(
            child: CarouselSlider(
             items: _viewModel.images,
             carouselController: _viewModel.slideController,
             options: CarouselOptions(
               autoPlay: true,
               viewportFraction: 1,
               onPageChanged: (index, reason){
                 setState(() {
                   _viewModel.indexSlide = index;
                 });
               }
             )
            ),
          ),
          Positioned(
            bottom: 8,
            left: 126 * scaleWidth,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: _viewModel.images.asMap().entries.map((entry) {
                return GestureDetector(
                  onTap: () => _viewModel.slideController.animateToPage(entry.key),
                  child: Container(
                    width: _viewModel.indexSlide == entry.key ? 21: 7,
                    height: 7,
                    margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                    decoration: BoxDecoration(
                        borderRadius: _viewModel.indexSlide == entry.key ? BorderRadius.circular(15) : BorderRadius.circular(90),
                        color: (Theme.of(context).brightness == Brightness.dark
                            ? Colors.white
                            : Colors.white)
                            .withOpacity(_viewModel.indexSlide == entry.key ? 0.9 : 0.4)
                    ),
                  ),
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildBookMark(String value, int size){
    return Container(
      width: Get.width,
      margin: EdgeInsets.only(top: 16),
      child: Stack(
        children: [
          Positioned(
            child: Image.asset(
              AppImages.png("home_bookmark1"),
              width: size * scaleWidth,
              height: 34 * scaleHeight,
              alignment: Alignment.centerLeft,
              fit: BoxFit.cover,
            ),
          ),
          Positioned(
            top: 6,
            left: 9,
              child: Text(
                value,
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w500,
                  fontSize: AppFontSize.fontText
                ),
              )
          ),
          Positioned(
            top: 8,
            left: 280,
            child: GestureDetector(
              onTap: (){
                Get.toNamed(Routers.promotionBesoul);
              },
              child: Container(
                alignment: Alignment.centerRight,
                child: Text(
                  "Xem thêm >",
                  style: TextStyle(
                      color: AppColors.gray04
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildProduct(List<Product> listProduct){
    return Container(
      margin: EdgeInsets.only(top: 12, left: 16 * scaleWidth),
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: List.generate(listProduct.length, (index) => Container(
            width: 136,
            height: 235,
            margin: EdgeInsets.only(right: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                border: Border.all(color: AppColors.gray06, style: BorderStyle.solid, width: 1)
            ),
            child: Column(
              children: [
                Stack(
                  children: [
                    Image.asset(
                      listProduct[index].imgProduct,
                      width: 136,
                      height: 136,
                      fit: BoxFit.fill,
                    ),
                    Positioned(
                      top: 5,
                      left: 5,
                      child: Image.asset(AppImages.png("product_sale"),
                        width: 31,
                        height: 22,
                      )
                    ),
                    Positioned(
                      top: 9,
                      left: 10,
                      child: Text(
                        '${(100-((listProduct[index].priceSale/listProduct[index].priceProduct)*100)).ceil()}%',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTextStyles.fontFamilyRoboto
                        ),
                      )
                    )
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 6, top: 6, right: 6),
                  child: Text(
                    listProduct[index].nameProduct,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: AppColors.gray01,
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 6, top: 2),
                  child: Row(
                    children: [
                      Row(
                          children: List.generate(5, (index1) =>  Container(
                            child: listProduct[index].rateStar >= index1 + 1 ?
                            Icon(
                                Icons.star, size: 15, color: AppColors.star
                            ) : listProduct[index].rateStar == 0 ? null :
                            Icon(
                                Icons.star, size: 15, color: AppColors.gray05
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Text(
                          listProduct[index].totalRate > 0 ? '(${listProduct[index].totalRate})' : "",
                          style: TextStyle(
                            color: AppColors.gray03,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            fontFamily: AppTextStyles.fontFamilyRoboto
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 6, top: 7),
                  child: Text(
                    '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(listProduct[index].priceSale)}',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: AppColors.redPrice,
                      fontSize: AppFontSize.fontText,
                      fontWeight: FontWeight.w500,
                      fontFamily: AppTextStyles.fontFamilyRoboto
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 6),
                  child: Text(
                    '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(listProduct[index].priceProduct)}',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: AppColors.gray03,
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      decoration: TextDecoration.lineThrough
                    ),
                  ),
                )
              ],
            ),
          )
          )
        ),
      ),
    );
  }

  Widget _buildGift(){
    return Container(
      width: Get.width * scaleWidth,
      margin: EdgeInsets.only(top: 12),
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        child: Row(
          children: List.generate(_viewModel.listGift.length, (index) =>  Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)
              ),
              color: Colors.white,
              shadowColor: AppColors.gray06,
              elevation: 2,
              child:  Row(
                children: [
                  Container(
                    width: 100,
                    height: 107,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(4), bottomLeft: Radius.circular(4)),
                        color: AppColors.mint
                    ),
                    child: Image.asset(
                      _viewModel.listGift[index].imgGift,
                      width: 63,
                      height: 63,
                    ),
                  ),
                  Container(
                    width: 200,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 17, top: 10),
                          child: Text(
                            _viewModel.listGift[index].nameGift,
                            style: TextStyle(
                                fontSize: AppFontSize.fontText,
                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                fontWeight: FontWeight.w500,
                                color: AppColors.gray01
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 17, top: 4),
                          child: Text(
                            _viewModel.listGift[index].giftApply,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                fontWeight: FontWeight.w400,
                                color: AppColors.gray01
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 17, top: 4),
                          child: Text(
                            'HSD: ${_viewModel.listGift[index].giftExpiry}',
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                fontWeight: FontWeight.w400,
                                color: AppColors.gray03
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 17, top: 5),
                          child: GestureDetector(
                            child: Text(
                              "Dùng ngay",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: AppTextStyles.fontFamilyRoboto,
                                  fontWeight: FontWeight.w500,
                                  color: AppColors.mint
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )
          ),)
        ),
      ),
    );
  }

  // Widget _buildPromotion(){
  //   return Column(
  //     children: List.generate(_viewModel.listPromotion.length, (index) => Container(
  //       margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
  //       padding: EdgeInsets.all(0),
  //       width: 343 * scaleWidth,
  //       decoration: BoxDecoration(
  //           borderRadius: BorderRadius.circular(4),
  //           border: Border.all(
  //             color: AppColors.gray06,
  //             width: 1,
  //           )
  //       ),
  //       child: Column(
  //         children: [
  //           Stack(
  //             children: [
  //               Positioned(
  //                 child: Container(
  //                   decoration: BoxDecoration(
  //                       borderRadius: BorderRadius.only(topLeft: Radius.circular(4), topRight: Radius.circular(4))
  //                   ),
  //                   child: Image.asset(_viewModel.listPromotion[index].imgPromotion,
  //                   ),
  //                 ),
  //               ),
  //               Positioned(
  //                 top: 10,
  //                 left: 10,
  //                 child: Container(
  //                   width: 51,
  //                   height: 19,
  //                   decoration: BoxDecoration(
  //                       borderRadius: BorderRadius.circular(20),
  //                       color: AppColors.gray01.withOpacity(0.45)
  //                   ),
  //                   child: Row(
  //                     children: [
  //                       Padding(
  //                         padding: const EdgeInsets.only(left: 9, right: 4),
  //                         child: Icon(
  //                           Icons.visibility_outlined,
  //                           size: 11,
  //                           color: Colors.white,
  //                         ),
  //                       ),
  //                       Text(
  //                         "466",
  //                         style: TextStyle(
  //                             fontSize: 12,
  //                             fontWeight: FontWeight.w400,
  //                             fontFamily: AppTextStyles.fontFamilyRoboto
  //                         ),
  //                       )
  //                     ],
  //                   ),
  //                 ),
  //               )
  //             ],
  //           ),
  //           Padding(
  //             padding: const EdgeInsets.only(left: 8, top: 9),
  //             child: Text(
  //               _viewModel.listPromotion[index].namePromotion,
  //               style: TextStyle(
  //                   color: AppColors.gray01,
  //                   fontWeight: FontWeight.w500,
  //                   fontSize: 14,
  //                   fontFamily: AppTextStyles.fontFamilyRoboto
  //               ),
  //             ),
  //           ),
  //           Row(
  //             children: [
  //               Padding(
  //                 padding: const EdgeInsets.only(left: 8, top: 6),
  //                 child: Icon(
  //                   Icons.calendar_month_outlined,
  //                   color: AppColors.gray03,
  //                   size: 12,
  //                 ),
  //               ),
  //               Padding(
  //                 padding: const EdgeInsets.only(left: 6, top: 6),
  //                 child: Text(
  //                   _viewModel.listPromotion[index].datePromotion,
  //                   style: TextStyle(
  //                       color: AppColors.gray03,
  //                       fontSize: 13,
  //                       fontWeight: FontWeight.w400,
  //                       fontFamily: AppTextStyles.fontFamilyRoboto
  //                   ),
  //                 ),
  //               )
  //             ],
  //           )
  //         ],
  //       ),
  //     )
  //     )
  //   );
  // }
}
