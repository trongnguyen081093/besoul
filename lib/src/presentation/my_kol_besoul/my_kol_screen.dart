import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/my_kol_besoul/my_kol_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class MyKOLScreen extends StatefulWidget {
  const MyKOLScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<MyKOLScreen> {
  late MyKOLViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<MyKOLViewModel>(
        viewModel: MyKOLViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: AppColors.gray09,
          );
        });
  }

  Widget _buildBody() {
    return Column(
      children: [
        _buildAppBar(),
        _buildListUser(),
        SizedBox(height: 4,),
        _buildMyKOLDetail()
      ],
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.createOrder);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 85, top: 30),
            child: Text(
              "My KOL",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListUser(){
    return Container(
      color: Colors.white,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Container(
          margin: EdgeInsets.only(left: 22),
          child: Row(
            children: List.generate(_viewModel.listUser.length, (index) => Container(
              padding: EdgeInsets.only(right: 20, top: 10, bottom: 16),
              child: Column(
                children: [
                  Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: AppColors.mint, width: 2)
                    ),
                    child: CircleAvatar(
                      backgroundImage: AssetImage(_viewModel.listUser[index].imgUser),
                    ),
                  ),
                  Container(
                    child: Text(
                      _viewModel.listUser[index].nameUser,
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray03,
                      ),
                    ),
                  )
                ],
              ),
            )),
          ),
        ),
      ),
    );
  }

  Widget _buildMyKOLDetail(){
    return Container(
      color: Colors.white,
    );
  }
}
