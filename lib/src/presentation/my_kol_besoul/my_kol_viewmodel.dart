import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class MyKOLViewModel extends BaseViewModel {
  init() async {}

  List<ListUser> listUser = [
    ListUser(imgUser: AppImages.png("user_1"), nameUser: "Nguyen Van A"),
    ListUser(imgUser: AppImages.png("user_2"), nameUser: "Nguyen Van B"),
    ListUser(imgUser: AppImages.png("user_3"), nameUser: "Nguyen Van C"),
    ListUser(imgUser: AppImages.png("user_1"), nameUser: "Nguyen Van A"),
    ListUser(imgUser: AppImages.png("user_2"), nameUser: "Nguyen Van B"),
    ListUser(imgUser: AppImages.png("user_3"), nameUser: "Nguyen Van C"),
    ListUser(imgUser: AppImages.png("user_1"), nameUser: "Nguyen Van A"),
    ListUser(imgUser: AppImages.png("user_2"), nameUser: "Nguyen Van B"),
    ListUser(imgUser: AppImages.png("user_3"), nameUser: "Nguyen Van C"),
  ];
}

class ListUser{
  final String imgUser;
  final String nameUser;

  ListUser({Key ? key, required this.imgUser, required this.nameUser});
}
