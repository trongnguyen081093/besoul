import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class SelectCountryViewModel extends BaseViewModel {
  init() async {}

  List<SelectCountry> flag = [
    SelectCountry(imgCountry: AppImages.imgFlagVietNam, nameCountry: "Việt Nam"),
    SelectCountry(imgCountry: AppImages.imgFlagUnitedKingdom, nameCountry: "Vương Quốc Anh"),
    SelectCountry(imgCountry: AppImages.imgFlagUSA, nameCountry: "Hoa Kỳ"),
    SelectCountry(imgCountry: AppImages.imgFlagSouthKorea, nameCountry: "Hàn Quốc"),
    SelectCountry(imgCountry: AppImages.imgFlagJapan, nameCountry: "Nhật Bản"),
    SelectCountry(imgCountry: AppImages.imgFlagBrazil, nameCountry: "Brazil"),
    SelectCountry(imgCountry: AppImages.imgFlagCanada, nameCountry: "Canada"),
    SelectCountry(imgCountry: AppImages.imgFlagGermany, nameCountry: "Đức"),
    SelectCountry(imgCountry: AppImages.imgFlagFrance, nameCountry: "Pháp"),
    SelectCountry(imgCountry: AppImages.imgFlagSweden, nameCountry: "Thụy Điển"),
  ];
}

class SelectCountry{
  final String imgCountry;
  final String nameCountry;

  SelectCountry({Key? key, required this.imgCountry, required this.nameCountry});
}
