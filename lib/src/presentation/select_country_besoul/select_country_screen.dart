import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/main.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/select_country_besoul/select_country_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class SelectCountryScreen extends StatefulWidget {
  const SelectCountryScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<SelectCountryScreen> {
  late SelectCountryViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SelectCountryViewModel>(
        viewModel: SelectCountryViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: AppColors.mint,
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Container(
        width: 375,
        child: Column(
          children: [
            SizedBox(height: 102,),
            Container(
              width: 155,
              child: Text("Chọn quốc gia",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 24,
                  fontFamily: AppTextStyles.fontFamilyRoboto
                ),
              ),
            ),
            SizedBox(height: 12,),
            Container(
              width: 306,
              child: Text(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In at tincidunt risus cursus donec",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                ),
              ),
            ),
            SizedBox(height: 40,),
            _buildSearch(),
            SizedBox(height: 8,),
            _buildCountry()
          ],
        ),
      ),
    );
  }

  Widget _buildSearch(){
    return Container(
      width: 262,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
          style: BorderStyle.solid,
          color: Colors.white)
      ),
      child: TextField(
        decoration: InputDecoration(
          isDense: true,
          border: InputBorder.none,
          prefixIcon: Icon(Icons.search, size: 20, color: Colors.white,),
          contentPadding: EdgeInsets.symmetric(vertical: 15),
          hintText: 'Tìm kiếm quốc gia',
          hintStyle: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 14,
            fontFamily: AppTextStyles.fontFamilyRoboto,
            color: Colors.white,
          )
          )
        ),
      );
  }

  Widget _buildCountry(){
    return SizedBox(
      width: Get.width,
      height: 470,
      child: ListView.builder(
        itemCount: _viewModel.flag.length,
        itemBuilder: (context, index){
          return GestureDetector(
            child: Column(
              children: [
                SizedBox(height: 16,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 93),
                  child: Row(
                    children: [
                      Image.asset(_viewModel.flag[index].imgCountry,
                        width: 20,
                        height: 20,
                      ),
                      SizedBox(width: 16,),
                      Text(
                        _viewModel.flag[index].nameCountry,
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          fontWeight: FontWeight.w400
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 16,),
                Container(
                  width: 231,
                  height: 1,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [Colors.white.withOpacity(0.5), Colors.white, Colors.white.withOpacity(0.5)]
                      )
                  ),
                )
              ],
            ),
            onTap: (){
              Get.toNamed(Routers.loginBesoul);
            },
          );
        }
      ),
    );
  }
}
