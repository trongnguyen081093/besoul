
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';

import '../presentation.dart';

class CreateRateViewModel extends BaseViewModel {
  init() async {}
  File? image;
  VideoPlayerController? video;

  Future pickImage() async {
    try{
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if(image == null) return;
      final imageTemp = File(image.path);
      this.image = imageTemp;
      notifyListeners();
    } on PlatformException catch(e){
      print("fail");
    }
  }

  Future pickVideo() async {
    try{
      final video = await ImagePicker().pickVideo(source: ImageSource.gallery);
      if(video == null) return;
      this.video = VideoPlayerController.file(File(video.path));
      await this.video!.initialize();
      await this.video!.play();
      notifyListeners();
    } on PlatformException catch(e){
      print("fail");
    }
  }

  void deleteImage(){
    this.image = null;
    notifyListeners();
  }

  void deleteVideo(){
    this.video = null;
    notifyListeners();
  }
}
