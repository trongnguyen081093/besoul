import 'dart:io';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/create_rate_besoul/create_rate_viewmodel.dart';
import 'package:get/get.dart';
import 'package:video_player/video_player.dart';

import '../presentation.dart';

class CreateRateScreen extends StatefulWidget {
  const CreateRateScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<CreateRateScreen> {
  late CreateRateViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreateRateViewModel>(
        viewModel: CreateRateViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
            bottomNavigationBar: _buildNavigateBar(),
            resizeToAvoidBottomInset: true,
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          _buildTitle(),
          Container(
            width: Get.width,
            height: 5,
            margin: EdgeInsets.only(top: 16, bottom: 16),
            color: AppColors.gray06,
          ),
          _buildText(),
          SizedBox(height: 16,),
          _buildSelectImage(),
          _buildRate()
        ],
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.productDetail);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 55, top: 30),
            child: Text(
              "Thêm đánh giá",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildTitle(){
    return Container(
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, right: 23, top: 12),
            width: 72,
            height: 72,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child: Image.asset(AppImages.png("home_inazuma"),
                fit: BoxFit.cover,
              ),
            )
          ),
          Column(
            children: [
              Container(
                width: 222,
                child: Text(
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sapien.",
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    color: AppColors.gray01
                  ),
                ),
              ),
              Container(
                width: 222,
                margin: EdgeInsets.only(top: 16),
                child: Text(
                 '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(450000)}',
                  style: TextStyle(
                    fontSize: AppFontSize.fontText,
                    fontWeight: FontWeight.w400,
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    color: AppColors.gray03
                  ),
                ),
              )
            ],
          )
        ],
      )
    );
  }

  Widget _buildText(){
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16),
      child: Column(
        children: [
          Container(
            width: Get.width,
            margin: EdgeInsets.only(bottom: 10),
            child: Text(
              "Review sản phẩm",
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: AppColors.gray01
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: AppColors.gray05)
            ),
            child: TextField(
              maxLines: 5,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(left: 12, top: 16, right: 12, bottom: 16),
              ),
              style: TextStyle(
                color: AppColors.gray01
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSelectImage(){
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16),
      child: Column(
        children: [
          Container(
            width: Get.width,
            child: Text(
              "Thêm ảnh hoặc video",
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: AppColors.gray01
              ),
            ),
          ),
          SizedBox(height: 10,),
          Row(
            children: [
              GestureDetector(
                onTap: (){
                  _viewModel.pickImage();
                },
                child: Stack(
                  children: [
                    Positioned(
                      child: Container(
                          width: 72,
                          height: 72,
                          child: Image.asset(AppImages.png("boder01"))
                      ),
                    ),
                    Positioned(
                      top: 24,
                      bottom: 24,
                      left: 22,
                      right: 22,
                      child: Container(
                          width: 24,
                          height: 24,
                          child: Image.asset(AppImages.png("gallary"))
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(width: 8,),
              GestureDetector(
                onTap: (){
                  _viewModel.pickVideo();
                },
                child: Stack(
                  children: [
                    Positioned(
                      child: Container(
                          width: 72,
                          height: 72,
                          child: Image.asset(AppImages.png("boder01"))
                      ),
                    ),
                    Positioned(
                      top: 24,
                      bottom: 24,
                      left: 22,
                      right: 22,
                      child: Container(
                          width: 24,
                          height: 24,
                          child: Image.asset(AppImages.png("video"))
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(width: 8,),
              _viewModel.image != null
                ? Stack(
                children: [
                  Positioned(
                    child: Container(
                      width: 72,
                      height: 72,
                      child: _viewModel.image != null ? Image.file(_viewModel.image!, fit: BoxFit.cover,) : SizedBox(),
                    ),
                  ),
                  Positioned(
                      left: 35,
                      bottom: 35,
                      child: IconButton(
                        icon: Image.asset(AppImages.png("icon_cancel"),
                          width: 20,
                          height: 20,
                        ),
                        onPressed: (){
                          _viewModel.deleteImage();
                        },
                      )
                  )
                ],
              )
                : SizedBox(),
              SizedBox(width: 8,),
              _viewModel.video != null
                  ? Stack(
                children: [
                  Positioned(
                    child: Container(
                      width: 72,
                      height: 72,
                      child: _viewModel.video != null ? VideoPlayer(_viewModel.video!) : SizedBox(),
                    ),
                  ),
                  Positioned(
                      left: 35,
                      bottom: 35,
                      child: IconButton(
                        icon: Image.asset(AppImages.png("icon_cancel"),
                          width: 20,
                          height: 20,
                        ),
                        onPressed: (){
                          _viewModel.deleteVideo();
                        },
                      )
                  )
                ],
              )
                  : SizedBox(),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildRate(){
    return Container(
      margin: EdgeInsets.only(left: 16, top: 16, right: 16),
      child: Column(
        children: [
          Container(
            width: Get.width,
            child: Text(
              "Review sản phẩm",
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: AppColors.gray01
              ),
            ),
          ),
          SizedBox(height: 10,),
          Container(
            width: Get.width,
            child: RatingBar(
              initialRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              ratingWidget: RatingWidget(
                full: Icon(Icons.star, color: AppColors.star, size: 26,),
                half: Icon(Icons.star, color: AppColors.star, size: 26,),
                empty: Icon(Icons.star, color: AppColors.gray05, size: 26,),
              ),
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _buildNavigateBar(){
    return Container(
      height: 84,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
              color: AppColors.gray05,
              blurRadius: 10
          )]
      ),
      child: Container(
        width: 20,
        height: 10,
        margin: EdgeInsets.only(left: 20, top: 10, right: 20, bottom: 25),
        child: TextButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(AppColors.mint),
          ),
          onPressed: (){},
          child: Text(
            "Đăng",
            style: TextStyle(
                fontSize: AppFontSize.fontText,
                fontWeight: FontWeight.w500,
                fontFamily: AppTextStyles.fontFamilyRoboto,
                color: Colors.white
            ),
          ),
        ),
      )
    );
  }
}
