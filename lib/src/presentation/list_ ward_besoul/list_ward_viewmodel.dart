import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../presentation.dart';

class ListWardViewModel extends BaseViewModel {
  init() async {
    listWard.sort();
    result.add(listWard);
    groupMyList();
  }

  TextEditingController search = TextEditingController();
  var result = BehaviorSubject<List<String>>();

  List<String> listWard = [
    "Phước Vĩnh",
    "Xuân Phú",
    "Thủy Cam",
    "Trường An",
    "Gia Hội",
    "Vĩnh Ninh",
    "Lộc Bổn",
    "Lộc Thủy",
    "Phú Bài",
    "Hương Hồ",
    "Hương Long",
    "An Cựu",
    "An Đông",
    "Bình Tiến",
    "An Hòa",
    "Phú Xuân",
    "Phú Cát",
    "Thuận Thành",
    "Vỹ Dạ",
    "Thuận An",
    "Thuận Lộc",
    "Tây Lộc",
  ];

  Map<String, List<String>> groupMyList() {
    Map<String, List<String>> groupedLists = {};
    result.value.forEach((city) {
      if (groupedLists['${city[0]}'] == null) {
        groupedLists['${city[0]}'] = <String>[];
      }
      groupedLists['${city[0]}']?.add(city);
    });
    return groupedLists;
  }

  void searchList(String keyWord){
    List<String> temp = [];
    listWard.forEach((city) {
      if(city.toLowerCase().contains(keyWord.toLowerCase()))temp.add(city);
    });
    result.add(temp);
  }
}
