import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/register_besoul/register_besoul_viewmodel.dart';
import 'package:flutter_app/src/utils/app_valid.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class RegisterBesoulScreen extends StatefulWidget {
  const RegisterBesoulScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<RegisterBesoulScreen> {
  late RegisterBesoulViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<RegisterBesoulViewModel>(
        viewModel: RegisterBesoulViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Container(
        width: Get.width,
        height: Get.height,
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: IconButton(
                  onPressed: (){
                    Get.toNamed(Routers.loginBesoul);
                  },
                  padding: EdgeInsets.symmetric(vertical: 50, horizontal: 20),
                  icon: Icon(Icons.arrow_back_ios, color: AppColors.gray03, size: 24,)),
            ),
            Text(
              "Đăng ký tài khoản",
              style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w500,
                  fontSize: 24,
                  color: AppColors.gray01
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 8,),
            Text(
              "Đăng ký để tiếp tục",
              style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: AppColors.gray03
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 78,),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                "Email hoặc số điện thoại",
                style: TextStyle(
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: AppColors.gray01
                ),
                textAlign: TextAlign.left,
              ),
            ),
            SizedBox(height: 12,),
            _buildForm()
          ],
        ),
      ),
    );
  }

  Widget _buildUser(){
    return Container(
      width: 335,
      margin: EdgeInsets.only(left: 20, right: 20),
      child: TextFormField(
          style: TextStyle(color: Colors.black),
          decoration: InputDecoration(
            border: InputBorder.none,
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: Colors.black, width: 1)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: AppColors.gray05, width: 1)),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: Colors.red, width: 1)),
            focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: Colors.black, width: 1)),
            hintText: "Nhập email hoặc số điện thoại",
            hintStyle: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              fontFamily: AppTextStyles.fontFamilyRoboto,
              color: AppColors.gray03,
            ),
            contentPadding: EdgeInsets.symmetric(horizontal: 12),
          ),
          validator: AppValid.validatePhoneOrEmail()
      ),
    );
  }

  Widget _buildPassWord(){
    return Container(
      width: 335,
      margin: EdgeInsets.only(left: 20, right: 20),
      child: TextFormField(
        style: TextStyle(color: Colors.black),
        decoration: InputDecoration(
          border: InputBorder.none,
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.black, width: 1)),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: AppColors.gray05, width: 1)),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.red, width: 1)),
          focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.black, width: 1)),
          hintText: "Nhập mật khẩu",
          hintStyle: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 14,
            fontFamily: AppTextStyles.fontFamilyRoboto,
            color: AppColors.gray03,
          ),
          contentPadding: EdgeInsets.symmetric(horizontal: 12),
          suffixIcon: Align(
              widthFactor: 1,
              heightFactor: 1,
              child: IconButton(
                onPressed: (){
                  if(_viewModel.checkShowPass == true)
                  {
                    setState(() {
                      _viewModel.checkShowPass = false;
                      _viewModel.changeIcon = Icons.visibility_off;
                    });
                  }
                  else
                  {
                    setState(() {
                      _viewModel.checkShowPass = true;
                      _viewModel.changeIcon = Icons.visibility;
                    });
                  }
                },
                icon: Icon(_viewModel.changeIcon, color: _viewModel.changeColor,),
              )
          ),
        ),
        obscureText: _viewModel.checkShowPass,
        validator: AppValid.validatePassword(),
      ),
    );
  }

  Widget _buildForm(){
    return Form(
      key: _viewModel.formKey,
      child: Column(
        children: [
          _buildUser(),
          SizedBox(height: 24,),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text(
              "Mật khẩu",
              style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: AppColors.gray01
              ),
              textAlign: TextAlign.left,
            ),
          ),
          SizedBox(height: 12,),
          _buildPassWord(),
          SizedBox(height: 48,),
          Container(
            width: 335,
            height: 51,
            margin: EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
            ),
            child: TextButton(
              onPressed: (){
                if(_viewModel.formKey.currentState!.validate()){
                  Get.toNamed(Routers.loginBesoul);
                }
              } ,
              child: Text(
                "Đăng ký",
                style: TextStyle(
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    fontWeight: FontWeight.w700,
                    fontSize: AppFontSize.fontText,
                    color: Colors.white
                ),
              ),
              style: TextButton.styleFrom(
                backgroundColor: AppColors.mint,
              ),
            ),
          ),
          SizedBox(height: 8,),
          Container(
            width: 335,
            height: 51,
            margin: EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              border: Border.all(color: AppColors.mint,)
            ),
            child: TextButton(
              onPressed: (){
                Get.toNamed(Routers.useCodeIntroduction);
              } ,
              child: Text(
                "Sử dụng mã giới thiệu",
                style: TextStyle(
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    fontWeight: FontWeight.w700,
                    fontSize: AppFontSize.fontText,
                    color: AppColors.mint01
                ),
              ),
              style: TextButton.styleFrom(
                backgroundColor: Colors.white,
              ),

            ),
          ),
          SizedBox(height: 24,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Tôi đã có tài khoản !",
                style: TextStyle(
                    color: AppColors.gray02,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontFamily: AppTextStyles.fontFamilyRoboto
                ),
              ),
              SizedBox(width: 1,),
              GestureDetector(
                onTap: (){
                  Get.toNamed(Routers.loginBesoul);
                },
                child: Text(
                  "Đăng nhập",
                  style: TextStyle(
                      color: AppColors.blue01,
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      decoration: TextDecoration.underline
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
