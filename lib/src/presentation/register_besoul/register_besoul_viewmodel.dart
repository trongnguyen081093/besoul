import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class RegisterBesoulViewModel extends BaseViewModel {
  init() async {}

  bool checkShowPass = true;

  var changeIcon = Icons.visibility;

  var changeColor = AppColors.gray04;

  final formKey = GlobalKey<FormState>();
}
