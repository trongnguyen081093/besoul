import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/login_besoul/login_besoul_viewmodel.dart';
import 'package:flutter_app/src/utils/app_valid.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class LoginBesoulScreen extends StatefulWidget {
  const LoginBesoulScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<LoginBesoulScreen> {
  late LoginBesoulViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<LoginBesoulViewModel>(
        viewModel: LoginBesoulViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Container(
        width: Get.width,
        height: Get.height,
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: IconButton(
                  onPressed: (){
                    Get.toNamed(Routers.selectCountryBesoul);
                  },
                  padding: EdgeInsets.symmetric(vertical: 50, horizontal: 20),
                  icon: Icon(Icons.arrow_back_ios, color: AppColors.gray03, size: 24,)),
            ),
            Text(
                "Đăng nhập tài khoản",
                style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w500,
                  fontSize: 24,
                  color: AppColors.gray01
                ),
                textAlign: TextAlign.center,
              ),
            SizedBox(height: 8,),
            Text(
              "Đăng nhập để tiếp tục",
              style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: AppColors.gray03
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 78,),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                "Email hoặc số điện thoại",
                style: TextStyle(
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: AppColors.gray01
                ),
                textAlign: TextAlign.left,
              ),
            ),
            SizedBox(height: 12,),
            _buildForm()
          ],
        ),
      ),
    );
  }

  Widget _buildUser(){
    return Container(
      width: 335,
      margin: EdgeInsets.only(left: 20, right: 20),
      child: TextFormField(
        style: TextStyle(color: Colors.black),
        decoration: InputDecoration(
          border: InputBorder.none,
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.black, width: 1)),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: AppColors.gray05, width: 1)),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.red, width: 1)),
          focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.black, width: 1)),
          hintText: "Nhập email hoặc số điện thoại",
          hintStyle: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 14,
            fontFamily: AppTextStyles.fontFamilyRoboto,
            color: AppColors.gray03,
          ),
          contentPadding: EdgeInsets.symmetric(horizontal: 12),
        ),
        validator: AppValid.validatePhoneOrEmail()
      ),
    );
  }

  Widget _buildPassWord(){
    return Container(
      width: 335,
      margin: EdgeInsets.only(left: 20, right: 20),
      child: TextFormField(
        style: TextStyle(color: Colors.black),
        decoration: InputDecoration(
          border: InputBorder.none,
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.black, width: 1)),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: AppColors.gray05, width: 1)),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.red, width: 1)),
          focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.black, width: 1)),
          hintText: "Nhập mật khẩu",
          hintStyle: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 14,
            fontFamily: AppTextStyles.fontFamilyRoboto,
            color: AppColors.gray03,
          ),
          contentPadding: EdgeInsets.symmetric(horizontal: 12),
          suffixIcon: Align(
            widthFactor: 1,
            heightFactor: 1,
            child: IconButton(
              onPressed: (){
                if(_viewModel.checkShowPass == true)
                  {
                    setState(() {
                      _viewModel.checkShowPass = false;
                      _viewModel.changeIcon = Icons.visibility_off_outlined;
                    });
                  }
                else
                  {
                    setState(() {
                      _viewModel.checkShowPass = true;
                      _viewModel.changeIcon = Icons.visibility_outlined;
                    });
                  }
              },
              icon: Icon(_viewModel.changeIcon, color: _viewModel.changeColor,),
            )
          ),
        ),
        obscureText: _viewModel.checkShowPass,
        validator: AppValid.validatePassword(),
      ),
    );
  }

  Widget _buildForm(){
    return Form(
      key: _viewModel.formKey,
      child: Column(
        children: [
          _buildUser(),
          SizedBox(height: 24,),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text(
              "Mật khẩu",
              style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: AppColors.gray01
              ),
              textAlign: TextAlign.left,
            ),
          ),
          SizedBox(height: 12,),
          _buildPassWord(),
          SizedBox(height: 8,),
          Container(
            alignment: Alignment.centerRight,
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              child: Text(
                "Quên mật khẩu",
                style: TextStyle(
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: AppColors.gray03
                ),
                textAlign: TextAlign.left,
              ),
              onTap: (){
                Get.toNamed(Routers.forgotPassword);
              },
            ),
          ),
          SizedBox(height: 48,),
          Container(
            width: 335,
            height: 51,
            margin: EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
            ),
            child: TextButton(
              onPressed: (){
                if(_viewModel.formKey.currentState!.validate()){
                  setState(() {
                    _viewModel.changeIcon = Icons.visibility_outlined;
                    _viewModel.changeColor = AppColors.gray04;
                  });
                  Get.toNamed(Routers.homeBesoul);
                }else{
                  setState(() {
                    _viewModel.changeIcon = Icons.warning;
                    _viewModel.changeColor = Colors.red;
                  });
                }
              } ,
              child: Text(
                "Đăng nhập",
                style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w700,
                  fontSize: AppFontSize.fontText,
                  color: Colors.white
                ),
              ),
              style: TextButton.styleFrom(
                backgroundColor: AppColors.mint,
              ),
            ),
          ),
          SizedBox(height: 24,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Tôi chưa có tài khoản !",
                style: TextStyle(
                    color: AppColors.gray02,
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    fontFamily: AppTextStyles.fontFamilyRoboto
                ),
              ),
              SizedBox(width: 1,),
              GestureDetector(
                onTap: (){
                  Get.toNamed(Routers.registerBesoul);
                },
                child: Text(
                  "Đăng ký",
                  style: TextStyle(
                      color: AppColors.blue01,
                      fontSize: 13,
                      fontWeight: FontWeight.w700,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                    decoration: TextDecoration.underline
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

}
