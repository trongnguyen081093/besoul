import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class CreateOrderViewModel extends BaseViewModel {
  init() async {}

  int selectIndex = -1;

  void selectPay(int index){
    switch (index){
      case 0:
        selectIndex = 0;
        notifyListeners();
        break;
      case 1:
        selectIndex = 1;
        notifyListeners();
        break;
    }
  }
  int result = 0;
  int Total(){
    int total = 0;
    for(int i = 0; i < listOrder.length; i++){
      for(int j = 0; j < listOrder[i].listProduct.length; j++){
        int resultTemp =  listOrder[i].listProduct[j].amount * listOrder[i].listProduct[j].price;
        total = total + resultTemp;

      }
    }
    result = total;
    notifyListeners();
    return result;
  }

  List<AddressOrder> listAddress = [
    AddressOrder(nameCus: "Nguyen", phoneNumber: "0932127783", addressCus: "20 Lê Lợi, Phường Vĩnh Ninh, Thành Phố Huế")
  ];

  List<ListOrder> listOrder = [
    ListOrder(
        listProduct: [
          ListProduct(imageProduct: AppImages.png("product_sua_rua_mat"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", amount: 2, price: 450000),
          ListProduct(imageProduct: AppImages.png("home_inazuma"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", amount: 1, price: 550000),
          ListProduct(imageProduct: AppImages.png("home_inazuma1"), nameProduct: "Sữa rửa mặt sạch sâu táo xanh innisfree Apple Seed Soft Cleansin...", amount: 2, price: 500000),
        ]
    )
  ];
  
  List<ListPay> listPay = [
    ListPay(imgPay: AppImages.png("pay_money"), namePay: "Thanh toán khi nhận hàng", contentPay: "Thanh toán bằng tiền mặt"),
    ListPay(imgPay: AppImages.png("pay_card"), namePay: "Chuyển khoản ngân hàng", contentPay: "Theo thông tin ngân hàng")
  ];
}

class AddressOrder{
  String nameCus;
  String phoneNumber;
  String addressCus;

  AddressOrder({Key ? key, required this.nameCus, required this.phoneNumber, required this.addressCus});
}

class ListOrder{
  List<ListProduct> listProduct;

  ListOrder({Key ? key, required this.listProduct});
}

class ListProduct{
  String imageProduct;
  String nameProduct;
  int amount;
  int price;

  ListProduct({Key ? key, required this.imageProduct, required this.nameProduct, required this.amount, required this.price});
}

class ListPay{
  String imgPay;
  String namePay;
  String contentPay;
  
  ListPay({Key ? key, required this.imgPay, required this.namePay, required this.contentPay});
}
