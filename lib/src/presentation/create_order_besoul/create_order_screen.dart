import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/create_order_besoul/create_order_viewmodel.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

import '../presentation.dart';

class CreateOrderScreen extends StatefulWidget {
  const CreateOrderScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<CreateOrderScreen> {
  late CreateOrderViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreateOrderViewModel>(
        viewModel: CreateOrderViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: AppColors.gray09,
            bottomNavigationBar: _buildNavigateBar(),
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          _buildAddress(),
          SizedBox(height: 4,),
          _buildOrder(),
          SizedBox(height: 4,),
          _buildNote(),
          SizedBox(height: 4,),
          _buildPay(),
          SizedBox(height: 4,),
          _buildVoucher(),
          SizedBox(height: 4,),
          _buildTotal(),
        ],
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.productDetail);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 75, top: 30),
            child: Text(
              "Đơn hàng",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          GestureDetector(
            onTap: (){

            },
            child: Container(
              margin: EdgeInsets.only(left: 80, top: 36, right: 16),
              child: Text(
                "Thêm",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    fontFamily: AppTextStyles.fontFamilyRoboto
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildAddress(){
    return Container(
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.only(left: 16, right: 16, top: 16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Image.asset(AppImages.png("icon_location"),
                width: 16,
                height: 16,
              ),
            ),
            SizedBox(width: 8,),
            Expanded(
              // width: 270,
              // margin: EdgeInsets.only(left: 10, top: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "Địa chỉ nhận hàng:",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray01
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8),
                    child: Row(
                      children: [
                        Container(
                          child: Text(
                            _viewModel.listAddress[0].nameCus,
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                color: AppColors.gray03
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 8),
                          width: 1,
                          height: 12,
                          color: AppColors.gray03,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 8),
                          child: Text(
                            _viewModel.listAddress[0].phoneNumber,
                            style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontFamily: AppTextStyles.fontFamilyRoboto,
                            color: AppColors.gray03
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8, bottom: 16),
                    child: Text(
                      _viewModel.listAddress[0].addressCus,
                      maxLines: 4,
                      style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray03,
                      ),
                    ),
                  )
                ],
              ),
            ),
            GestureDetector(
              onTap: (){
                Get.toNamed(Routers.listAddress);
              },
              child: Container(
                width: 24,
                height: 24,
                child: Icon(Icons.navigate_next,
                  color: AppColors.gray04,
                ),
              ),
            ),
          ],
        )
      ),
    );
  }

  Widget _buildOrder(){
    return Container(
      color: Colors.white,
      child: Column(
        children: List.generate(_viewModel.listOrder.length, (index) => Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: Column(
            children: List.generate(_viewModel.listOrder[index].listProduct.length, (index1) => Container(
              margin: EdgeInsets.only(top: 16,),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Image.asset(_viewModel.listOrder[index].listProduct[index1].imageProduct, fit: BoxFit.cover,),
                        width: 77,
                        height: 77,
                      ),
                      Container(
                        width: 240,
                        margin: EdgeInsets.only(left: 10),
                        child: Column(
                          children: [
                            Container(
                              child: Text(
                                _viewModel.listOrder[index].listProduct[index1].nameProduct,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: AppTextStyles.fontFamilyRoboto,
                                  color: AppColors.gray01,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 26),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    child: Text(
                                      '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.listOrder[index].listProduct[index1].price)}',
                                      style: TextStyle(
                                        fontSize: AppFontSize.fontText,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray02,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      'x${_viewModel.listOrder[index].listProduct[index1].amount}',
                                      style: TextStyle(
                                        fontSize: AppFontSize.fontText,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray03,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 16,),
                  Divider(
                    height: 1,
                    color: AppColors.gray09,
                  ),
                ],
              ),
            )
            )
          ),
        )
        )
      ),
    );
  }

  Widget _buildNote(){
    return Container(
      color: Colors.white,
      child: Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(left:16,right: 16, top: 12, bottom: 12),
              child: Row(
                children: [
                  Container(
                    child: Image.asset(AppImages.png("icon_car"),
                      width: 15,
                      height: 13,
                    ),
                  ),
                  SizedBox(width: 8,),
                  Expanded(
                    child: Text(
                      "Đơn vị giao vận",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: (){
                      Get.toNamed(Routers.listTransportation);
                    },
                    child: Row(
                      children: [
                        Container(
                          child: Text(
                            "Viettel Post",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontFamily: AppTextStyles.fontFamilyRoboto,
                              color: AppColors.gray04,
                            ),
                          ),
                        ),
                        Container(
                          width: 24,
                          height: 24,
                          child: Icon(Icons.navigate_next,
                            color: AppColors.gray04,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: Get.width,
              height: 1,
              color: AppColors.gray09,
            ),
            Container(
              margin: EdgeInsets.only(left:16,right: 16, top: 12, bottom: 12),
              child: Row(
                children: [
                  Container(
                    child: Image.asset(AppImages.png("icon_note"),
                      width: 15,
                      height: 13,
                    ),
                  ),
                  SizedBox(width: 8,),
                  Expanded(
                    child: Text(
                      "Thêm Ghi chú",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: (){
                      _buildShowBottomSheet();
                    },
                    child: Container(
                      margin: EdgeInsets.only( left: 188),
                      width: 24,
                      height: 24,
                      child: Icon(Icons.navigate_next,
                        color: AppColors.gray04,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildPay(){
    return Container(
      color: Colors.white,
      width: Get.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 16,top: 16, bottom: 12),
            child: Text(
              "Hình thức thanh toán",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                fontFamily: AppTextStyles.fontFamilyRoboto,
                color: AppColors.gray01,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 16),
            child: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              child: Row(
                children: List.generate(_viewModel.listPay.length, (index) => GestureDetector(
                  onTap: (){
                    _viewModel.selectPay(index);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right:6, bottom: 24),
                    width: 223,
                    height: 72,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: _viewModel.selectIndex == index ? AppColors.mint : AppColors.gray05),
                      color: _viewModel.selectIndex == index ? AppColors.mint02 : Colors.white,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 12, top: 17, right: 8),
                          child: Image.asset(_viewModel.listPay[index].imgPay,
                            width: 24,
                            height: 18,
                          ),
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 19),
                                child: Text(
                                  _viewModel.listPay[index].namePay,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: AppTextStyles.fontFamilyRoboto,
                                    color: AppColors.gray01,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8),
                                child: Text(
                                  _viewModel.listPay[index].contentPay,
                                  style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: AppTextStyles.fontFamilyRoboto,
                                    color: AppColors.gray03,
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )
                )
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildVoucher(){
    return Container(
      width: Get.width,
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.only(left: 16, right: 16, top: 14, bottom: 14),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: 8),
              child: Image.asset(AppImages.png("icon_voucher"),
                width: 20,
                height: 13,
              ),
            ),
            Expanded(
              child: Text(
                "Voucher",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: AppColors.gray01,
                ),
              ),
            ),
            GestureDetector(
              child: Row(
                children: [
                  Container(
                    child: Text(
                      "Thêm Voucher",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray04,
                      ),
                    ),
                  ),
                  Container(
                    width: 24,
                    height: 24,
                    child: Icon(Icons.navigate_next,
                      color: AppColors.gray04,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    );
  }

  Widget _buildTotal(){
    return Container(
      width: Get.width,
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.only(left: 16, right: 16),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "Tạm tính",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  ),
                  Container(
                    child: Text(
                      "Phí vận chuyển",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  ),
                  Container(
                    child: Text(
                      "Giảm giá Voucher",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  ),
                  Container(
                    child: Text(
                      "Tổng cộng",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 12, bottom: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    child: Text(
                      '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.Total())}',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  ),
                  Container(
                    child: Text(
                      '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(15000)}',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  ),
                  Container(
                    child: Text(
                      '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(-15000)}',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray01,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  ),
                  Container(
                    child: Text(
                      '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.Total() + 15000)}',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.redPrice,
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 8),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildNavigateBar(){
    return Container(
      height: 84,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
              color: AppColors.gray05,
              blurRadius: 10
          )]
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, right: 16),
            child: Column(
              children: List.generate(1, (index) => Container(
                margin: EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        'Tổng tiền (${_viewModel.listOrder[index].listProduct.length} sản phẩm)',
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray01
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Text(
                        '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.Total())}',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.redPrice,
                        ),
                      ),
                    ),
                  ],
                ),
              )
              )
            ),
          ),
          Container(
            width: 171,
            height: 40,
            margin: EdgeInsets.only(right: 16),
            child: TextButton(
                onPressed: (){
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(AppColors.mint),
                ),
                child: Text(
                  "Đặt hàng",
                  style: TextStyle(
                      fontSize: AppFontSize.fontText,
                      fontWeight: FontWeight.w500,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: Colors.white
                  ),
                )
            ),
          )
        ],
      ),
    );
  }

  void _buildShowBottomSheet(){
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (context){
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState){
                return KeyboardDismisser(
                  child: Container(
                      width: Get.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                        color: Colors.white,
                      ),
                      padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom,
                      ),
                      child: Column(
                        children: [
                          Container(
                            width: Get.width,
                            height: 51,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                                color: AppColors.mint02
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 16),
                                  child: GestureDetector(
                                      onTap: (){
                                        Navigator.pop(context);
                                      },
                                      child: Text(
                                          "Hủy",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: AppTextStyles.fontFamilyRoboto,
                                              color: AppColors.gray03
                                          )
                                      )
                                  ),
                                ),
                                Container(
                                  child: Text(
                                      "Thêm ghi chú",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: AppTextStyles.fontFamilyRoboto,
                                          color: AppColors.gray01
                                      )
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(right: 16),
                                  child: GestureDetector(
                                    onTap: (){
                                      Navigator.pop(context);
                                    },
                                    child: Text(
                                      "Xong",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: AppTextStyles.fontFamilyRoboto,
                                            color: AppColors.gray03
                                        )
                                    )
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            height: 1,
                            color: AppColors.mint,
                          ),
                          Container(
                            child: TextField(
                              maxLines: 5,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(left: 12, top: 16, right: 12),
                              ),
                              style: TextStyle(
                                  color: AppColors.gray01
                              ),
                            )
                          )
                        ],
                      )
                  ),
                );
              }
          );
        }
    );
  }
}
