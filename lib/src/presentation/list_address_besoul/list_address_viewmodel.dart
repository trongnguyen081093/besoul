import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class ListAddressViewModel extends BaseViewModel {
  init() async {}

  int selectIndex = -1;

  void SelectAddress(int index){
    selectIndex = index;
    notifyListeners();
  }

  List<AddressOrder> listAddress = [
    AddressOrder(nameCus: "Nguyen", phoneNumber: "0932127783", addressCus: "20 Lê Lợi, Phường Vĩnh Ninh, Thành Phố Huế"),
    AddressOrder(nameCus: "Nguyen", phoneNumber: "0932127783", addressCus: "20 Lê Lợi, Phường Vĩnh Ninh, Thành Phố Huế")
  ];
}

class AddressOrder{
  String nameCus;
  String phoneNumber;
  String addressCus;

  AddressOrder({Key ? key, required this.nameCus, required this.phoneNumber, required this.addressCus});
}
