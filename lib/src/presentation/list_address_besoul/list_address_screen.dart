import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/list_address_besoul/list_address_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class ListAddressScreen extends StatefulWidget {
  const ListAddressScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<ListAddressScreen> {
  late ListAddressViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ListAddressViewModel>(
        viewModel: ListAddressViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: AppColors.gray06,
          );
        });
  }

  Widget _buildBody() {
    return Column(
      children: [
        _buildAppBar(),
        _buildAddress(),
        _buildAddAddress()
      ],
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.createOrder);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 85, top: 30),
            child: Text(
              "Địa chỉ",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAddress(){
    return Container(
      color: Colors.white,
      child: Column(
        children: List.generate(_viewModel.listAddress.length, (index) => GestureDetector(
          onTap: (){
            _viewModel.SelectAddress(index);
          },
          child: Column(
            children: [
              Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Image.asset(AppImages.png("icon_location_1"),
                          width: 16,
                          height: 16,
                        ),
                      ),
                      SizedBox(width: 8,),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text(
                                "Địa chỉ nhận hàng:",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: AppTextStyles.fontFamilyRoboto,
                                    color: AppColors.gray01
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 8),
                              child: Row(
                                children: [
                                  Container(
                                    child: Text(
                                      _viewModel.listAddress[index].nameCus,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: AppTextStyles.fontFamilyRoboto,
                                          color: AppColors.gray03
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 8),
                                    width: 1,
                                    height: 12,
                                    color: AppColors.gray03,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 8),
                                    child: Text(
                                      _viewModel.listAddress[index].phoneNumber,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: AppTextStyles.fontFamilyRoboto,
                                          color: AppColors.gray03
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 8, bottom: 16),
                              child: Text(
                                _viewModel.listAddress[index].addressCus,
                                maxLines: 4,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: AppTextStyles.fontFamilyRoboto,
                                  color: AppColors.gray03,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      _viewModel.selectIndex == index
                      ? Container(
                          width: 16,
                          height: 16,
                          child: Image.asset(AppImages.png("icon_check_1"))
                        )
                      : SizedBox()
                    ],
                  )
              ),
              Container(
                height: 4,
                color: AppColors.gray06,
              )
            ],
          ),
        )),
      ),
    );
  }

  Widget _buildAddAddress(){
    return Container(
      color: Colors.white,
      child: GestureDetector(
        child: Container(
          margin: EdgeInsets.only(left: 16, top: 16, bottom: 16),
          child: Row(
            children: [
              Container(
                child: Icon(Icons.add,
                  color: AppColors.mint,
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 8),
                child: Text(
                  "Thêm địa chỉ nhận hàng",
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.mint
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
