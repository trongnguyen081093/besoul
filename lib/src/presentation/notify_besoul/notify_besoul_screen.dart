import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/notify_besoul/notify_besoul_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class NotifyBesoulScreen extends StatefulWidget {
  const NotifyBesoulScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<NotifyBesoulScreen> {
  late NotifyBesoulViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NotifyBesoulViewModel>(
        viewModel: NotifyBesoulViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return Container(
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            _buildAppBar(),
            SizedBox(height: 12,),
            _buildList(),
            SizedBox(height: 12,),
            _buildListNotify(),
          ],
        ),
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.navigation);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 45, top: 30),
            child: Text(
              "Tin khuyến mãi",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left:60, top: 30),
            child: IconButton(
              icon: Image.asset(AppImages.png("notify_tick"),
                width: 24,
                height: 24,
              ),
              onPressed: (){},
            )
          )
        ],
      ),
    );
  }

  Widget _buildList(){
    return Container(
      height: 40,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
            color: AppColors.gray06,
            offset: const Offset(0, 3),
            blurRadius: 2,
            spreadRadius: 0.05,
          )]
      ),
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: List.generate(_viewModel.listApp.length, (index) => Container(
            height: 40,
            margin: EdgeInsets.only(right: 20),
            child: GestureDetector(
              onTap: (){
                setState(() {
                  _viewModel.indexItem = index;
                });
              },
              child: Column(
                children: [
                  Container(
                    child: Text(
                      _viewModel.listApp[index],
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        fontWeight: FontWeight.w400,
                        color: _viewModel.indexItem == index ? AppColors.mint : AppColors.gray01
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  _viewModel.indexItem == index ?
                  Container(
                    width: _viewModel.sizeContainer(index),
                    height: 2,
                    color: AppColors.mint,
                  ) :
                  SizedBox()
                ],
              ),
            ),
          )
          ),
        ),
      ),
    );
  }

  Widget _buildListNotify(){
    return Container(
      child: Column(
        children: List.generate(_viewModel.selectList().length, (index) => Container(
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Container(
                        child: Image.asset(
                          _viewModel.selectList()[index].imgNotify,
                          width: 40,
                          height: 40,
                        ),
                      width: 84,
                    ),
                    Container(
                      width: 275,
                      child: Column(
                        children: [
                          Container(
                            child: Text(
                              _viewModel.selectList()[index].nameNotify,
                              style: TextStyle(
                                fontSize: AppFontSize.fontText,
                                fontWeight: FontWeight.w500,
                                color: AppColors.gray01
                              ),
                            ),
                            alignment: Alignment.centerLeft,
                          ),
                          SizedBox(height: 4,),
                          Text(
                            _viewModel.selectList()[index].contentNotify,
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: AppColors.gray04
                            ),
                          ),
                          SizedBox(height: 8,),
                          Row(
                            children: [
                              Icon(
                                Icons.access_time,
                                color: AppColors.gray03,
                                size: 12,
                              ),
                              SizedBox(width: 5,),
                              Text(
                                _viewModel.selectList()[index].dateNotify,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.gray04
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 12,),
              Container(
                width: 343 * scaleWidth,
                height: 1,
                color: AppColors.gray06,
              ),
              SizedBox(height: 12,),
            ],
          ),
        ))
      ),
    );
  }
}
