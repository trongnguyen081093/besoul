import 'package:flutter/cupertino.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/home_besoul/home_besoul_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class NotifyBesoulViewModel extends BaseViewModel {
  init() async {
  }
  int indexItem = 0;
  List<String> listApp = ["Tất cả", "Khuyến mãi", "Tình trạng đơn hàng", "Chăm sóc khách hàng"];

  double sizeContainer(int index){
    switch (index){
      case 0:
        return 70;
      case 1:
        return 105;
      case 2:
        return 150;
      default:
        return 150;
    }
  }
  
  List<MyNotify> listNotify = [
    MyNotify(imgNotify: AppImages.png("notify_besoul"), nameNotify: "Chăm sóc khách hàng", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "25/11/2021"),
    MyNotify(imgNotify: AppImages.png("notify_product"), nameNotify: "Đã tiếp nhận đơn hàng", contentNotify: "BeSoul đã tiếp nhận đơn hàng #123456789 của bạn", dateNotify: "05/11/2021"),
    MyNotify(imgNotify: AppImages.png("notify_gift"), nameNotify: "Khuyến mãi", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "25/01/2021"),
    MyNotify(imgNotify: AppImages.png("notify_besoul"), nameNotify: "Chăm sóc khách hàng", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "24/11/2021"),
    MyNotify(imgNotify: AppImages.png("notify_besoul"), nameNotify: "Chăm sóc khách hàng", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "25/11/2021"),
    MyNotify(imgNotify: AppImages.png("notify_product"), nameNotify: "Đã tiếp nhận đơn hàng", contentNotify: "BeSoul đã tiếp nhận đơn hàng #123456789 của bạn", dateNotify: "05/11/2021"),
    MyNotify(imgNotify: AppImages.png("notify_gift"), nameNotify: "Khuyến mãi", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "25/01/2021"),
    MyNotify(imgNotify: AppImages.png("notify_besoul"), nameNotify: "Chăm sóc khách hàng", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "24/11/2021"),
    MyNotify(imgNotify: AppImages.png("notify_besoul"), nameNotify: "Chăm sóc khách hàng", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "25/11/2021"),
    MyNotify(imgNotify: AppImages.png("notify_product"), nameNotify: "Đã tiếp nhận đơn hàng", contentNotify: "BeSoul đã tiếp nhận đơn hàng #123456789 của bạn", dateNotify: "05/11/2021"),
    MyNotify(imgNotify: AppImages.png("notify_gift"), nameNotify: "Khuyến mãi", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "25/01/2021"),
    MyNotify(imgNotify: AppImages.png("notify_besoul"), nameNotify: "Chăm sóc khách hàng", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "24/11/2021"),
    MyNotify(imgNotify: AppImages.png("notify_besoul"), nameNotify: "Chăm sóc khách hàng", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "25/11/2021"),
    MyNotify(imgNotify: AppImages.png("notify_product"), nameNotify: "Đã tiếp nhận đơn hàng", contentNotify: "BeSoul đã tiếp nhận đơn hàng #123456789 của bạn", dateNotify: "05/11/2021"),
    MyNotify(imgNotify: AppImages.png("notify_gift"), nameNotify: "Khuyến mãi", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "25/01/2021"),
    MyNotify(imgNotify: AppImages.png("notify_besoul"), nameNotify: "Chăm sóc khách hàng", contentNotify: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit", dateNotify: "24/11/2021"),

  ];

  List<MyNotify> selectList(){
    switch(indexItem){
      case 0:
        return listNotify.toList();
        break;
      case 1:
        return listNotify.where((i) => i.nameNotify.toLowerCase() == ("Khuyến mãi").toLowerCase()).toList();
        break;
      case 2:
        return listNotify.where((i) => i.nameNotify.toLowerCase() == ("Đã tiếp nhận đơn hàng").toLowerCase()).toList();
        break;
      case 3:
        return listNotify.where((i) => i.nameNotify.toLowerCase() == ("Chăm sóc khách hàng").toLowerCase()).toList();
        break;
      default:
        return listNotify.toList();
        break;
    }
  }
}

class MyNotify{
  String imgNotify;
  String nameNotify;
  String contentNotify;
  String dateNotify;

  MyNotify({Key ? key, required this.imgNotify, required this.nameNotify, required this.contentNotify, required this.dateNotify});
}

