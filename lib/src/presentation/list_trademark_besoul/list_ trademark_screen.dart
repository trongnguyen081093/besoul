import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/list_trademark_besoul/list_%20trademark_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class ListTrademarkScreen extends StatefulWidget {
  const ListTrademarkScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<ListTrademarkScreen> {
  late ListTrademarkViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ListTrademarkViewModel>(
        viewModel: ListTrademarkViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: AppColors.gray06,
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          _buildListTrademark()
        ],
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.createOrder);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 55, top: 30),
            child: Text(
              "Thương hiệu",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListTrademark(){
    return Container(
      child: Wrap(
        spacing: 2,
        runSpacing: 2,
        children: List.generate(_viewModel.listTrademark.length, (index) => GestureDetector(
          child: Container(
            width: 118,
            height: 124,
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  width: 72,
                  height: 72,
                  margin: EdgeInsets.only(top: 14),
                  child: Image.asset(_viewModel.listTrademark[index].imgTrademark, fit: BoxFit.cover,),
                ),
                SizedBox(height: 8,),
                Container(
                  child: Text(
                    _viewModel.listTrademark[index].nameTrademark,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray01,
                    ),
                  ),
                )
              ],
            ),
          ),
        )),
      ),
    );
  }
}
