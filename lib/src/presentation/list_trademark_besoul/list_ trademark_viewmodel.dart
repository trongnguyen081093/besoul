import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class ListTrademarkViewModel extends BaseViewModel {
  init() async {}

  List<Trademark> listTrademark = [
    Trademark(imgTrademark: AppImages.png("mask1"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask2"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask3"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask4"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask5"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask6"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask7"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask8"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask1"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask2"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask3"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask4"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask5"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask6"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask7"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask8"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask1"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask2"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask3"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask4"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask5"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask6"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask7"), nameTrademark: "Innisfree"),
    Trademark(imgTrademark: AppImages.png("mask8"), nameTrademark: "Innisfree")
  ];
}

class Trademark{
  final String imgTrademark;
  final String nameTrademark;

  Trademark({Key ? key, required this.imgTrademark, required this.nameTrademark});
}
