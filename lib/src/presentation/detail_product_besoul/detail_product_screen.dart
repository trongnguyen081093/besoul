import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/detail_product_besoul/detail_product_viewmodel.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../configs/constanst/app_constants.dart';
import '../presentation.dart';

class DetailProductScreen extends StatefulWidget {
  const DetailProductScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<DetailProductScreen> {
  late DetailProductViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DetailProductViewModel>(
        viewModel: DetailProductViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
            bottomNavigationBar: _buildNavigateBar(),
            floatingActionButton: ValueListenableBuilder<bool>(
              valueListenable: _viewModel.showFloatButton,
              builder: (context, value, child) =>
              value ? FloatingActionButton(
                  onPressed: (){
                    _viewModel.scrollController.jumpTo(0.0);
                  },
                  backgroundColor: AppColors.blue02,
                  child: Stack(
                    children: [
                      Positioned(
                        child: Image.asset(AppImages.png("product_detail_float")
                        )
                      ),
                      Positioned(
                        top: 12,
                        left: 5,
                        child: Image.asset(AppImages.png("product_detail_float1"),
                          width: 20,
                          height: 6,
                        )
                      )
                    ],
                  )
              ) : SizedBox(),
            ),
          );
        });
  }

  Widget _buildBody() {
    return Container(
      width: Get.width,
      height: Get.height,
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        controller: _viewModel.scrollController,
        child: Column(
          children: [
            _buildSlide(),
            SizedBox(height: 16,),
            _buildDetailProduct(),
            SizedBox(height: 16,),
            _buildRate(),
            SizedBox(height: 4,),
            _buildListProduct(),
            SizedBox(height: 34,),
          ],
        ),
      ),
    );
  }

  Widget _buildSlide(){
    return Container(
      child: Column(
        children: [
          Stack(
            children: [
              CarouselSlider(
                  items: _viewModel.images,
                  options: CarouselOptions(
                    height: 375,
                      autoPlay: true,
                      viewportFraction: 1,
                      onPageChanged: (index, reason) {
                        setState(() {
                          _viewModel.indexSlide = index;
                        });
                      }
                  ),
              ),
              Positioned(
                bottom: 24,
                right: 26 * scaleWidth,
                child: Container(
                  width: 36,
                  height: 19,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: AppColors.gray01.withOpacity(0.45)
                  ),
                )
              ),
              Positioned(
                  bottom: 26,
                  right: 33 * scaleWidth,
                  child: Text(
                    '${_viewModel.indexSlide + 1}/${_viewModel.images.length}'
                  )
              ),
              Positioned(
                  top: 22,
                  left: 16 * scaleWidth,
                  child: GestureDetector(
                    child: Container(
                      width: 38,
                      height: 38,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(90),
                          color: AppColors.gray01.withOpacity(0.45)
                      ),
                      child: Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                          size: 24,
                        ),
                      ),
                    ),
                  )
              ),
              Positioned(
                  top: 22,
                  right: 16 * scaleWidth,
                  child: GestureDetector(
                    child: Container(
                      width: 38,
                      height: 38,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(90),
                          color: AppColors.gray01.withOpacity(0.45)
                      ),
                      child: Container(
                        margin: EdgeInsets.only(left: 5, right: 10),
                        child: Image.asset(
                          AppImages.png("product_detail_shopping_cart"),
                        )
                      ),
                    ),
                  )
              ),
              Positioned(
                  top: 22,
                  right: 20 * scaleWidth,
                  child: Image.asset(
                    AppImages.png("product_detail_notify"),
                    width: 20,
                    height: 15,
                  )
              ),
              Positioned(
                  top: 24,
                  right: 24 * scaleWidth,
                  child: Text(
                    "10",
                    style: TextStyle(
                      fontSize: 10,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: AppTextStyles.fontFamilyRoboto
                    ),
                    textAlign: TextAlign.center,
                  )
              ),
            ],
          ),
          SizedBox(height: 14,),
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Text(
              "Tinh chất trà xanh dưỡng ẩm innisfree Green Tea Seed Serum 80mL",
              style: TextStyle(
                fontSize: 18,
                fontFamily: AppTextStyles.fontFamilyRoboto,
                fontWeight: FontWeight.w500,
                color: AppColors.gray01
              ),
            ),
          ),
          SizedBox(height: 8,),
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 16),
                child: Row(
                  children: [
                    Row(
                      children: List.generate(5, (index) => Container(
                        child: 4 >= index + 1 ?
                        Icon(
                            Icons.star, size: 15, color: AppColors.star
                        ) :
                        Icon(
                            Icons.star, size: 15, color: AppColors.gray05
                        ),
                      ),
                      ),
                    ),
                    SizedBox(width: 10,),
                    Text(
                      "4.7",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray03
                      ),
                    ),
                    SizedBox(width: 10,),
                    Container(
                      width: 1,
                      height: 12,
                      color: AppColors.gray08,
                    ),
                    SizedBox(width: 10,),
                    Text(
                      "Đã bán 100 sản phẩm",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray03
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 10,),
          Row(
            children: [
              Container(
                padding: const EdgeInsets.only(left: 16),
                child: Text(
                  '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(350000)}',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: AppColors.redPrice,
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      fontFamily: AppTextStyles.fontFamilyRoboto
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 8),
                child: Text(
                  '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(350000)}',
                  style: TextStyle(
                      color: AppColors.gray03,
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      decoration: TextDecoration.lineThrough
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 16,),
          Container(
            width: Get.width,
            height: 4,
            color: AppColors.gray09,
          )
        ],
      ),
    );
  }

  Widget _buildDetailProduct(){
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16),
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Thông tin sản phẩm",
              style: TextStyle(
                fontSize: AppFontSize.fontText,
                fontFamily: AppTextStyles.fontFamilyRoboto,
                fontWeight: FontWeight.w500,
                color: AppColors.gray01
              ),
            ),
          ),
          SizedBox(height: 12,),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              _viewModel.text1,
              style: TextStyle(
                  fontSize: 14,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w300,
                  color: AppColors.gray01
              ),
            ),
          ),
          SizedBox(height: 8,),
          Column(
            children: [
              AnimatedSize(
                duration: Duration(milliseconds: 300),
                child: ConstrainedBox(
                  constraints: _viewModel.isExpanded ? BoxConstraints() : BoxConstraints(maxHeight: 80),
                  child: Text(
                    _viewModel.text2,
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        fontWeight: FontWeight.w300,
                        color: AppColors.gray01
                    ),
                    softWrap: true,
                    overflow: TextOverflow.fade,
                  ),
                ),
              ),
              _viewModel.isExpanded
                ? TextButton(
                  child: const Text('thu gọn'),
                  onPressed: () => setState(() => _viewModel.isExpanded = false))
                : TextButton(
                  child: const Text('xem thêm'),
                  onPressed: () => setState(() => _viewModel.isExpanded = true))
            ],
          ),
          SizedBox(height: 24,),
          Container(
            height: 4,
            color: AppColors.gray09,
          )
        ],
      ),
    );
  }

  Widget _buildRate(){
    return Container(
      width: 343,
      margin: EdgeInsets.only(left: 16, right: 16),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                child: Text(
                  "Đánh giá sản phẩm",
                  style: TextStyle(
                    fontSize: AppFontSize.fontText,
                    fontWeight: FontWeight.w500,
                    color: AppColors.gray01,
                    fontFamily: AppTextStyles.fontFamilyRoboto
                  ),
                ),
              ),
              GestureDetector(
                onTap: (){
                  Get.toNamed(Routers.rateDetail);
                },
                child: Container(
                  margin: EdgeInsets.only(left: 125 * scaleWidth),
                  child: Text(
                    "Xem tất cả >",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: AppColors.gray03,
                        fontFamily: AppTextStyles.fontFamilyRoboto
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 7,),
          Row(
            children: [
              Row(
                children: List.generate(5, (index) => Container(
                  child: 4 >= index + 1 ?
                  Icon(
                      Icons.star, size: 15, color: AppColors.star
                  ) :
                  Icon(
                      Icons.star, size: 15, color: AppColors.gray05
                  ),
                ),
                ),
              ),
              SizedBox(width: 10,),
              Text(
                "4.7",
                style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    color: AppColors.gray03
                ),
              ),
              SizedBox(width: 4,),
              Text(
                "(299 Đánh giá)",
                style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    color: AppColors.gray03
                ),
              ),
            ],
          ),
          SizedBox(height: 16,),
          SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(_viewModel.listRate.length, (index) => Row(
                children: List.generate(_viewModel.listRate[index].imgProduct.length, (index1) => Container(
                  width: 82,
                  height: 82,
                  margin: EdgeInsets.only(right: 5),
                  child: Image.asset(
                      _viewModel.listRate[index].imgProduct[index1],
                    fit: BoxFit.fill,
                  ),
                )),
                )
              )
            ),
          ),
          SizedBox(height: 26,),
          Column(
            children: List.generate(3, (index) => Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Row(
                children: [
                  Container(
                    height: 200,
                    alignment: Alignment.topLeft,
                    child: CircleAvatar(
                      backgroundImage: AssetImage("assets/images/png/user_icon.png"),
                      radius: 20,
                    ),
                  ),
                  SizedBox(width: 12,),
                  Container(
                    width: 287 * scaleWidth,
                    padding: EdgeInsets.all(0),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            _viewModel.listRate[index].userName,
                            style: TextStyle(
                                fontSize: AppFontSize.fontText,
                                fontWeight: FontWeight.w500,
                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                color: AppColors.gray01
                            ),
                          ),
                        ),
                        SizedBox(height: 6,),
                        Container(
                          child: Row(
                            children: [
                              Row(
                                children: List.generate(5, (index1) =>  Container(
                                  child: _viewModel.listRate[index].rateStar >= index1 + 1 ?
                                  Icon(
                                      Icons.star, size: 15, color: AppColors.star
                                  ) : _viewModel.listRate[index].rateStar == 0 ? SizedBox() :
                                  Icon(
                                      Icons.star, size: 15, color: AppColors.gray05
                                  ),
                                ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 10,),
                        Container(
                          child: Text(
                            _viewModel.listRate[index].contentRate,
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: AppColors.gray03
                            ),
                          ),
                        ),
                        SizedBox(height: 12,),
                        Row(
                          children: List.generate(_viewModel.listRate[index].imgProduct.length, (index1) => Container(
                            margin: EdgeInsets.only(right: 5),
                            child: Image.asset(
                              _viewModel.listRate[index].imgProduct[index1],
                              width: 82,
                              height: 82,
                              fit: BoxFit.fill,
                            ),
                          )),
                        )
                      ]
                    ),
                  )
                ],
              ),
            )),
          ),
        ],
      ),
    );
  }

  Widget _buildListProduct(){
    return Column(
      children: [
        Container(
          width: Get.width,
          height: 4,
          color: AppColors.gray09,
        ),
        Container(
          margin: EdgeInsets.only(left: 16, top: 16, bottom: 16),
          alignment: Alignment.centerLeft,
          child: Text(
            "Sản phẩm gợi ý",
            style: TextStyle(
              fontSize: AppFontSize.fontText,
              fontWeight: FontWeight.w500,
              fontFamily: AppTextStyles.fontFamilyRoboto,
              color: AppColors.gray01
            ),
          ),
        ),
        Container(
          width: Get.width,
          height: Get.height,
          child: GridView.extent(
            maxCrossAxisExtent: 310,
            padding: EdgeInsets.only(top: 12, left: 16, right: 16),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            childAspectRatio: 0.6,
            physics: NeverScrollableScrollPhysics(),
            primary: false,
            children: List.generate(_viewModel.listProduct.length, (index1) => Container(
              width: 167,
              height: 269,
              margin: EdgeInsets.only( bottom: 12),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(color: AppColors.gray06, style: BorderStyle.solid, width: 1)
              ),
              child: Column(
                children: [
                  Stack(
                    children: [
                      Image.asset(
                        _viewModel.listProduct[index1].imgProduct,
                        width: 167,
                        height: 167,
                        fit: BoxFit.fill,
                      ),
                      Positioned(
                          top: 5,
                          left: 5,
                          child: Image.asset(AppImages.png("product_sale"),
                            width: 31,
                            height: 22,
                          )
                      ),
                      Positioned(
                          top: 9,
                          left: 10,
                          child: Text(
                            '${(100-((_viewModel.listProduct[index1].priceSale/_viewModel.listProduct[index1].priceProduct)*100)).ceil()}%',
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                                fontFamily: AppTextStyles.fontFamilyRoboto
                            ),
                          )
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 6, top: 6, right: 6),
                    child: Text(
                      _viewModel.listProduct[index1].nameProduct,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: AppColors.gray01,
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          fontFamily: AppTextStyles.fontFamilyRoboto
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 6, top: 2),
                    child: Row(
                      children: [
                        Row(
                          children: List.generate(5, (index2) =>  Container(
                            child: _viewModel.listProduct[index1].rateStar >= index2 + 1 ?
                            Icon(
                                Icons.star, size: 15, color: AppColors.star
                            ) : _viewModel.listProduct[index1].rateStar == 0 ? null :
                            Icon(
                                Icons.star, size: 15, color: AppColors.gray05
                            ),
                          ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8),
                          child: Text(
                            _viewModel.listProduct[index1].totalRate > 0 ? '(${_viewModel.listProduct[index1].totalRate})' : "",
                            style: TextStyle(
                                color: AppColors.gray03,
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                fontFamily: AppTextStyles.fontFamilyRoboto
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(left: 6, top: 7),
                        child: Text(
                          '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.listProduct[index1].priceSale)}',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: AppColors.redPrice,
                              fontSize: AppFontSize.fontText,
                              fontWeight: FontWeight.w500,
                              fontFamily: AppTextStyles.fontFamilyRoboto
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 6, top: 10),
                        child: Text(
                          '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.listProduct[index1].priceProduct)}',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: AppColors.gray03,
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              fontFamily: AppTextStyles.fontFamilyRoboto,
                              decoration: TextDecoration.lineThrough
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
            ),
          )
        ),
      ],
    );
  }

  Widget _buildNavigateBar(){
    return Container(
      height: 84,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
              color: AppColors.gray05,
              blurRadius: 10
          )]
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            width: 160 * scaleWidth,
            height: 40,
            margin: EdgeInsets.only(left: 16, right: 9),
            child: TextButton(
              onPressed: (){
                _buildShowBottomSheetAddCart();
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(AppColors.gray05),
              ),
              child: Text(
                "Thêm vào giỏ",
                style: TextStyle(
                    fontSize: AppFontSize.fontText,
                    fontWeight: FontWeight.w500,
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    color: AppColors.gray01
                ),
              )
            ),
          ),
          Container(
            width: 160 * scaleWidth,
            height: 40,
            margin: EdgeInsets.only(right: 16),
            child: TextButton(
                onPressed: (){
                  _buildShowBottomSheetBuy();
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(AppColors.mint),
                ),
                child: Text(
                  "Mua ngay",
                  style: TextStyle(
                      fontSize: AppFontSize.fontText,
                      fontWeight: FontWeight.w500,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: Colors.white
                  ),
                )
            ),
          )
        ],
      ),
    );
  }

  void _buildShowBottomSheetAddCart(){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState){
                return Container(
                    width: Get.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom,
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                            left: 330,
                            bottom: 395,
                            child: GestureDetector(
                              onTap: (){
                                Navigator.pop(context);
                              },
                              child: Image.asset(AppImages.png("icon_x"),
                                width: 14,
                                height: 14,
                              ),
                            )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 32),
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 16, right: 16),
                                child: Row(
                                  children: [
                                    Container(
                                        margin: EdgeInsets.only(right: 10),
                                        width: 96,
                                        height: 96,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(4),
                                          child: Image.asset(AppImages.png("home_inazuma"),
                                            fit: BoxFit.cover,
                                          ),
                                        )
                                    ),
                                    Column(
                                      children: [
                                        Container(
                                          width: 222,
                                          child: Text(
                                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sapien.",
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                                color: AppColors.gray01
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: 222,
                                          margin: EdgeInsets.only(top: 12),
                                          child: Text(
                                            '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(300000)}',
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                                color: AppColors.redPrice
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: 222,
                                          margin: EdgeInsets.only(top: 4),
                                          child: Text(
                                            '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(350000)}',
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                                color: AppColors.gray03,
                                                decoration: TextDecoration.lineThrough
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 1,
                                margin: EdgeInsets.only(top: 16),
                                color: AppColors.gray06,
                              ),
                              Container(
                                width: Get.width,
                                margin: EdgeInsets.only(right: 16, top: 24, left: 16),
                                child: Text(
                                  "Dung lượng",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: AppTextStyles.fontFamilyRoboto,
                                      color: AppColors.gray01
                                  ),
                                ),
                              ),
                              Row(
                                children: List.generate(_viewModel.sizeCapacity.length, (index) => ValueListenableBuilder<int>(
                                    valueListenable: _viewModel.selectCapacity,
                                    builder: (context, value, child){
                                      return GestureDetector(
                                        onTap: (){
                                          _viewModel.SelectCapacity(index);
                                          _viewModel.ShowButtonSheet();
                                        },
                                        child: Container(
                                          width: 81,
                                          height: 32,
                                          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(4),
                                              color: value == index ? AppColors.mint02 : AppColors.gray06,
                                              border: Border.all(color: value == index ? AppColors.mint : AppColors.gray06)
                                          ),
                                          child: Center(
                                            child: Text(
                                              _viewModel.sizeCapacity[index],
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: AppTextStyles.fontFamilyRoboto,
                                                  color: AppColors.gray02
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    }
                                )
                                ),
                              ),
                              Container(
                                width: Get.width,
                                margin: EdgeInsets.only(right: 16, top: 24, left: 16),
                                child: Text(
                                  "Số lượng",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: AppTextStyles.fontFamilyRoboto,
                                      color: AppColors.gray01
                                  ),
                                ),
                              ),
                              SizedBox(height: 16,),
                              Row(
                                children: [
                                  Container(
                                      margin: EdgeInsets.only(left: 16),
                                      width: 32,
                                      height: 32,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(topLeft: Radius.circular(4), bottomLeft: Radius.circular(4)),
                                          color: AppColors.gray06
                                      ),
                                      child: IconButton(
                                        onPressed: (){
                                          if(_viewModel.count >= 1){
                                            setState(() {
                                              _viewModel.count--;
                                            });
                                          }
                                        },
                                        icon: Icon(Icons.remove, color: AppColors.gray03, size: 15,),
                                      )
                                  ),
                                  Container(
                                      width: 40,
                                      height: 32,
                                      decoration: BoxDecoration(
                                          border: Border.all(color: AppColors.gray06)
                                      ),
                                      child: Center(
                                        child: Text(
                                          '${_viewModel.count}',
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: AppTextStyles.fontFamilyRoboto,
                                              color: AppColors.gray01
                                          ),
                                        ),
                                      )
                                  ),
                                  Container(
                                      width: 32,
                                      height: 32,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(topRight: Radius.circular(4), bottomRight: Radius.circular(4)),
                                          color: AppColors.gray06
                                      ),
                                      child: IconButton(
                                        onPressed: (){
                                          setState((){
                                            _viewModel.count++;
                                          });
                                        },
                                        icon: Icon(Icons.add, color: AppColors.gray03, size: 15,),
                                      )
                                  ),
                                ],
                              ),
                              SizedBox(height: 24,),
                              Container(
                                  height: 84,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: [BoxShadow(
                                          color: AppColors.gray05,
                                          blurRadius: 10
                                      )]
                                  ),
                                  child: Container(
                                    width: Get.width,
                                    margin: EdgeInsets.only(left: 20, top: 10, right: 20, bottom: 25),
                                    child: ValueListenableBuilder<bool>(
                                        valueListenable: _viewModel.isShowButtonSheet,
                                        builder: (context, value, child){
                                          return TextButton(
                                            style: ButtonStyle(
                                              backgroundColor: value ? MaterialStateProperty.all<Color>(AppColors.mint) : MaterialStateProperty.all<Color>(AppColors.gray07),
                                            ),
                                            onPressed: (){},
                                            child: Text(
                                              "Thêm vào giỏ",
                                              style: TextStyle(
                                                  fontSize: AppFontSize.fontText,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: AppTextStyles.fontFamilyRoboto,
                                                  color: value ? Colors.white : AppColors.gray03
                                              ),
                                            ),
                                          );
                                        }
                                    ),
                                  )
                              )
                            ],
                          ),
                        ),
                      ],
                    )
                );
              }
          );
        }
    );
  }

  void _buildShowBottomSheetBuy(){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState){
                return Container(
                    width: Get.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom,
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                            left: 330,
                            bottom: 395,
                            child: GestureDetector(
                              onTap: (){
                                Navigator.pop(context);
                              },
                              child: Image.asset(AppImages.png("icon_x"),
                                width: 14,
                                height: 14,
                              ),
                            )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 32),
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 16, right: 16),
                                child: Row(
                                  children: [
                                    Container(
                                        margin: EdgeInsets.only(right: 10),
                                        width: 96,
                                        height: 96,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(4),
                                          child: Image.asset(AppImages.png("home_inazuma"),
                                            fit: BoxFit.cover,
                                          ),
                                        )
                                    ),
                                    Column(
                                      children: [
                                        Container(
                                          width: 222,
                                          child: Text(
                                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sapien.",
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                                color: AppColors.gray01
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: 222,
                                          margin: EdgeInsets.only(top: 12),
                                          child: Text(
                                            '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(300000)}',
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                                color: AppColors.redPrice
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: 222,
                                          margin: EdgeInsets.only(top: 4),
                                          child: Text(
                                            '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(350000)}',
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                                color: AppColors.gray03,
                                                decoration: TextDecoration.lineThrough
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 1,
                                margin: EdgeInsets.only(top: 16),
                                color: AppColors.gray06,
                              ),
                              Container(
                                width: Get.width,
                                margin: EdgeInsets.only(right: 16, top: 24, left: 16),
                                child: Text(
                                  "Dung lượng",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: AppTextStyles.fontFamilyRoboto,
                                      color: AppColors.gray01
                                  ),
                                ),
                              ),
                              Row(
                                children: List.generate(_viewModel.sizeCapacity.length, (index) => ValueListenableBuilder<int>(
                                    valueListenable: _viewModel.selectCapacity,
                                    builder: (context, value, child){
                                      return GestureDetector(
                                        onTap: (){
                                          _viewModel.SelectCapacity(index);
                                          _viewModel.ShowButtonSheet();
                                        },
                                        child: Container(
                                          width: 81,
                                          height: 32,
                                          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(4),
                                              color: value == index ? AppColors.mint02 : AppColors.gray06,
                                              border: Border.all(color: value == index ? AppColors.mint : AppColors.gray06)
                                          ),
                                          child: Center(
                                            child: Text(
                                              _viewModel.sizeCapacity[index],
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: AppTextStyles.fontFamilyRoboto,
                                                  color: AppColors.gray02
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    }
                                )
                                ),
                              ),
                              Container(
                                width: Get.width,
                                margin: EdgeInsets.only(right: 16, top: 24, left: 16),
                                child: Text(
                                  "Số lượng",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: AppTextStyles.fontFamilyRoboto,
                                      color: AppColors.gray01
                                  ),
                                ),
                              ),
                              SizedBox(height: 16,),
                              Row(
                                children: [
                                  Container(
                                      margin: EdgeInsets.only(left: 16),
                                      width: 32,
                                      height: 32,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(topLeft: Radius.circular(4), bottomLeft: Radius.circular(4)),
                                          color: AppColors.gray06
                                      ),
                                      child: IconButton(
                                        onPressed: (){
                                          if(_viewModel.count >= 1){
                                            setState(() {
                                              _viewModel.count--;
                                            });
                                          }
                                        },
                                        icon: Icon(Icons.remove, color: AppColors.gray03, size: 15,),
                                      )
                                  ),
                                  Container(
                                      width: 40,
                                      height: 32,
                                      decoration: BoxDecoration(
                                          border: Border.all(color: AppColors.gray06)
                                      ),
                                      child: Center(
                                        child: Text(
                                          '${_viewModel.count}',
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: AppTextStyles.fontFamilyRoboto,
                                              color: AppColors.gray01
                                          ),
                                        ),
                                      )
                                  ),
                                  Container(
                                      width: 32,
                                      height: 32,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(topRight: Radius.circular(4), bottomRight: Radius.circular(4)),
                                          color: AppColors.gray06
                                      ),
                                      child: IconButton(
                                        onPressed: (){
                                          setState((){
                                            _viewModel.count++;
                                          });
                                        },
                                        icon: Icon(Icons.add, color: AppColors.gray03, size: 15,),
                                      )
                                  ),
                                ],
                              ),
                              SizedBox(height: 24,),
                              Container(
                                  height: 84,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: [BoxShadow(
                                          color: AppColors.gray05,
                                          blurRadius: 10
                                      )]
                                  ),
                                  child: Container(
                                    width: Get.width,
                                    margin: EdgeInsets.only(left: 20, top: 10, right: 20, bottom: 25),
                                    child: ValueListenableBuilder<bool>(
                                        valueListenable: _viewModel.isShowButtonSheet,
                                        builder: (context, value, child){
                                          return TextButton(
                                            style: ButtonStyle(
                                              backgroundColor: value ? MaterialStateProperty.all<Color>(AppColors.mint) : MaterialStateProperty.all<Color>(AppColors.gray07),
                                            ),
                                            onPressed: (){},
                                            child: Text(
                                              "Mua ngay",
                                              style: TextStyle(
                                                  fontSize: AppFontSize.fontText,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: AppTextStyles.fontFamilyRoboto,
                                                  color: value ? Colors.white : AppColors.gray03
                                              ),
                                            ),
                                          );
                                        }
                                    ),
                                  )
                              )
                            ],
                          ),
                        ),
                      ],
                    )
                );
              }
          );
        }
    );
  }
}
