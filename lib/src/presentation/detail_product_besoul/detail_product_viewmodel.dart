import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:get/get.dart';

import '../../configs/constanst/app_constants.dart';
import '../presentation.dart';

class DetailProductViewModel extends BaseViewModel {
  init() async {
    scrollController.addListener(() {
      if(scrollController.offset == scrollController.position.maxScrollExtent){
        showFloatButton.value = true;
      }
      else{
        showFloatButton.value = false;
      }
    });
  }

  int indexSlide = 0;

  int count = 1;

  bool isExpanded = false;

  bool isShowBottomSheet = false;

  final ScrollController scrollController = ScrollController();

  final CarouselController slideController = CarouselController();

  ValueNotifier<bool> showFloatButton = ValueNotifier(false);

  ValueNotifier<int> selectCapacity = ValueNotifier(-1);

  ValueNotifier<bool> isShowButtonSheet = ValueNotifier(false);

  void SelectCapacity(int index){
    switch (index){
      case 0:
        selectCapacity.value = 0;
        break;
      case 1:
        selectCapacity.value = 1;
        break;
      case 2:
        selectCapacity.value = 2;
        break;
    }
  }

  bool ShowButtonSheet(){
    if (selectCapacity.value >= 0){
      return isShowButtonSheet.value = true;
    }
    else{
      return isShowButtonSheet.value = false;
    }
  }

  List<String> sizeCapacity = ["150ml", "350ml", "550ml"];

  List<Image> images = [
    Image.asset(AppImages.png("product_detail"), fit: BoxFit.fill,),
    Image.asset(AppImages.png("home_inazuma"), fit: BoxFit.fill,),
    Image.asset(AppImages.png("product_detail"), fit: BoxFit.fill,),
    Image.asset(AppImages.png("home_inazuma1"), fit: BoxFit.fill,),
  ];

  String text1 = "Kem dưỡng ẩm trà xanh innisfree Green Tea Seed Cream, thấm nhanh, giúp làn da thiếu nước trở nên mịn màng. Phù hợp cho da khô và da thường.";
  String text2 = "1. Xây dựng 92.3% hàng rào dưỡng ẩm chỉ trong 30 phút sử dụng \nGreen Tea Seed Serum với công nghệ cấp nước độc quyền, củng cố hàng rào dưỡng ẩm để bảo vệ da khỏi những tổn thương do thiếu nước và cải thiện hệ cân bằng ẩm cho làn da sáng khỏe. Những tổn thương trên da do thiếu nước: Khô và căng da, da sần, kém mịn màng và kém săn chắc do những yếu tố chủ quan lẫn khách quan.\n \n2. Tăng cường độ ẩm cho da sáng khoẻ \nTinh chất cấp nước chuyên sâu, bổ sung kịp thời lượng nước cho da với nước trà xanh tươi Fresh Green Tea Water và 05 loại Hyaluronic Acid dưỡng ẩm từ đậu nành Jeju. Làn da ẩm mượt hơn 710% chỉ sau 1 lần sử dụng. \n\n3. Tăng cường hiệu quả làm dịu da \nHỗ trợ làm dịu da, phù hợp cho làn da dễ kích ứng \n\n4. Thiết kế thân thiện môi trường \nCải tiến với thiết kế vặn đầu chai thông minh, không nắp cài và thân thiện môi trường nhờ giảm 13.8% lượng nhựa trong bao bì so với Green Tea Seed Serum 80ml phiên bản cũ. \n\nSau khi rửa mặt, lấy một lượng thích hợp rồi nhẹ nhàng thoa đều lên mặt và cổ.\n \n*Thứ tự sử dụng:Serum - Skin hoặc Essence-in-Lotion - Kem dưỡng vùng da quanh mắt - Kem dưỡng";

  List<Rate> listRate = [
    Rate(imgProduct: [AppImages.png("product_sua_rua_mat"),AppImages.png("home_inazuma")],
      userName: "Nguyen",
      contentRate: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Penatibus mauris, dignissim porta dignissim mauris rutrum eu urna",
      rateStar: 4,),
    Rate(imgProduct: [AppImages.png("home_inazuma"),AppImages.png("home_inazuma1")],
      userName: "Huy",
      contentRate: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Penatibus mauris, dignissim porta dignissim mauris rutrum eu urna",
      rateStar: 3,),
    Rate(imgProduct: [AppImages.png("home_inazuma1")],
      userName: "Nguyen1",
      contentRate: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Penatibus mauris, dignissim porta dignissim mauris rutrum eu urna",
      rateStar: 1,),
    Rate(imgProduct: [AppImages.png("product_sua_rua_mat"),AppImages.png("home_inazuma1")],
      userName: "Nguyen2",
      contentRate: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Penatibus mauris, dignissim porta dignissim mauris rutrum eu urna",
      rateStar: 4,),
  ];

  List<Product> listProduct = [
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisfhksdabgvkhasbd adsbgaerb",
        rateStar: 4,
        totalRate: 300,
        priceSale: 300000,
        priceProduct: 350000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 3,
        totalRate: 100,
        priceSale: 500000,
        priceProduct: 550000),
    Product(imgProduct: AppImages.png("home_inazuma1"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 3,
        totalRate: 100,
        priceSale: 400000,
        priceProduct: 550000),
    Product(imgProduct: AppImages.png("home_inazuma"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 5,
        totalRate: 100,
        priceSale: 450000,
        priceProduct: 650000),
    Product(imgProduct: AppImages.png("product_sua_rua_mat"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 2,
        totalRate: 100,
        priceSale: 300000,
        priceProduct: 500000),
    Product(imgProduct: AppImages.png("home_inazuma1"),
        nameProduct: "Sữa rửa mặt sạch sâu táo xanh Innisf...",
        rateStar: 4,
        totalRate: 100,
        priceSale: 450000,
        priceProduct: 500000),
  ];
}

class Rate{
  final List<String> imgProduct;
  final String userName;
  final String contentRate;
  final int rateStar;

  Rate({Key ? key, required this.imgProduct, required this.userName, required this.contentRate, required this.rateStar,});
}

class Product{
  final String imgProduct;
  final String nameProduct;
  final int rateStar;
  final int totalRate;
  final int priceSale;
  final int priceProduct;

  Product({Key ? key, required this.imgProduct, required this.nameProduct, required this.rateStar,
    required this.totalRate, required this.priceSale, required this.priceProduct,});
}
