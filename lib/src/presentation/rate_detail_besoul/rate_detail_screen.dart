import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/rate_detail_besoul/rate_detail_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class RateDetailScreen extends StatefulWidget {
  const RateDetailScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<RateDetailScreen> {
  late RateDetailViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<RateDetailViewModel>(
        viewModel: RateDetailViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          _buildRate(),
          Container(
            width: Get.width,
            height: 1,
            color: AppColors.gray09,
          ),
          SizedBox(height: 18,),
          _buildRateDetail()
        ],
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.productDetail);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 55, top: 30),
            child: Text(
              "Xem đánh giá",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildRate(){
    return Container(
      margin: EdgeInsets.only(left: 8, right: 8),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(top: 36, bottom: 30),
            child: Column(
              children: [
                Text(
                  "4.8",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w500,
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    color: AppColors.gray01
                  ),
                ),
                SizedBox(height: 4,),
                Row(
                  children: List.generate(5, (index) => Container(
                    child: 4 >= index + 1 ?
                    Icon(
                        Icons.star, size: 15, color: AppColors.star
                    ) :
                    Icon(
                        Icons.star, size: 15, color: AppColors.gray05
                    ),
                  ),
                  ),
                ),
                SizedBox(height: 6,),
                Text(
                  "484 Đánh giá",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray03
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: 6,),
          Container(
            width: 1,
            height: 90,
            color: AppColors.gray09,
          ),
          SizedBox(width: 6,),
          Container(
            child: Column(
              children: [
                Row(
                  children: List.generate(5, (index) => Container(
                    child: 5 >= index + 1 ?
                    Icon(
                        Icons.star, size: 15, color: AppColors.star
                    ) :
                    Icon(
                        Icons.star, size: 15, color: AppColors.gray05
                    ),
                  ),
                  ),
                ),
                Row(
                  children: List.generate(5, (index) => Container(
                    child: 4 >= index + 1 ?
                    Icon(
                        Icons.star, size: 15, color: AppColors.star
                    ) :
                    Icon(
                        Icons.star, size: 15, color: AppColors.gray05
                    ),
                  ),
                  ),
                ),
                Row(
                  children: List.generate(5, (index) => Container(
                    child: 3 >= index + 1 ?
                    Icon(
                        Icons.star, size: 15, color: AppColors.star
                    ) :
                    Icon(
                        Icons.star, size: 15, color: AppColors.gray05
                    ),
                  ),
                  ),
                ),
                Row(
                  children: List.generate(5, (index) => Container(
                    child: 2 >= index + 1 ?
                    Icon(
                        Icons.star, size: 15, color: AppColors.star
                    ) :
                    Icon(
                        Icons.star, size: 15, color: AppColors.gray05
                    ),
                  ),
                  ),
                ),
                Row(
                  children: List.generate(5, (index) => Container(
                    child: 1 >= index + 1 ?
                    Icon(
                        Icons.star, size: 15, color: AppColors.star
                    ) :
                    Icon(
                        Icons.star, size: 15, color: AppColors.gray05
                    ),
                  ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: 6,),
          Container(
            margin: EdgeInsets.only(top: 15),
            child: Column(
              children: List.generate(5, (index) =>  Stack(
                children: [
                  Positioned(
                      child: Container(
                        width: 130,
                        height: 5,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: AppColors.gray09
                        ),
                      )
                  ),
                  SizedBox(height: 15,),
                  Positioned(
                      child: Container(
                        width: ((_viewModel.listRate[index] / 130) * 100) > 130 ? 130 : ((_viewModel.listRate[index] / 130) * 100),
                        height: 5,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: AppColors.mint
                        ),
                      )
                  )
                ],
              ))
            ),
          ),
          SizedBox(width: 6,),
          Container(
            margin: EdgeInsets.only(top: 2),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: List.generate(_viewModel.listRate.length, (index) => Container(
                margin: EdgeInsets.only(bottom: 1),
                child: Text(
                  '${_viewModel.listRate[index].ceil()}',
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray03
                  ),
                ),
              )
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildRateDetail(){
    return Column(
      children: List.generate(_viewModel.listRateDetail.length, (index) => Container(
        margin: EdgeInsets.only(bottom: 20, left: 16),
        child: Row(
          children: [
            Container(
              height: 110,
              alignment: Alignment.topCenter,
              child: CircleAvatar(
                backgroundImage: AssetImage("assets/images/png/user_icon.png"),
                radius: 20,
              ),
            ),
            SizedBox(width: 12,),
            Container(
              width: 287 * scaleWidth,
              padding: EdgeInsets.all(0),
              child: Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        _viewModel.listRateDetail[index].userName,
                        style: TextStyle(
                            fontSize: AppFontSize.fontText,
                            fontWeight: FontWeight.w500,
                            fontFamily: AppTextStyles.fontFamilyRoboto,
                            color: AppColors.gray01
                        ),
                      ),
                    ),
                    SizedBox(height: 6,),
                    Container(
                      child: Row(
                        children: [
                          Row(
                            children: List.generate(5, (index1) =>  Container(
                              child: _viewModel.listRateDetail[index].rateStar >= index1 + 1 ?
                              Icon(
                                  Icons.star, size: 15, color: AppColors.star
                              ) : _viewModel.listRateDetail[index].rateStar == 0 ? SizedBox() :
                              Icon(
                                  Icons.star, size: 15, color: AppColors.gray05
                              ),
                            ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      child: Text(
                        _viewModel.listRateDetail[index].contentRate,
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: AppColors.gray03
                        ),
                      ),
                    ),
                    SizedBox(height: 12,),
                  ]
              ),
            )
          ],
        ),
      )),
    );
  }
}
