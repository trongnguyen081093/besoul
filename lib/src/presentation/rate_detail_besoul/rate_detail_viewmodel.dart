import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class RateDetailViewModel extends BaseViewModel {
  init() async {}

  List<double> listRate = [23,280,10,9,5];

  List<Rate> listRateDetail = [
    Rate(imgProduct: [AppImages.png("product_sua_rua_mat"),AppImages.png("home_inazuma")],
      userName: "Nguyen",
      contentRate: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Penatibus mauris, dignissim porta dignissim mauris rutrum eu urna",
      rateStar: 4,),
    Rate(imgProduct: [AppImages.png("home_inazuma"),AppImages.png("home_inazuma1")],
      userName: "Huy",
      contentRate: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Penatibus mauris, dignissim porta dignissim mauris rutrum eu urna",
      rateStar: 3,),
    Rate(imgProduct: [AppImages.png("home_inazuma1")],
      userName: "Nguyen1",
      contentRate: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Penatibus mauris, dignissim porta dignissim mauris rutrum eu urna",
      rateStar: 1,),
    Rate(imgProduct: [AppImages.png("product_sua_rua_mat"),AppImages.png("home_inazuma1")],
      userName: "Nguyen2",
      contentRate: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Penatibus mauris, dignissim porta dignissim mauris rutrum eu urna",
      rateStar: 4,),
    Rate(imgProduct: [AppImages.png("product_sua_rua_mat"),AppImages.png("home_inazuma1")],
      userName: "Nguyen2",
      contentRate: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Penatibus mauris, dignissim porta dignissim mauris rutrum eu urna",
      rateStar: 3,),
    Rate(imgProduct: [AppImages.png("product_sua_rua_mat"),AppImages.png("home_inazuma1")],
      userName: "Nguyen2",
      contentRate: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Penatibus mauris, dignissim porta dignissim mauris rutrum eu urna",
      rateStar: 2,),
    Rate(imgProduct: [AppImages.png("product_sua_rua_mat"),AppImages.png("home_inazuma1")],
      userName: "Nguyen2",
      contentRate: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Penatibus mauris, dignissim porta dignissim mauris rutrum eu urna",
      rateStar: 5,),
  ];
}

class Rate{
  final List<String> imgProduct;
  final String userName;
  final String contentRate;
  final int rateStar;

  Rate({Key ? key, required this.imgProduct, required this.userName, required this.contentRate, required this.rateStar,});
}
