import 'package:flutter/cupertino.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class SelectLanguageViewModel extends BaseViewModel {
  init() async {}
  List<PickLanguage> items = [
    PickLanguage(imgCountry: AppImages.imgFlagUnitedKingdom, nameCountry: "English"),
    PickLanguage(imgCountry: AppImages.imgFlagVietNam, nameCountry: "Tiếng Việt"),
    PickLanguage(imgCountry: AppImages.imgFlagSouthKorea, nameCountry: "한국어"),
  ];
}

class PickLanguage{
  final String imgCountry;
  final String nameCountry;

  PickLanguage({Key? key, required this.imgCountry, required this.nameCountry});
}
