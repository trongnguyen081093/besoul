import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/select_language_besoul/select_language_besoul_viewmodel.dart';



import 'package:get/get.dart';

import '../presentation.dart';

class SelectLanguageScreen extends StatefulWidget {
  const SelectLanguageScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<SelectLanguageScreen> {
  late SelectLanguageViewModel _viewModel;


  @override
  Widget build(BuildContext context) {
    return BaseWidget<SelectLanguageViewModel>(
        viewModel: SelectLanguageViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: AppColors.mint,
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Container(
        width: Get.width,
        height: Get.height,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 102),
              child: Text(
                "Chào mừng",
                style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w700,
                  fontSize: 24,
                ),
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              width: 306,
              height: 32,
              child: Text(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In at tincidunt risus cursus donec",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                ),
              ),
            ),
            SizedBox(
              height: 152,
            ),
            Container(
                width: 231,
                height: 127,
                child: CupertinoPicker(
                  onSelectedItemChanged: (value){
                  },
                  backgroundColor: AppColors.mint,
                  itemExtent: 53,
                  selectionOverlay: Column(
                    children: [
                      Container(
                        height: 1,
                        width: 231,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [Colors.white.withOpacity(0.5), Colors.white, Colors.white.withOpacity(0.5)]
                          )
                        ),
                      ),
                      SizedBox(height: 50,),
                      Container(
                        height: 1,
                        width: 231,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [Colors.white.withOpacity(0.5), Colors.white, Colors.white.withOpacity(0.5)]
                            )
                        ),
                      ),
                    ],
                  ),
                  scrollController: FixedExtentScrollController(initialItem: 1),
                  children: List.generate(_viewModel.items.length, (index) => _buildChooseFlag(index)),
                )
            ),
            SizedBox(height: 174,),
            GestureDetector(
              onTap: (){
                Get.toNamed(Routers.selectCountryBesoul);
              },
              child: Container(
                width: 83,
                height: 83,
                child: AvatarGlow(
                  endRadius: 94,
                  animate: true,
                  duration: Duration(milliseconds: 2000),
                  repeatPauseDuration: Duration(milliseconds: 100),
                  child: Material(
                    elevation: 11,
                    shape: CircleBorder(),
                    child: CircleAvatar(
                      backgroundColor: Colors.grey[100],
                      child: Image.asset(
                        AppImages.imgNext,
                        height: 16,
                        width: 8,
                      ),
                      radius: 30,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildChooseFlag(index){
    return Padding(
      padding: const EdgeInsets.fromLTRB(39, 0, 0, 0),
      child: Row(
        children: [
          Image.asset(
            _viewModel.items[index].imgCountry,
            width: 20,
            height: 20,
            errorBuilder: (context, child, stackTrace) => Image.asset(
              _viewModel.items[index].imgCountry,
              width: 20,
              height: 20,),
          ),
          SizedBox(width: 27,),
          Text(
            _viewModel.items[index].nameCountry,
            style: TextStyle(
                fontSize: 18,
                fontFamily: AppTextStyles.fontFamilyRoboto,
                fontWeight: FontWeight.w400,
                color: Colors.white
            ),
          )
        ],
      ),
    );
  }
}
