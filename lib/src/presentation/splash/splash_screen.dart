import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<SplashViewModel>(
        viewModel: SplashViewModel(),
        onViewModelReady: (viewModel) {
          viewModel.init();
        },
        child: _buildBody(context),
        childMobile: _buildBodyMobile(context),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: _buildBody(context),
          );
        });
  }

  Widget _buildBodyMobile(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: Image.asset(AppImages.imgIntro),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: Stack(
          children: [
            Image.asset(
              AppImages.imgIntro,
              fit: BoxFit.cover,
              width: Get.width,),
            Positioned(
              bottom: 48,
              left: 16,
              child: Image.asset(
                AppImages.imgLogoIntro,
                width: 146,
                height: 28,
              )
            ),
          ],
        )
      ),
    );
  }
}
