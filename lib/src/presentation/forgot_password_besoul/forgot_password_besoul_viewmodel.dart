import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class ForgotPasswordViewModel extends BaseViewModel {
  final TextEditingController textController = TextEditingController();
  ValueNotifier<bool> isButtonDisable = ValueNotifier(false);
  ValueNotifier<bool> isIconDisable = ValueNotifier(false);

  init() async {
    textController.addListener(() {
      validateEmailOrPhoneNumber();
      showIcon();
    });
  }

  @override
  void dispose() {
    super.dispose();
    textController.dispose();
    isButtonDisable.dispose();
  }

  void ClearText(){
    textController.text = "";
  }

  void showIcon(){
    if (textController.text.isNotEmpty){
      isIconDisable.value = true;
    } else {
      isIconDisable.value = false;
    }
  }

  void validateEmailOrPhoneNumber(){
    RegExp regexPhone = new RegExp(r'^(?:[+0]9)?[0-9]{10}$');
    RegExp regexEmail = new RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
    if (textController.text.isNotEmpty){
      if (regexPhone.hasMatch(textController.text)) {
        isButtonDisable.value = true;
      } else if (regexEmail.hasMatch(textController.text)) {
        isButtonDisable.value = true;
      } else {
        isButtonDisable.value = false;
      }
    } else {
      isButtonDisable.value = false;
    }
  }
}
