import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/forgot_password_besoul/forgot_password_besoul_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<ForgotPasswordScreen> {
  late ForgotPasswordViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ForgotPasswordViewModel>(
        viewModel: ForgotPasswordViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
            resizeToAvoidBottomInset: true,
          );
        });
  }

  Widget _buildBody() {
    return Container(
      width: Get.width,
      height: Get.height,
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 20, top: 50),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.loginBesoul);
                },
                icon: Icon(Icons.arrow_back_ios, color: AppColors.gray03, size: 24,)),
          ),
          Container(
            width: Get.width,
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(left: 20, right: 20, top: 30),
            child: Text(
              "Xác minh danh tính của bạn",
              style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w700,
                  fontSize: 22,
                  color: AppColors.gray01
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Container(
            width: Get.width,
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(left: 20,right: 20, top: 8),
            child: Text(
              "Nhập email  hoặc số điện thoại của bạn và chúng tôi sẽ gửi một mã để thiết lập lại mật khẩu",
              style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: AppColors.gray01
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Container(
            width: Get.width,
            margin: EdgeInsets.only(left: 20, right: 20,top: 24),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                border: Border.all(
                    style: BorderStyle.solid,
                    color: AppColors.gray05)
            ),
            child: TextField(
              controller: _viewModel.textController,
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                  isDense: true,
                  border: InputBorder.none,
                  hintText: "Nhập email hoặc số điện thoại",
                  hintStyle: TextStyle(
                      fontSize: 16,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      fontWeight: FontWeight.w400,
                      color: AppColors.gray03
                  ),
                  suffixIcon: Align(
                      widthFactor: 1,
                      heightFactor: 1,
                      child: ValueListenableBuilder<bool>(
                        valueListenable: _viewModel.isIconDisable,
                        builder: (context,value,child) => IconButton(
                          onPressed: (){ _viewModel.ClearText();},
                          icon: Icon(value ? Icons.cancel : null, color: AppColors.gray04, size: 12),
                        ),
                      )
                  ),
                  contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 12)
              ),
            ),
          ),
          Spacer(),
          Container(
            width: Get.width,
            height: 1,
            margin: EdgeInsets.only(bottom: 8),
            decoration: BoxDecoration(
                border: Border.all(color: AppColors.gray06)
            ),
          ),
          Container(
            width: Get.width,
            height: 54,
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 12,),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
            ),
            child: ValueListenableBuilder<bool>(
                valueListenable: _viewModel.isButtonDisable,
                builder: (context,value,child) => TextButton(
                  style: TextButton.styleFrom(
                      backgroundColor: value ? AppColors.mint : AppColors.gray07
                  ),
                  onPressed: value ? () => Get.toNamed(Routers.otpForgotPassword) : null,
                  child: Text(
                    "Tiếp tục",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: AppFontSize.fontText
                    ),
                  ),
                )),
          )
        ],
      ),
    );
  }
}
