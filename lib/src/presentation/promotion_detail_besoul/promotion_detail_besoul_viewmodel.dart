import 'package:get/get.dart';

import '../presentation.dart';

class PromotionDetailViewModel extends BaseViewModel {
  init() async {}

  String content1 = "Chương trình ưu đãi dành cho chủ sở hữu điện thoại Samsung chính hãng khi mua Samsung Galaxy A52s 5G (SM-A528) tại hệ thống Siêu thị Điện Máy Chợ Lớn";
  String content2 = "- Giảm ngay 800.000 đồng khi mua điện thoại A52s 5G (SM-A528) với Mã ưu đãi được lấy trên ứng dụng Quà tặng Galaxy. Mã ưu đãi dành riêng cho khách hàng đang sở hữu sản phẩm Samsung chính hãng thuộc một trong các dòng sản phẩm sau: J7 (SM-J730, SM-J710, SM-J700), J7 Duo (SM-J720), A70 (SM-A705), A50 (SM-A505), A50s (SM-A507), A5 2015 (SM-A500), A7 2015 (SM-A700), A8 2015 (SM-A800), A5 2016 (SM-A510), A7 2016 (SM-A710), A5 2017 (SM-A520), A7 2017 (SM-A720), A8 2018 (SM-A530), A8+ 2018 (SM-A730), A8 Star (SM- G885), A9 2018 (SM-A920), A7 (2018) (SM-A750), A80 (SM-A805)";
  String content3 = "- Chương trình khuyến mại này được áp dụng đồng thời với chương trình Trả góp 0% lãi suất trong vòng 6 hoặc 9 tháng qua nhà tài chính (Home Credit/FE Credit).";
  String content4 = "- Khách hàng tải và cài đặt ứng dụng Quà Tặng Galaxy từ Galaxy Store trên thiết bị Samsung được áp dụng chương trình.";
  String content5 = "- Mở ứng dụng Quà tặng Galaxy (Galaxy Gift). Click vào chương trình “Giảm ngay 800.000đ khi mua Galaxy A52s 5G” và chọn “Lấy mã ưu đãi”";
  String content6 = "- Khách hàng chỉ có thể lấy 01 mã ưu đãi duy nhất trên mỗi thiết bị, có thể sử dụng ngay trong lần giao dịch và chỉ có giá trị sử dụng 01 lần.";
  String content7 = "- Khách hàng mang thiết bị Samsung có mã ưu đãi của chương trình đến các cửa hàng để mua sản phẩm Samsung Galaxy A52s 5G (A528), xác thực mã ưu đãi và được giảm ngay 800.000đ trên giá bán lẻ.";
  String content8 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ornare lacinia arcu, amet leo, nisi, cras sed. Sed elementum, sapien facilisi id accumsan quis. Arcu nunc, mattis leo faucibus tellus urna. Magna duis dapibus in sed accumsan, lorem id in. Eu leo, in convallis in. Eleifend ut egestas non tincidunt non libero, non id. Luctus porttitor feugiat eleifend morbi sapien pretium. Pellentesque vitae sed vitae id sagittis justo cursus mauris nec. Mauris amet fermentum est augue sed ridiculus etiam. Dignissim volutpat, ut adipiscing non volutpat leo faucibus. Augue velit accumsan pulvinar sed integer.\nEget amet, lectus viverra pretium. Mattis bibendum tortor sed urna id. In habitant eget et orci platea et morbi. Ut ultricies aliquet netus venenatis commodo. Sollicitudin tristique eu bibendum quisque mattis lectus. \nErat proin arcu amet mattis erat amet. Quam mauris ut vitae, fames. Turpis aliquet nisi nunc adipiscing in. Interdum blandit id in vitae. Lobortis in egestas venenatis aliquet lectus sagittis, diam. Nisl duis nisl ullamcorper ipsum sit arcu augue egestas posuere.";
}
