import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/promotion_detail_besoul/promotion_detail_besoul_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class PromotionDetailScreen extends StatefulWidget {
  const PromotionDetailScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<PromotionDetailScreen> {
  late PromotionDetailViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<PromotionDetailViewModel>(
        viewModel: PromotionDetailViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return Container(
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            _buildAppBar(),
            _buildContent(),
            SizedBox(height: 20,)
          ],
        ),
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.promotionBesoul);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 45, top: 30),
            child: Text(
              "Tin khuyến mãi",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildContent(){
    return Container(
      width: Get.width,
      margin: EdgeInsets.only(left: 16, right: 16),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 16),
            child: Text(
              "ƯU ĐÃI \“ĐẶC QUYỀN NÂNG CẤP \" GALAXY A52s 5G",
              style: TextStyle(
                color: AppColors.gray01,
                fontSize: 20,
                fontWeight: FontWeight.w500,
                fontFamily: AppTextStyles.fontFamilyRoboto
              )
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 2),
            alignment: Alignment.centerLeft,
            child: Text(
              "Ngày đăng: 15/11/2021",
              style: TextStyle(
                color: AppColors.gray03,
                fontSize: 15,
                fontWeight: FontWeight.w400,
                fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            alignment: Alignment.centerLeft,
            child: Text(
              _viewModel.content1,
              style: TextStyle(
                  color: AppColors.gray01,
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          Container(
            height: 200,
            margin: EdgeInsets.only(top: 16),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
            ),
            child: Image.asset(
              AppImages.png("home_inazuma"),
            )
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            alignment: Alignment.centerLeft,
            child: Text(
                "I. GIẢM NGAY 800.000 QUA GALAXY GIFT",
                style: TextStyle(
                    color: AppColors.gray01,
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                    fontFamily: AppTextStyles.fontFamilyRoboto
                )
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 4),
            alignment: Alignment.centerLeft,
            child: Text(
                "1. Thời gian áp dụng chương trình:",
                style: TextStyle(
                    color: AppColors.gray01,
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                    fontFamily: AppTextStyles.fontFamilyRoboto
                )
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 4),
            alignment: Alignment.centerLeft,
            child: Text(
              "Từ ngày 01/11/2021 đến hết ngày 15/11/2021",
              style: TextStyle(
                  color: AppColors.gray01,
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            alignment: Alignment.centerLeft,
            child: Text(
                "2. Ưu đãi đặc biệt:",
                style: TextStyle(
                    color: AppColors.gray01,
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                    fontFamily: AppTextStyles.fontFamilyRoboto
                )
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 4),
            alignment: Alignment.centerLeft,
            child: Text(
              _viewModel.content2,
              style: TextStyle(
                  color: AppColors.gray01,
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 8),
            alignment: Alignment.centerLeft,
            child: Text(
              _viewModel.content3,
              style: TextStyle(
                  color: AppColors.gray01,
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            alignment: Alignment.centerLeft,
            child: Text(
                "3. Hướng dẫn mua hàng:",
                style: TextStyle(
                    color: AppColors.gray01,
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                    fontFamily: AppTextStyles.fontFamilyRoboto
                )
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 4),
            alignment: Alignment.centerLeft,
            child: Text(
              _viewModel.content4,
              style: TextStyle(
                  color: AppColors.gray01,
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 8),
            alignment: Alignment.centerLeft,
            child: Text(
              _viewModel.content5,
              style: TextStyle(
                  color: AppColors.gray01,
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 8),
            alignment: Alignment.centerLeft,
            child: Text(
              _viewModel.content6,
              style: TextStyle(
                  color: AppColors.gray01,
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 8),
            alignment: Alignment.centerLeft,
            child: Text(
              _viewModel.content7,
              style: TextStyle(
                  color: AppColors.gray01,
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            alignment: Alignment.centerLeft,
            child: Text(
                "II. ƯU ĐÃI ĐẶC QUYỀN NÂNG CẤP - NHẬN NGAY 1 TRIỆU ĐIỂM THƯỞNG",
                style: TextStyle(
                    color: AppColors.gray01,
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                    fontFamily: AppTextStyles.fontFamilyRoboto
                )
            ),
          ),
          Container(
              height: 200,
              margin: EdgeInsets.only(top: 16),
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
              ),
              child: Image.asset(
                AppImages.png("home_inazuma"),
              )
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            alignment: Alignment.centerLeft,
            child: Text(
                "1. Thời gian và phạm vi chương trình:",
                style: TextStyle(
                    color: AppColors.gray01,
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                    fontFamily: AppTextStyles.fontFamilyRoboto
                )
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 4),
            alignment: Alignment.centerLeft,
            child: Text(
              _viewModel.content8,
              style: TextStyle(
                  color: AppColors.gray01,
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
        ],
      ),
    );
  }
}
