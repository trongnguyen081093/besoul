import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/set_new_password/set_new_password_viewmodel.dart';
import 'package:flutter_app/src/utils/app_valid.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class SetNewPasswordScreen extends StatefulWidget {
  const SetNewPasswordScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<SetNewPasswordScreen> {
  late SetNewPasswordViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SetNewPasswordViewModel>(
        viewModel: SetNewPasswordViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return Container(
      width: Get.width,
      height: Get.height,
      child: Form(
        key: _viewModel.formKey,
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 20, top: 50),
              child: IconButton(
                  onPressed: (){
                    Get.toNamed(Routers.loginBesoul);
                  },
                  icon: Icon(Icons.arrow_back_ios, color: AppColors.gray03, size: 24,)),
            ),
            Container(
              width: Get.width,
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(left: 20, right: 20, top: 30),
              child: Text(
                "Đặt lại mật khẩu",
                style: TextStyle(
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    fontWeight: FontWeight.w700,
                    fontSize: 22,
                    color: AppColors.gray01
                ),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              width: Get.width,
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(left: 20,right: 20, top: 8),
              child: Text(
                "Ít nhất có 8 ký tự, có phân biệt chữ hoa, chữ thường",
                style: TextStyle(
                    fontFamily: AppTextStyles.fontFamilyRoboto,
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: AppColors.gray01
                ),
                textAlign: TextAlign.left,
              ),
            ),
            _buildPassWord(),
            _buildPassWordConfirm(),
            Spacer(),
            Container(
              width: Get.width,
              height: 1,
              margin: EdgeInsets.only(bottom: 8),
              decoration: BoxDecoration(
                  border: Border.all(color: AppColors.gray06)
              ),
            ),
            Container(
              width: Get.width,
              height: 54,
              margin: EdgeInsets.only(left: 20, right: 20, bottom: 12,),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
              ),
              child: ValueListenableBuilder<bool>(
                  valueListenable: _viewModel.isButtonDisable,
                  builder: (context,value,child) => TextButton(
                    style: TextButton.styleFrom(
                        backgroundColor: value ? AppColors.mint : AppColors.gray07
                    ),
                    onPressed: (){
                      if(value){
                        if(_viewModel.formKey.currentState!.validate()){
                          Get.toNamed(Routers.loginBesoul);
                        } else {
                          if(_viewModel.password1Controller.text != _viewModel.password2Controller.text){
                              setState(() {
                                _viewModel.changeIcon = Icons.warning;
                                _viewModel.changeColor = Colors.red;
                            });
                          }else{
                            _viewModel.changeIcon = Icons.visibility_outlined;
                            _viewModel.changeColor = AppColors.gray04;
                         }
                        }
                      } else {
                        return null;
                      }
                    },
                    child: Text(
                      "Tiếp tục",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: AppFontSize.fontText
                      ),
                    ),
                  )),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildPassWord(){
    return Container(
      width: Get.width,
      margin: EdgeInsets.only(left: 20, right: 20,top: 24),
      child: ValueListenableBuilder(
        valueListenable: _viewModel.isShowPass,
        builder: (context, value, child) => TextFormField(
          controller: _viewModel.password1Controller,
          style: TextStyle(color: Colors.black),
          obscureText: _viewModel.isShowPass.value,
          validator: AppValid.validatePassword(),
          decoration: InputDecoration(
            border: InputBorder.none,
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: Colors.black, width: 1)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: AppColors.gray05, width: 1)),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: Colors.red, width: 1)),
            focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: Colors.black, width: 1)),
            hintText: "Nhập mật khẩu",
            hintStyle: TextStyle(
                fontSize: 14,
                fontFamily: AppTextStyles.fontFamilyRoboto,
                fontWeight: FontWeight.w400,
                color: AppColors.gray03
            ),
            contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
            suffixIcon: Align(
                widthFactor: 1,
                heightFactor: 1,
                child: ValueListenableBuilder<bool>(
                  valueListenable: _viewModel.isShowPass,
                  builder: (context, value, child) => IconButton(
                    onPressed: (){
                      _viewModel.showPass();
                    },
                    icon: Icon(value ? Icons.visibility_outlined : Icons.visibility_off_outlined , color: AppColors.gray04),
                  ),
                )
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildPassWordConfirm(){
    return Container(
      width: Get.width,
      margin: EdgeInsets.only(left: 20, right: 20,top: 16),
      child: ValueListenableBuilder(
        valueListenable: _viewModel.isShowPassConfirm,
        builder: (context, value, child) => TextFormField(
          controller: _viewModel.password2Controller,
          style: TextStyle(color: Colors.black),
          obscureText: _viewModel.isShowPassConfirm.value,
          validator: AppValid.validatePasswordConfirm(_viewModel.password1Controller),
          decoration: InputDecoration(
            border: InputBorder.none,
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: Colors.black, width: 1)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: AppColors.gray05, width: 1)),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: Colors.red, width: 1)),
            focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: Colors.black, width: 1)),
            hintText: "Nhập lại mật khẩu",
            hintStyle: TextStyle(
                fontSize: 14,
                fontFamily: AppTextStyles.fontFamilyRoboto,
                fontWeight: FontWeight.w400,
                color: AppColors.gray03
            ),
            contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
            suffixIcon: Align(
                widthFactor: 1,
                heightFactor: 1,
                child: IconButton(
                  onPressed: (){
                    if(_viewModel.isShowPassConfirm.value == true){
                      setState(() {
                        _viewModel.isShowPassConfirm.value = false;
                        _viewModel.changeIcon = Icons.visibility_off_outlined;
                      });
                    } else {
                      setState(() {
                        _viewModel.isShowPassConfirm.value = true;
                        _viewModel.changeIcon = Icons.visibility_outlined;
                      });
                    }
                  },
                  icon: Icon(_viewModel.changeIcon , color: _viewModel.changeColor,),
                ),
            ),
          ),
        ),
      ),
    );
  }
}
