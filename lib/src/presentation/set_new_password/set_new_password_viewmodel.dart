import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class SetNewPasswordViewModel extends BaseViewModel {

  ValueNotifier<bool> isButtonDisable = ValueNotifier(false);

  ValueNotifier<bool> isShowPass = ValueNotifier(true);

  ValueNotifier<bool> isShowPassConfirm = ValueNotifier(true);

  final TextEditingController password1Controller = TextEditingController();

  final TextEditingController password2Controller = TextEditingController();

  final formKey = GlobalKey<FormState>();

  var changeIcon = Icons.visibility_outlined;

  var changeColor = AppColors.gray04;

  init() async {
    password2Controller.addListener(() {
      checkPassWordConfirm();
    });
  }

  @override
  void dispose(){
    super.dispose();
    isButtonDisable.dispose();
    isShowPass.dispose();
    isShowPassConfirm.dispose();
  }

  bool showPass(){
    if(isShowPass.value)
      {
        return isShowPass.value = false;
      } else {
      return isShowPass.value = true;
    }
  }

  void checkPassWordConfirm(){
    if (password1Controller.text.isEmpty || password2Controller.text.isEmpty){
      isButtonDisable.value = false;
    }
    else{
      isButtonDisable.value = true;
    }
  }
}

