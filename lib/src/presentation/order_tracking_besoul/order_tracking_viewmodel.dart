import 'package:get/get.dart';

import '../presentation.dart';

class OrderTrackingViewModel extends BaseViewModel {
  init() async {}

  String name1 = "Đã nhận hàng";
  String name2 = "Đang vận chuyển";
  String name3 = "Đóng gói xong";
  String name4 = "Đã tiếp nhận đơn hàng";
}
