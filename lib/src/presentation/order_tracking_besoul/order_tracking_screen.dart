import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/contact_besoul/contact_besoul_screen.dart';
import 'package:flutter_app/src/presentation/order_tracking_besoul/order_tracking_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class OrderTrackingScreen extends StatefulWidget {
  const OrderTrackingScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<OrderTrackingScreen> {
  late OrderTrackingViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<OrderTrackingViewModel>(
        viewModel: OrderTrackingViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return Column(
      children: [
        _buildAppBar(),
        SizedBox(height: 16,),
        _buildTitle(),
        _buildContent()
      ],
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.transportationBesoul);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 35, top: 30),
            child: Text(
              "Theo dõi đơn hàng",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTitle(){
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    "Mã đơn hàng",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: AppColors.gray01
                    ),
                  )
                ),
                Container(
                  child: Text(
                    "#123456789G",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.mart
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 8,),
          Container(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Row(
              children: [
                Expanded(
                    child: Text(
                      "Thời gian đặt hàng",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray01
                      ),
                    )
                ),
                Container(
                  child: Text(
                    "10-11-2021",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray03
                    ),
                  ),
                ),
                SizedBox(width: 12,),
                Container(
                  child: Text(
                    "10:29",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTextStyles.fontFamilyRoboto,
                        color: AppColors.gray03
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 16,),
          Container(
            height: 5,
            color: AppColors.gray06,
          )
        ],
      ),
    );
  }

  Widget _buildContent(){
    return Container(
      padding: EdgeInsets.only(left: 20, top: 27),
      child: Stack(
        children: [
          Positioned(
            top: 10,
              left: 4.5,
              child: Container(
                width: 1,
                height: 96,
                color: AppColors.gray12,
              )
          ),
          Column(
            children: [
              _buildList(_viewModel.name1, AppColors.mint, AppColors.gray01, AppColors.mint),
              SizedBox(height: 16,),
              _buildList(_viewModel.name2, AppColors.gray12, AppColors.gray03, AppColors.gray03),
              SizedBox(height: 16,),
              _buildList(_viewModel.name3, AppColors.gray12, AppColors.gray03, AppColors.gray03),
              SizedBox(height: 16,),
              _buildList(_viewModel.name4, AppColors.gray12, AppColors.gray03, AppColors.gray03),
            ],
          ),
        ],
      )
    );
  }


  Widget _buildList(String name, Color color1, Color color2, Color color3){
    return Container(
      child: Row(
        children: [
          Container(
            width: 10,
            height: 10,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: color1
            ),
          ),
          SizedBox(width: 12,),
          Container(
            child: Text(
              "11:10",
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: color2
              ),
            ),
          ),
          SizedBox(width: 5,),
          Container(
            child: Text(
              "17-07-2021",
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: color2
              ),
            ),
          ),
          SizedBox(width: 16,),
          Container(
            child: Text(
              name,
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: color3
              ),
            ),
          ),
        ],
      ),
    );
  }
}
