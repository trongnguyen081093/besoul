import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class ListTranportationViewModel extends BaseViewModel {
  init() async {}
  
  List<ListTransport> listTransport = [
    ListTransport(imgTransport: Image.asset(AppImages.png("icon_viettel_port"), width: 34, height: 17,), nameTransport: "Viettel Post:", price: 15000),
    ListTransport(imgTransport: Image.asset(AppImages.png("icon_vietport"), width: 26, height: 13,), nameTransport: "Việt Nam Spot (VNpost / EMS):", price: 16000),
    ListTransport(imgTransport: Image.asset(AppImages.png("icon_ghn"), width: 24, height: 24,), nameTransport: "Giao Hàng Nhanh (GHN):", price: 17000),
    ListTransport(imgTransport: Image.asset(AppImages.png("icon_ghtk"), width: 24, height: 24,), nameTransport: "Giao hàng tiết kiệm (GHTK):", price: 17000),
  ];
}

class ListTransport{
  Image imgTransport;
  String nameTransport;
  int price;
  
  ListTransport({Key ? key, required this.imgTransport, required this.nameTransport, required this.price});
}
