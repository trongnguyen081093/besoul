import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/list_%20transportation_besoul/list_transportation_viewmodel.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../presentation.dart';

class ListTranportationScreen extends StatefulWidget {
  const ListTranportationScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<ListTranportationScreen> {
  late ListTranportationViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ListTranportationViewModel>(
        viewModel: ListTranportationViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: AppColors.gray06,
            bottomNavigationBar: _buildNavigateBar(),
          );
        });
  }

  Widget _buildBody() {
    return Column(
      children: [
        _buildAppBar(),
        _buildListTransport()
      ],
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.createOrder);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 55, top: 30),
            child: Text(
              "Đơn vị giao vận",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListTransport(){
    return Container(
      child: Column(
        children: List.generate(_viewModel.listTransport.length, (index) => Container(
          color: Colors.white,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                child: Row(
                  children: [
                    Expanded(
                      child: _viewModel.listTransport[index].imgTransport,
                    ),
                    Expanded(
                      flex: 7,
                      child: Text(
                        _viewModel.listTransport[index].nameTransport,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTextStyles.fontFamilyRoboto,
                          color: AppColors.gray01
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        '${NumberFormat.simpleCurrency(locale: 'vi', decimalDigits: 0).format(_viewModel.listTransport[index].price)}',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            fontFamily: AppTextStyles.fontFamilyRoboto,
                            color: AppColors.redPrice
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                width: Get.width,
                height: 1,
                color: AppColors.gray09,
              )
            ],
          ),
        )
        ),
      ),
    );
  }

  Widget _buildNavigateBar(){
    return Container(
      height: 84,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
              color: AppColors.gray05,
              blurRadius: 10
          )]
      ),
      child: Container(
        margin: EdgeInsets.only(left: 20, top: 9, right: 20, bottom: 25),
        child: TextButton(
            onPressed: (){
            },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(AppColors.mint),
            ),
            child: Text(
              "Đồng ý",
              style: TextStyle(
                  fontSize: AppFontSize.fontText,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: Colors.white
              ),
            )
        ),
      ),
    );
  }
}
