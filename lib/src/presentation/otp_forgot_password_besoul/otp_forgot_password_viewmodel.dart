import 'package:flutter/material.dart';
import 'package:flutter_app/src/presentation/forgot_password_besoul/forgot_password_besoul_viewmodel.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class OtpForgotPasswordViewModel extends BaseViewModel {
  ForgotPasswordViewModel viewModelForgot = ForgotPasswordViewModel();
  final TextEditingController textController = TextEditingController();
  ValueNotifier<bool> isButtonDisable = ValueNotifier(false);
  ValueNotifier<bool> isShowSendTo = ValueNotifier(false);
  final CustomTimerController timeController = CustomTimerController();

  init() async {
    timeController.start();
    timeController.addListener(() {
      if(timeController.isBlank == false)
      {
        isShowSendTo.value = true;
      }
      else {
        isShowSendTo.value = false;
      }
    });

    textController.addListener(() {
      isButtonDisable.value = textController.text.length == 6;
    });
  }

  @override
  void dispose(){
    super.dispose();
    timeController.dispose();
    isShowSendTo.dispose();
  }
}
