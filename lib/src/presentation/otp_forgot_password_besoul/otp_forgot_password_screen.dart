import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/otp_forgot_password_besoul/otp_forgot_password_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class OtpForgotPasswordScreen extends StatefulWidget {
  const OtpForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<OtpForgotPasswordScreen> {
  late OtpForgotPasswordViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<OtpForgotPasswordViewModel>(
        viewModel: OtpForgotPasswordViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
            resizeToAvoidBottomInset: true,
          );
        });
  }

  Widget _buildBody() {
    return Container(
      width: Get.width,
      height: Get.height,
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 20, top: 50),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.forgotPassword);
                },
                icon: Icon(Icons.arrow_back_ios, color: AppColors.gray03, size: 24,)),
          ),
          Container(
            width: Get.width,
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(left: 20, right: 20, top: 30),
            child: Text(
              "Nhập mã OTP",
              style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyOpenSans,
                  fontWeight: FontWeight.w700,
                  fontSize: 22,
                  color: AppColors.gray01
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Container(
            width: Get.width,
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(left: 20,right: 20, top: 8),
            child: Text(
              'Mã xác nhận đã được gửi qua tin nhắn SMS của số điện thoại +84912345678',
              style: TextStyle(
                  fontFamily: AppTextStyles.fontFamilyOpenSans,
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: AppColors.gray01
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Container(
            width: Get.width,
            margin: EdgeInsets.only(left: 20, right: 20,top: 24),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                border: Border.all(
                    style: BorderStyle.solid,
                    color: AppColors.gray05)
            ),
            child: TextField(
              controller: _viewModel.textController,
              keyboardType: TextInputType.number,
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                  isDense: true,
                  border: InputBorder.none,
                  hintText: "Nhập mã OTP",
                  hintStyle: TextStyle(
                      fontSize: 16,
                      fontFamily: AppTextStyles.fontFamilyOpenSans,
                      fontWeight: FontWeight.w400,
                      color: AppColors.gray04
                  ),
                  contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 12)
              ),
            ),
          ),
          Container(
            width: Get.width,
            margin: EdgeInsets.only(left: 253, right: 20, top: 8),
            child: Row(
              children: [
                ValueListenableBuilder<bool>(
                  valueListenable: _viewModel.isShowSendTo,
                  builder: (context, value, child) => GestureDetector(
                    child: Text(
                      "Gửi lại",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: AppTextStyles.fontFamilyOpenSans,
                        fontSize: 14,
                        color: value ? AppColors.blue01 : Colors.white,
                        fontWeight: FontWeight.w400,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    onTap: () => value ? _viewModel.timeController.reset() : null
                  ),
                ),
                SizedBox(width: 1,),
                CustomTimer(
                    controller: _viewModel.timeController,
                    begin: Duration(minutes: 3),
                    end: Duration(),
                    builder: (remaining){
                      return Text(
                        "(${remaining.minutes}:${remaining.seconds})",
                        style: TextStyle(
                          fontFamily: AppTextStyles.fontFamilyOpenSans,
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: AppColors.gray01
                        ),
                      );
                    }
                )
              ],
            ),
          ),
          Spacer(),
          Container(
            width: Get.width,
            height: 1,
            margin: EdgeInsets.only(bottom: 8),
            decoration: BoxDecoration(
                border: Border.all(color: AppColors.gray06)
            ),
          ),
          Container(
            width: Get.width,
            height: 54,
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 12,),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
            ),
            child: ValueListenableBuilder<bool>(
                valueListenable: _viewModel.isButtonDisable,
                builder: (context,value,child) => TextButton(
                  style: TextButton.styleFrom(
                      backgroundColor: value ? AppColors.mint : AppColors.gray07
                  ),
                  onPressed: value ? () => Get.toNamed(Routers.setNewPassword) : null,
                  child: Text(
                    "Tiếp tục",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: AppFontSize.fontText
                    ),
                  ),
                )),
          )
        ],
      ),
    );
  }
}
