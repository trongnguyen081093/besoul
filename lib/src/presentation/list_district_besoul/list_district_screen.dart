import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/list_district_besoul/list_district_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class ListDistrictScreen extends StatefulWidget {
  const ListDistrictScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<ListDistrictScreen> {
  late ListDistrictViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ListDistrictViewModel>(
        viewModel: ListDistrictViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          _buildSearch(),
          _buildListCity()
        ],
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.createAddress);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 55, top: 30),
            child: Text(
              "Quận, Huyện",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSearch(){
    return Container(
      height: 40,
      color: AppColors.gray06,
      child: TextField(
        onChanged: (string){
          _viewModel.searchList(string);
        },
        controller: _viewModel.search,
        decoration: InputDecoration(
            isDense: true,
            border: InputBorder.none,
            prefixIcon: Container(
              margin: EdgeInsets.only(left: 16, right: 0),
              child: Icon(Icons.search, size: 20, color: AppColors.gray04,),
            ),
            contentPadding: EdgeInsets.only(left: 8, top: 10, bottom: 10),
            hintText: 'Tìm Tỉnh/ Thành phố',
            hintStyle: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              fontFamily: AppTextStyles.fontFamilyRoboto,
              color: AppColors.gray03,
            )
        ),
        style: TextStyle(
            color: AppColors.gray01
        ),
      ),
    );
  }

  Widget _buildListCity(){
    return StreamBuilder<List<String>>(
        stream: _viewModel.result.stream,
        builder: (context, snapshot) {
          if(snapshot.hasData){
            return Container(
              width: Get.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  for(var entry in _viewModel.groupMyList().entries)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 16, top: 16, bottom: 8),
                          child: Text(
                            entry.key,
                            style: TextStyle(
                                fontSize: AppFontSize.fontText,
                                fontWeight: FontWeight.w600,
                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                color: AppColors.mint
                            ),
                          ),
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: List.generate(entry.value.length, (index) => Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 16, bottom: 12,),
                                    child: Text(
                                      entry.value[index],
                                      style: TextStyle(
                                          fontSize: AppFontSize.fontText,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: AppTextStyles.fontFamilyRoboto,
                                          color: AppColors.gray01
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 16, right: 16, bottom: 12),
                                    height: 1,
                                    color: AppColors.gray06,
                                  )
                                ],
                              ),
                            )),
                          ),
                        )
                      ],
                    )
                ],
              ),
            );
          } else {
            return Center(
                child: CircularProgressIndicator()
            );
          }
        }
    );
  }
}
