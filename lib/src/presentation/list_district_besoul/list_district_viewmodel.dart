import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../presentation.dart';

class ListDistrictViewModel extends BaseViewModel {
  init() async {
    listDistrict.sort();
    result.add(listDistrict);
    groupMyList();
  }

  TextEditingController search = TextEditingController();
  var result = BehaviorSubject<List<String>>();

  List<String> listDistrict = [
    "Huyện Bình Chánh",
    "Huyện Củ Chi",
    "Huyện Nhà Bè",
    "Quận 1",
    "Quận 2",
    "Quận 3",
    "Quận 4",
    "Quận 5",
    "Quận 6",
    "Quận Thanh Khê",
    "Huyện Phú Lộc",
    "Huyện Phú Vang",
    "Huyện Nam Đông",
    "Quận 6",
    "Quận 7",
    "Quận 8",
    "Quận 9",
    "Quận 10",
    "Quận 11",
    "Quận 12",
    "Quận 13",
    "Quận 14",
  ];

  Map<String, List<String>> groupMyList() {
    Map<String, List<String>> groupedLists = {};
    result.value.forEach((city) {
      if (groupedLists['${city[0]}'] == null) {
        groupedLists['${city[0]}'] = <String>[];
      }
      groupedLists['${city[0]}']?.add(city);
    });
    return groupedLists;
  }

  void searchList(String keyWord){
    List<String> temp = [];
    listDistrict.forEach((city) {
      if(city.toLowerCase().contains(keyWord.toLowerCase()))temp.add(city);
    });
    result.add(temp);
  }
}
