import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/list_voucher_besoul/list_voucher_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class ListVoucherScreen extends StatefulWidget {
  const ListVoucherScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<ListVoucherScreen> {
  late ListVoucherViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ListVoucherViewModel>(
        viewModel: ListVoucherViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          _buildAppBar(),
          _buildInputVoucher(),
          SizedBox(height: 24,),
          _buildListVoucher()
        ],
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.createOrder);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 85, top: 30),
            child: Text(
              "Voucher",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildInputVoucher(){
    return Container(
      child: Stack(
        children: [
          Container(
            width: 343,
            height: 34,
            margin: EdgeInsets.only(left: 16, right: 16, top: 16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: AppColors.gray06
            ),
            child: TextField(
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(left: 16, bottom: 14),
                hintText: "Nhập mã khuyến mãi",
                hintStyle: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: AppColors.gray03
                )
              ),
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: AppTextStyles.fontFamilyRoboto,
                  color: AppColors.gray01
              )
            ),
          ),
          Positioned(
            child: Container(
              margin: EdgeInsets.only(left: 243,top: 16),
              width: 100,
              height: 34,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: AppColors.mint
              ),
              child: Center(
                child: Text(
                  "Áp dụng",
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      fontFamily: AppTextStyles.fontFamilyRoboto,
                      color: Colors.white
                  )
                ),
              ),
            )
          )
        ],
      ),
    );
  }

  Widget _buildListVoucher(){
    return Container(
      child: Column(
        children: List.generate(10, (index) => GestureDetector(
          onTap: (){
            _viewModel.SetIndex(index);
          },
          child: Container(
            width: 343,
            height: 107,
            margin: EdgeInsets.only(left: 16, right: 16, bottom: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: Colors.white,
                border: Border.all(color: AppColors.gray06),
                boxShadow: [
                  BoxShadow(
                    color: AppColors.gray06.withOpacity(0.5),
                    offset: new Offset(2, 2.0),
                  )
                ]
            ),
            child: Row(
              children: [
                Container(
                    width: 100,
                    height: 107,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(4), bottomLeft: Radius.circular(4)),
                      color: AppColors.mint,
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                            top: 45,
                            bottom: 46,
                            right: 91,
                            child: Image.asset(AppImages.png("ellipse"))
                        ),
                        Center(
                          child: Image.asset(AppImages.png("home_logogift")),
                        )
                      ],
                    )
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                            "GIẢM 50%",
                            style: TextStyle(
                                fontSize: AppFontSize.fontText,
                                fontWeight: FontWeight.w500,
                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                color: AppColors.gray01
                            )
                        ),
                        margin: EdgeInsets.only(top: 22),
                      ),
                      SizedBox(height: 4,),
                      Container(
                        child: Text(
                            "Áp dụng với hoá đơn 150k",
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                color: AppColors.gray01
                            )
                        ),
                      ),
                      SizedBox(height: 4,),
                      Container(
                        child: Text(
                            "HSD: 05/11/2021",
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontFamily: AppTextStyles.fontFamilyRoboto,
                                color: AppColors.gray03
                            )
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 8),
                  child: Column(
                    children: [
                      GestureDetector(
                        onTap: (){
                          _buildShowBottomSheet();
                        },
                        child: Container(
                          child: Image.asset(AppImages.png("icon_more"),
                            width: 20,
                            height: 20,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 24),
                        child: _viewModel.selectIndex == index
                            ? Image.asset(AppImages.png("icon_check"),
                                width: 20,
                                height: 20,
                              )
                            : Image.asset(AppImages.png("icon_uncheck"),
                                width: 20,
                                height: 20,
                              ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        )),
      ),
    );
  }

  void _buildShowBottomSheet(){
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
        context: context,
        builder: (context){
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState){
                return Container(
                    width: Get.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom,
                    ),
                    child: Column(
                      children: [
                        Container(
                          width: Get.width,
                          height: 51,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                            color: AppColors.mint02
                          ),
                          child: Row(
                            children: [
                              Container(
                                child: Text(
                                "Điều kiện và điều khoản",
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: AppTextStyles.fontFamilyRoboto,
                                    color: AppColors.gray01
                                  )
                                ),
                                margin: EdgeInsets.only(left: 90),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 45),
                                child: GestureDetector(
                                  onTap: (){
                                    Navigator.pop(context);
                                  },
                                  child: Image.asset(AppImages.png("icon_x"),
                                    width: 10,
                                    height: 10,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 1,
                          color: AppColors.mint,
                        ),
                        Container(
                          margin: EdgeInsets.only(left:16, right:14, top: 32,),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Text(
                                  "VOUCHER",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray01
                                    )
                                ),
                              ),
                              SizedBox(height: 16,),
                              Container(
                                child: Text(
                                    "1.  Lorem ipsum dolor sit amet, consectetur adipi scing elit ut aliquam, purus sit amet luctus venenatis.",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray03
                                    )
                                ),
                              ),
                              SizedBox(height: 8,),
                              Container(
                                child: Text(
                                  "2.  Lorem ipsum dolor sit amet, consectetur adipi scing elit ut aliquam, purus sit amet luctus venenatis.",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray03
                                    )
                                ),
                              ),
                              SizedBox(height: 8,),
                              Container(
                                child: Text(
                                    "3.  Lorem ipsum dolor sit amet, consectetur adipi scing elit ut aliquam, purus sit amet luctus venenatis.",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray03
                                    )
                                ),
                              ),
                              SizedBox(height: 24,),
                              Container(
                                child: Text(
                                    "NƠI ÁP DỤNG",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray01
                                    )
                                ),
                              ),
                              SizedBox(height: 16,),
                              Container(
                                child: Text(
                                    "Tp. Hồ Chí Minh",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray03
                                    )
                                ),
                              ),
                              SizedBox(height: 8,),
                              Container(
                                child: Text(
                                    "Hà Nội",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray03
                                    )
                                ),
                              ),
                              SizedBox(height: 24,),
                              Container(
                                child: Text(
                                    "HẠN SỬ DỤNG",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray01
                                    )
                                ),
                              ),
                              SizedBox(height: 8,),
                              Container(
                                child: Text(
                                    "5/11/2021",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: AppTextStyles.fontFamilyRoboto,
                                        color: AppColors.gray03
                                    )
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                );
              }
          );
        }
    );
  }
}
