import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/promotion_besoul/promotion_besoul_viewmodel.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class PromotionScreen extends StatefulWidget {
  const PromotionScreen({Key? key}) : super(key: key);

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<PromotionScreen> {
  late PromotionViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<PromotionViewModel>(
        viewModel: PromotionViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
            backgroundColor: Colors.white,
          );
        });
  }

  Widget _buildBody() {
    return Container(
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            _buildAppBar(),
            SizedBox(height: 12,),
            WidgetPromotionBesoulState().buildPromotion(3)
          ],
        ),
      ),
    );
  }

  Widget _buildAppBar(){
    return Container(
      width: Get.width,
      height: 74,
      color: AppColors.mint,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, top: 30),
            child: IconButton(
                onPressed: (){
                  Get.toNamed(Routers.navigation);
                },
                icon: Icon(Icons.arrow_back_ios, color: Colors.white, size: 24,)),
          ),
          Container(
            margin: EdgeInsets.only(left: 45, top: 30),
            child: Text(
              "Tin khuyến mãi",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500,
                fontFamily: AppTextStyles.fontFamilyRoboto
              ),
            ),
          )
        ],
      ),
    );
  }
}
