
import 'package:flutter/material.dart';
import 'package:flutter_app/src/presentation/access_order_besoul/access_order_screen.dart';
import 'package:flutter_app/src/presentation/cart_besoul/cart_besoul_screen.dart';
import 'package:flutter_app/src/presentation/contact_besoul/contact_besoul_screen.dart';
import 'package:flutter_app/src/presentation/create_address_besoul/create_address_screen.dart';
import 'package:flutter_app/src/presentation/create_order_besoul/create_order_screen.dart';
import 'package:flutter_app/src/presentation/create_rate_besoul/create_rate_screen.dart';
import 'package:flutter_app/src/presentation/detail_order_1_besoul/detail_order_1_screen.dart';
import 'package:flutter_app/src/presentation/detail_order_besoul/detail_order_screen.dart';
import 'package:flutter_app/src/presentation/detail_product_1_besoul/detail_product_1_screen.dart';
import 'package:flutter_app/src/presentation/detail_product_besoul/detail_product_screen.dart';
import 'package:flutter_app/src/presentation/forgot_password_besoul/export.dart';
import 'package:flutter_app/src/presentation/home_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_%20transportation_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_%20ward_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_address_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_city_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_district_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_trademark_besoul/export.dart';
import 'package:flutter_app/src/presentation/list_voucher_besoul/export.dart';
import 'package:flutter_app/src/presentation/login_besoul/export.dart';
import 'package:flutter_app/src/presentation/my_kol_besoul/export.dart';
import 'package:flutter_app/src/presentation/notify_besoul/export.dart';
import 'package:flutter_app/src/presentation/order_tracking_besoul/export.dart';
import 'package:flutter_app/src/presentation/otp_forgot_password_besoul/export.dart';
import 'package:flutter_app/src/presentation/promotion_besoul/export.dart';
import 'package:flutter_app/src/presentation/promotion_detail_besoul/export.dart';
import 'package:flutter_app/src/presentation/rate_detail_besoul/export.dart';
import 'package:flutter_app/src/presentation/register_besoul/export.dart';
import 'package:flutter_app/src/presentation/search_besoul/export.dart';
import 'package:flutter_app/src/presentation/select_country_besoul/select_country_screen.dart';
import 'package:flutter_app/src/presentation/select_language_besoul/export.dart';
import 'package:flutter_app/src/presentation/set_new_password/export.dart';
import 'package:flutter_app/src/presentation/transportation_besoul/export.dart';
import 'package:flutter_app/src/presentation/use_code_introduction/export.dart';
import 'package:flutter_app/src/presentation/your_gift_besoul/export.dart';

import 'navigation/navigation_screen.dart';

class Routers {
  static const String navigation = "/navigation";
  static const String selectLanguageBesoul = "/select_language_besoul";
  static const String selectCountryBesoul = "/select_country_besoul";
  static const String loginBesoul = "/login_besoul";
  static const String registerBesoul = "/register_besoul";
  static const String useCodeIntroduction = "/use_code_introduction";
  static const String forgotPassword = "/forgot_password_besoul";
  static const String otpForgotPassword = "/otp_forgot_password_besoul";
  static const String setNewPassword = "/set_new_password";
  static const String homeBesoul = "/home_besoul";
  static const String promotionBesoul = "/promotion_besoul";
  static const String promotionDetailBesoul = "/promotion_detail_besoul";
  static const String notifyBesoul = "/notify_besoul";
  static const String rateDetail = "/rate_detail_besoul";
  static const String productDetail = "/detail_product_besoul";
  static const String createRate = "/create_rate_besoul";
  static const String contactBesoul = "/contact_besoul";
  static const String createOrder = "/create_order_besoul";
  static const String listTransportation = "/list_transportation";
  static const String listVoucher = "/list_voucher_besoul";
  static const String listAddress = "/list_address_besoul";
  static const String createAddress = "/create_address_besoul";
  static const String listCity = "/list_city_besoul";
  static const String listDistrict= "/list_district_besoul";
  static const String listWard= "/list_ward_besoul";
  static const String listGift = "/your_gift_besoul";
  static const String accessOrder = "/access_order_besoul";
  static const String cartBesoul = "/cart_besoul";
  static const String searchBesoul = "/search_besoul";
  static const String transportationBesoul = "/transportation_besoul";
  static const String orderTracking = "/order_tracking_besoul";
  static const String detailOrder = "/detail_order_besoul";
  static const String detailOrder1 = "/detail_order_1_besoul";
  static const String listTrademark = "/list_trademark_besoul";
  static const String myKOL = "/my_kol_besoul";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    var arguments = settings.arguments;
    switch (settings.name) {
      case navigation:
        return animRoute(NavigationScreen(), name: navigation);
        break;
      case selectLanguageBesoul:
        return animRoute(SelectLanguageScreen(), name: selectLanguageBesoul, beginOffset: _right);
        break;
      case selectCountryBesoul:
        return animRoute(SelectCountryScreen(), name: selectCountryBesoul, beginOffset: _right);
        break;
      case loginBesoul:
        return animRoute(LoginBesoulScreen(), name: loginBesoul, beginOffset: _right);
        break;
      case registerBesoul:
        return animRoute(RegisterBesoulScreen(), name: registerBesoul, beginOffset: _right);
        break;
      case useCodeIntroduction:
        return animRoute(UseCodeIntroductionScreen(), name: useCodeIntroduction, beginOffset: _right);
        break;
      case forgotPassword:
        return animRoute(ForgotPasswordScreen(), name: forgotPassword, beginOffset: _right);
        break;
      case otpForgotPassword:
        return animRoute(OtpForgotPasswordScreen(), name: otpForgotPassword, beginOffset: _right);
        break;
      case setNewPassword:
        return animRoute(SetNewPasswordScreen(), name: setNewPassword, beginOffset: _right);
        break;
      case homeBesoul:
        return animRoute(HomeBesoulScreen(), name: homeBesoul, beginOffset: _right);
        break;
      case promotionBesoul:
        return animRoute(PromotionScreen(), name: promotionBesoul, beginOffset: _right);
        break;
      case promotionDetailBesoul:
        return animRoute(PromotionDetailScreen(), name: promotionDetailBesoul, beginOffset: _right);
        break;
      case notifyBesoul:
        return animRoute(NotifyBesoulScreen(), name: notifyBesoul, beginOffset: _right);
        break;
      case rateDetail:
        return animRoute(RateDetailScreen(), name: rateDetail, beginOffset: _right);
        break;
      case productDetail:
        return animRoute(DetailProductScreen(), name: productDetail, beginOffset: _right);
        break;
      case createRate:
        return animRoute(CreateRateScreen(), name: createRate, beginOffset: _right);
        break;
      case contactBesoul:
        return animRoute(ContactScreen(), name: contactBesoul, beginOffset: _right);
        break;
      case createOrder:
        return animRoute(CreateOrderScreen(), name: createOrder, beginOffset: _right);
        break;
      case listTransportation:
        return animRoute(ListTranportationScreen(), name: listTransportation, beginOffset: _right);
        break;
      case listVoucher:
        return animRoute(ListVoucherScreen(), name: listVoucher, beginOffset: _right);
        break;
      case listAddress:
        return animRoute(ListAddressScreen(), name: listAddress, beginOffset: _right);
        break;
      case createAddress:
        return animRoute(CreateAddressScreen(), name: createAddress, beginOffset: _right);
        break;
      case listCity:
        return animRoute(ListCityScreen(), name: listCity, beginOffset: _right);
        break;
      case listDistrict:
        return animRoute(ListDistrictScreen(), name: listDistrict, beginOffset: _right);
        break;
      case listWard:
        return animRoute(ListWardScreen(), name: listWard, beginOffset: _right);
        break;
      case listGift:
        return animRoute(YourGiftScreen(), name: listGift, beginOffset: _right);
        break;
      case accessOrder:
        return animRoute(AccessOrderScreen(), name: accessOrder, beginOffset: _right);
        break;
      case cartBesoul:
        return animRoute(CartBesoulScreen(), name: cartBesoul, beginOffset: _right);
        break;
      case searchBesoul:
        return animRoute(SearchScreen(), name: searchBesoul, beginOffset: _right);
        break;
      case transportationBesoul:
        return animRoute(TransportationScreen(), name: transportationBesoul, beginOffset: _right,);
        break;
      case orderTracking:
        return animRoute(OrderTrackingScreen(), name: orderTracking, beginOffset: _right);
        break;
      case detailOrder:
        return animRoute(DetailOrderScreen(), name: detailOrder, beginOffset: _right);
        break;
      case detailOrder1:
        return animRoute(DetailOrder1Screen(), name: detailOrder1, beginOffset: _right);
        break;
      case listTrademark:
        return animRoute(ListTrademarkScreen(), name: listTrademark, beginOffset: _right);
        break;
      case myKOL:
        return animRoute(MyKOLScreen(), name: myKOL, beginOffset: _right);
        break;
      default:
        return animRoute(Container(
            child:
                Center(child: Text('No route defined for ${settings.name}'))
          ), name: "/error"
        );
    }
  }

  static Route animRoute(Widget page,
      {Offset? beginOffset, required String name, Object? arguments}) {
    return PageRouteBuilder(
      settings: RouteSettings(name: name, arguments: arguments),
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = beginOffset ?? Offset(0.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  static Offset _center = Offset(0.0, 0.0);
  static Offset _top = Offset(0.0, 1.0);
  static Offset _bottom = Offset(0.0, -1.0);
  static Offset _left = Offset(-1.0, 0.0);
  static Offset _right = Offset(1.0, 0.0);
}
