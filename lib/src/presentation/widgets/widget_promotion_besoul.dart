import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_colors.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:flutter_app/src/configs/constanst/app_text_styles.dart';
import 'package:flutter_app/src/presentation/presentation.dart';
import 'package:get/get.dart';

import '../../configs/constanst/app_constants.dart';

class WidgetPromotionBesoul extends StatefulWidget {
  WidgetPromotionBesoul({Key? key}) : super(key : key);


  @override
  WidgetPromotionBesoulState createState() => WidgetPromotionBesoulState();
}

class WidgetPromotionBesoulState extends State<WidgetPromotionBesoul> {

  List<Promotion> listPromotion = [
    Promotion(imgPromotion: AppImages.png("home_inazuma"),
        namePromotion: "CÔNG BỐ KẾT QUẢ CHIẾN THẮNG MINIGAME. ĐỐ VUI KHUI QUÀ",
        datePromotion: "25/03/2022"),
    Promotion(imgPromotion: AppImages.png("home_khuyen_mai"),
        namePromotion: "CÔNG BỐ KẾT QUẢ CHIẾN THẮNG MINIGAME. ĐỐ VUI KHUI QUÀ",
        datePromotion: "25/03/2022"),
    Promotion(imgPromotion: AppImages.png("home_khuyen_mai1"),
        namePromotion: "CÔNG BỐ KẾT QUẢ CHIẾN THẮNG MINIGAME. ĐỐ VUI KHUI QUÀ",
        datePromotion: "25/03/2022"),
  ];

  @override
  Widget build(BuildContext context) {
    return buildBody();
  }

  Widget buildBody(){
    return SizedBox();
  }

  Widget buildPromotion(int length){
    return Column(
        children: List.generate(length, (index) => Container(
          margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
          padding: EdgeInsets.all(0),
          width: 343 * scaleWidth,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              border: Border.all(
                color: AppColors.gray06,
                width: 1,
              )
          ),
          child: Column(
            children: [
              Stack(
                children: [
                  Positioned(
                    child: GestureDetector(
                      onTap: (){
                        Get.toNamed(Routers.promotionDetailBesoul);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(4), topRight: Radius.circular(4))
                        ),
                        child: Image.asset(listPromotion[index].imgPromotion,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 10,
                    left: 10,
                    child: Container(
                      width: 51,
                      height: 19,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: AppColors.gray01.withOpacity(0.45)
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 9, right: 4),
                            child: Icon(
                              Icons.visibility_outlined,
                              size: 11,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            "466",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                fontFamily: AppTextStyles.fontFamilyRoboto
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8, top: 9),
                child: GestureDetector(
                  onTap: (){
                    Get.toNamed(Routers.promotionDetailBesoul);
                  },
                  child: Text(
                    listPromotion[index].namePromotion,
                    style: TextStyle(
                        color: AppColors.gray01,
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                        fontFamily: AppTextStyles.fontFamilyRoboto
                    ),
                  ),
                ),
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8, top: 6),
                    child: Icon(
                      Icons.calendar_month_outlined,
                      color: AppColors.gray03,
                      size: 12,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 6, top: 6),
                    child: Text(
                      listPromotion[index].datePromotion,
                      style: TextStyle(
                          color: AppColors.gray03,
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          fontFamily: AppTextStyles.fontFamilyRoboto
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        )
        )
    );
  }
}

class Promotion{
  final String imgPromotion;
  final String namePromotion;
  final String datePromotion;

  Promotion({Key ? key, required this.imgPromotion, required this.namePromotion, required this.datePromotion});
}